<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','IndexController@index')->name('index');
Route::get('/conferencehall','IndexController@conferencehall')->name('conferencehall');
Route::get('/contact','IndexController@contact')->name('contact');
Route::get('/event','IndexController@event')->name('event');
Route::get('/eventbook','IndexController@eventbook')->name('eventbook');
Route::get('/form','IndexController@form')->name('form');
Route::get('/gallery','IndexController@gallery')->name('gallery');
Route::get('/roombook','IndexController@roombook')->name('roombook');
Route::get('/roomdetail','IndexController@roomdetail')->name('roomdetail');
Route::get('/room','IndexController@room')->name('room');
Route::get('/search','IndexController@search')->name('search');
Route::get('/resturant','IndexController@resturant')->name('resturant');
Route::post('/conference/book','ConferenceController@save')->name('conference.save');

define('PARENT_PREFIX', 'admin');
    Route::group(array('prefix' => PARENT_PREFIX), function () {

        #Authentication
        Route::get('/', 'AuthController@showLogin')->name('admin.login');
        Route::get('login', 'AuthController@showLogin')->name('admin.login');
        Route::post('login-attempt', 'AuthController@postLogin')->name('admin.post.login');
        Route::get('logout', 'AuthController@showLogout')->name('admin.logout');

        Route::group(['middleware' => ['auth']], function () {

            Route::get('/dashboard', function () {
                return view('admin.dashboard');
            })->name('dashboard');

            //    routes for activities
            Route::get('/activities', 'ActivitiesController@index')->name('admin.activities');
            Route::get('/activities/add', 'ActivitiesController@add')->name('admin.activities.add');
            Route::post('/activities/save', 'ActivitiesController@save')->name('admin.activities.save');
            Route::get('/activities/edit/{id}', 'ActivitiesController@edit')->name('admin.activities.edit');
            Route::post('/activities/update', 'ActivitiesController@update')->name('admin.activities.update');
            Route::get('/activities/delete/{id}', 'ActivitiesController@delete')->name('admin.activities.delete');

            //routes for amenities
            Route::get('/amenities', 'AmenitiesController@index')->name('admin.amenities');
            Route::get('/amenities/add', 'AmenitiesController@add')->name('admin.amenities.add');
            Route::post('/amenities/save', 'AmenitiesController@save')->name('admin.amenities.save');
            Route::get('/amenities/edit/{id}', 'AmenitiesController@edit')->name('admin.amenities.edit');
            Route::post('/amenities/update', 'AmenitiesController@update')->name('admin.amenities.update');
            Route::get('/amenities/delete/{id}', 'AmenitiesController@delete')->name('admin.amenities.delete');

            //routes for company
            Route::get('/company', 'CompanyController@index')->name('admin.company');
            Route::get('/company/edit/{id}', 'CompanyController@edit')->name('admin.company.edit');
            Route::post('/company/update', 'CompanyController@update')->name('admin.company.update');

            //Routes for conference
            Route::get('/conference','ConferenceController@index')->name('admin.conference');
            Route::get('/conference/view/{id}','ConferenceController@view')->name('admin.conference.view');
            Route::get('/conference/add', 'ConferenceController@add')->name('admin.conference.add');
            Route::post('/conference/save', 'ConferenceController@save')->name('admin.conference.save');
            Route::get('/conference/edit/{id}', 'ConferenceController@edit')->name('admin.conference.edit');
            Route::post('/conference/update', 'ConferenceController@update')->name('admin.conference.update');
            Route::get('/conference/delete/{id}', 'ConferenceController@delete')->name('admin.conference.delete');

            //    routes for contact
            Route::get('/admin/contact', 'ContactController@index')->name('admin.contact');
            Route::get('/admin/contact/add', 'ContactController@add')->name('admin.contact.add');
            Route::POST('/admin/contact/save', 'ContactController@save')->name('admin.contact.save');
            Route::get('/admin/contact/edit/{id}', 'ContactController@edit')->name('admin.contact.edit');
            Route::POST('/admin/contact/update', 'ContactController@update')->name('admin.contact.update');
            Route::get('/admin/contact/delete/{id}', 'ContactController@delete')->name('admin.contact.delete');

//    routes for customer
            Route::get('/admin/customer', 'CustomerController@index')->name('admin.customer');
            Route::get('/admin/customer/add', 'CustomerController@add')->name('admin.customer.add');
            Route::POST('/admin/customer/save', 'CustomerController@save')->name('admin.customer.save');
            Route::get('/admin/customer/edit/{id}', 'CustomerController@edit')->name('admin.customer.edit');
            Route::POST('/admin/customer/update', 'CustomerController@update')->name('admin.customer.update');
            Route::get('/admin/customer/delete/{id}', 'CustomerController@delete')->name('admin.customer.delete');

//    routes for event
            Route::get('/admin/event', 'EventController@index')->name('admin.event');
            Route::get('/admin/event/add', 'EventController@add')->name('admin.event.add');
            Route::POST('/admin/event/save', 'EventController@save')->name('admin.event.save');
            Route::get('/admin/event/edit/{id}', 'EventController@edit')->name('admin.event.edit');
            Route::POST('/admin/event/update', 'EventController@update')->name('admin.event.update');
            Route::get('/admin/event/delete/{id}', 'EventController@delete')->name('admin.event.delete');

//    routes for food
            Route::get('/admin/foods', 'FoodController@index')->name('admin.foods');
            Route::get('/admin/foods/add', 'FoodController@add')->name('admin.foods.add');
            Route::POST('/admin/foods/save', 'FoodController@save')->name('admin.foods.save');
            Route::get('/admin/foods/edit/{id}', 'FoodController@edit')->name('admin.foods.edit');
            Route::POST('/admin/foods/update', 'FoodController@update')->name('admin.foods.update');
            Route::get('/admin/foods/delete/{id}', 'FoodController@delete')->name('admin.foods.delete');

//    routes for gallery
            Route::get('/admin/gallery', 'GalleryController@index')->name('admin.gallery');
            Route::get('/admin/gallery/add', 'GalleryController@add')->name('admin.gallery.add');
            Route::POST('/admin/gallery/save', 'GalleryController@save')->name('admin.gallery.save');
            Route::get('/admin/gallery/edit/{id}', 'GalleryController@edit')->name('admin.gallery.edit');
            Route::POST('/admin/gallery/update', 'GalleryController@update')->name('admin.gallery.update');
            Route::get('/admin/gallery/delete/{id}', 'GalleryController@delete')->name('admin.gallery.delete');

//    routes for logs
            Route::get('/admin/logs', 'LogController@index')->name('admin.logs');
            Route::get('/admin/logs/add', 'LogController@add')->name('admin.logs.add');
            Route::POST('/admin/logs/save', 'LogController@save')->name('admin.logs.save');
            Route::get('/admin/logs/edit/{id}', 'LogController@edit')->name('admin.logs.edit');
            Route::POST('/admin/logs/update', 'LogController@update')->name('admin.logs.update');
            Route::get('/admin/logs/delete/{id}', 'LogController@delete')->name('admin.logs.delete');

//    routes for resturant
            Route::get('/admin/restaurant', 'ResturantController@index')->name('admin.restaurant');
//            Route::get('/admin/resturant/add', 'ResturantController@add')->name('admin.resturant.add');
//            Route::get('/admin/resturant/save', 'ResturantController@save')->name('resturant.save');
            Route::get('/admin/restaurant/edit/{id}', 'ResturantController@edit')->name('admin.restaurant.edit');
            Route::POST('/admin/restaurant/update', 'ResturantController@update')->name('admin.restaurant.update');
            Route::get('/admin/restaurant/delete/{id}', 'ResturantController@delete')->name('admin.restaurant.delete');


//    routes for rooms
            Route::get('/admin/rooms', 'RoomController@index')->name('admin.rooms');
            Route::get('/admin/rooms/add', 'RoomController@add')->name('admin.rooms.add');
            Route::POST('/admin/rooms/save', 'RoomController@save')->name('admin.rooms.save');
            Route::get('/admin/rooms/edit/{id}', 'RoomController@edit')->name('admin.rooms.edit');
            Route::POST('/admin/rooms/update', 'RoomController@update')->name('admin.rooms.update');
            Route::get('/admin/rooms/delete/{id}', 'RoomController@delete')->name('admin.rooms.delete');

//    routes for setting
            Route::get('/admin/setting', 'SettingController@index')->name('admin.setting');
//            Route::get('/admin/setting/add', 'SettingController@add')->name('setting.add');
//            Route::get('/admin/setting/save', 'SettingController@save')->name('setting.save');
            Route::get('/admin/setting/edit/{id}', 'SettingController@edit')->name('admin.setting.edit');
            Route::POST('/admin/setting/update', 'SettingController@update')->name('admin.setting.update');
//            Route::get('/admin/setting/delete/{id}', 'SettingController@delete')->name('setting.delete');



//    routes for subs
            Route::get('/admin/subscription', 'SubsController@index')->name('subscription');
            Route::get('/admin/subscription/add', 'SubsController@add')->name('subscription.add');
            Route::get('/admin/subscription/save', 'SubsController@save')->name('subscription.save');
            Route::get('/admin/subscription/edit/{id}', 'SubsController@edit')->name('subscription.edit');
            Route::get('/admin/subscription/update', 'SubsController@update')->name('subscription.update');
            Route::get('/admin/subscription/delete/{id}', 'SubsController@delete')->name('subscription.delete');


// routes for slider
            Route::get('/admin/Slider', 'SliderController@index')->name('admin.slider');
            Route::get('/admin/Slider/add', 'SliderController@add')->name('admin.slider.add');
            Route::POST('/admin/Slider/save', 'SliderController@save')->name('admin.slider.save');
            Route::get('/admin/Slider/edit/{id}', 'SliderController@edit')->name('admin.slider.edit');
            Route::POST('/admin/Slider/update', 'SliderController@update')->name('admin.slider.update');
            Route::get('/admin/Slider/delete/{id}', 'SliderController@delete')->name('admin.slider.delete');


//    routes for testimonials
            Route::get('/admin/testimonials', 'TestimonialController@index')->name('admin.testimonials');
            Route::get('/admin/testimonials/add', 'TestimonialController@add')->name('admin.testimonials.add');
            Route::POST('/admin/testimonials/save', 'TestimonialController@save')->name('admin.testimonials.save');
            Route::get('/admin/testimonials/edit/{id}', 'TestimonialController@edit')->name('admin.testimonials.edit');
            Route::POST('/admin/testimonials/update', 'TestimonialController@update')->name('admin.testimonials.update');
            Route::get('/admin/testimonials/delete/{id}', 'TestimonialController@delete')->name('admin.testimonials.delete');

//    routes for upcomingevents
            Route::get('/admin/upcomingevents', 'UpcomingEventController@index')->name('admin.upcomingevents');
            Route::get('/admin/upcomingevents/add', 'UpcomingEventController@add')->name('admin.upcomingevents.add');
            Route::POST('/admin/upcomingevents/save', 'UpcomingEventController@save')->name('admin.upcomingevents.save');
            Route::get('/admin/upcomingevents/edit/{id}', 'UpcomingEventController@edit')->name('admin.upcomingevents.edit');
            Route::POST('/admin/upcomingevents/update', 'UpcomingEventController@update')->name('admin.upcomingevents.update');
            Route::get('/admin/upcomingevents/delete/{id}', 'UpcomingEventController@delete')->name('admin.upcomingevents.delete');


        });
    });

