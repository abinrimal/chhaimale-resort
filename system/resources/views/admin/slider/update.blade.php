@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Slider
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Slider</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  Update Slider
                        </div>
                        <div class="panel-body">
                            <form action="{{route('admin.slider.update')}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <input type="hidden" name="id" value="{!! $slider->id !!}">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('slider_text') ? 'has-error' : '' }}">
                                            <label>Text</label>
                                            <input class="form-control" type="text" name="slider_text"  value="{{$slider->link_text}}">
                                            @if ($errors->has('slider_text'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('slider_text') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('slider_order') ? 'has-error' : '' }}">
                                            <label>Order</label>
                                            <input class="form-control" type="text" name="slider_order"  value="{{$slider->order}}">
                                            @if ($errors->has('slider_order'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('slider_order') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('slider_image') ? 'has-error' : '' }}">
                                            <label>Image</label>
                                            <input class="form-control" type="file" name="slider_image" value="{{$slider->image}}">
                                            @if ($errors->has('slider_image'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('slider_image') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4 form-group">
                                    <button type="submit" class="btn btn-success" value="save">Update </button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop