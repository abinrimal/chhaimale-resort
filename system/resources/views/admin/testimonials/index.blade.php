@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Testimonials
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Testimonials</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  All Testimonials
                            <a href="{{route('admin.testimonials.add')}}" class="pull-right"><i class="fa fa-plus"></i> Add New</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover"
                                       @if(count($alltestimonial) >0)
                                       id="dataTables-example"
                                        @endif
                                >
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Position</th>
                                        <th>Company</th>
                                        <th>Rating</th>
                                        <th>Image</th>
                                        <th>Number</th>
                                        <th>Created at</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1; ?>
                                    @forelse($alltestimonial as $testimonial)
                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td>{!! $testimonial->name !!}</td>
                                            <td>{!! $testimonial->position !!}</td>
                                            <td>{!! $testimonial->company !!}</td>
                                            <td>{!! $testimonial->rating !!}</td>
                                            <td><img class="img-responsive" style="width: 50px; height: 50px;" src="{!! asset($img_path.$testimonial->picture) !!}" alt=""></td>
                                            <td>{!! $testimonial->phone_number !!}</td>
                                            <td>{!! $testimonial->getUserName($testimonial->added_by)!!}</td>
                                            <td>{!! $testimonial->getUserName($testimonial->edited_by) !!}</td>
                                            <td>{!! date('d F Y',strtotime($testimonial->created_at)) !!}</td>
                                            <td>
                                                <nobr>
                                                    <a href="" class="btn btn-success" title="View"><i class="fa fa-search"></i></a>
                                                    <a href="{{route('admin.testimonials.edit',$testimonial->id)}}" class="btn btn-info" title="View"><i class="fa fa-edit"></i></a>
                                                    <a href="{{route('admin.testimonials.delete',$testimonial->id)}}" class="btn btn-danger" title="View"><i class="fa fa-trash"></i></a>
                                                </nobr>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    @empty
                                        <tr>
                                            <td colspan="9">No Activities yet.</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </section>
        <!-- /.content -->
    </div>
@stop

@section('scripts')
    @parent

@stop