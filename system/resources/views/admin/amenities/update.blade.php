@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Amenities
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Amenities</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  Update Amenity
                        </div>
                        <div class="panel-body">
                            <form action="{{route('admin.amenities.update')}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <input type="hidden" name="id" value="{!! $amenity->id !!}">
                                <div class="col-md-4">
                                    <div class="form-group {{$errors->has('amenity_name') ? 'has-error' : '' }}">
                                        <label>Amenity Name</label>
                                        <input class="form-control" type="text" name="amenity_name"  value="{{ $amenity->name }}">
                                        @if ($errors->has('amenity_name'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('amenity_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group {{$errors->has('amenity_image') ? 'has-error' : '' }}">
                                        <label>Amenity Image</label>
                                        <input class="form-control" type="file" name="amenity_image"  value="">
                                        <img src="{!! asset($img_path.$amenity->image) !!}" alt="">
                                        @if ($errors->has('amenity_image'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('amenity_image') }}</strong>

                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4 form-group">
                                    <button type="submit" class="btn btn-success" value="save">Update Amenity</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop