@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Conference conferences
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Conference conferences</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  Conference Booking Details
                            <a href="{{route('admin.company.edit',$conference->id)}}" class="pull-right"><i class="fa fa-plus"></i> Edit Booking Details</a>
                        </div>
                        <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                    <b>Name:</b>  {!! $conference->title !!} {!! $conference->first_name  !!} {!! $conference->last_name !!}
                            </div>
                            <div class="col-md-4">
                                <b>Address 1:</b>  {{$conference->address1}}
                            </div>
                                <div class="col-md-4">
                                <b>Address2:</b> {{$conference->address2}}
                            </div>
                        </div>
                            <hr>
                        <div class="row">
                            <div class="col-md-4">
                                    <b>City:</b>  {!! $conference->city !!} 
                            </div>
                            <div class="col-md-4">
                                <b>Zip Code:</b>  {{$conference->zip}}
                            </div>
                                <div class="col-md-4">
                                <b>Country:</b> {{$conference->country}}
                            </div>
                        </div>
                            <hr>
                        <div class="row">
                            <div class="col-md-4">
                                    <b>Phone:</b>  {!! $conference->phone !!} 
                            </div>
                            <div class="col-md-4">
                                <b>Email Address:</b>  {{$conference->email}}
                            </div>
                                <div class="col-md-4">
                                <b>Oraganization Name:</b> {{$conference->org}}
                            </div>
                        </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                <b>Conference Name:</b> {{$conference->cfname}}
                            </div>
                            <div class="col-md-4">
                                    <b>Start Date Time:</b>  {!! $conference->startdate !!}  {!! $conference->starttime !!} 
                            </div>
                            <div class="col-md-4">
                                <b>End Date Time:</b>  {!! $conference->enddate !!}  {!! $conference->endtime !!} 
                            </div> 
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                <b>No Of Attandance:</b> {!! $conference->attendance !!}
                            </div>
                            <div class="col-md-4">
                                    <b>Setup Style:</b>  {!! $conference->setup_suggestion !!} 
                            </div>
                            <div class="col-md-4">
                                <b>Setup Suggestion:</b>  {{!! $conference->setup_suggestion !!} 
                            </div> 
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                <b>Meal:</b> {!! $conference->meal !!}
                            </div>
                            <div class="col-md-6">
                                <b>Meal Suggestion:</b>  {!! $conference->meal_suggestion !!} 
                            </div> 
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                <b>Room Details:</b> {!! $conference->room_detail !!}
                            </div>
                            <div class="col-md-6">
                                <b>Room Suggestion:</b>  {!! $conference->room_suggestion !!} 
                            </div> 
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                <b>Credit Card Number:</b> {!! $conference->credit_card !!}
                            </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </section>
        <!-- /.content -->
    </div>
@stop

@section('scripts')
    @parent

@stop