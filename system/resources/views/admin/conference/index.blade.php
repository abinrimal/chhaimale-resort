@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Conference Bookings
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Conferance Bookings</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  All Conference Bookings
                            <a href="{{route('admin.conference.add')}}" class="pull-right"><i class="fa fa-plus"></i> Add New</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover"
                                       @if(count($conference_bookings) >0)
                                       id="dataTables-example"
                                        @endif
                                >
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Booking By</th>
                                        <th>Phone Number</th>
                                        <th>Email Address</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Added By</th>
                                        <th>Edited By</th>
                                        <th>Created at</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1; ?>
                                    @forelse($conference_bookings as $conference)
                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td>{!! $conference->title !!} {!! $conference->first_name  !!} {!! $conference->last_name !!}</td>
                                            <td>{!! $conference->phone !!}</td>
                                            <td>{!! $conference->email !!}</td>
                                            <td>{!! $conference->startdate!!}</td>
                                            <td>{!! $conference->enddate!!}</td>
                                            <td>{!! $conference->getUserName($conference->added_by)!!}</td>
                                            <td>{!! $conference->getUserName($conference->edited_by) !!}</td>
                                            <td>{!! date('d F Y',strtotime($conference->created_at)) !!}</td>
                                            <td>
                                                <nobr>
                                                    <a href="{{route('admin.conference.view',$conference->id)}}" class="btn btn-success" title="View"><i class="fa fa-search"></i></a>
                                                    <a href="{{route('admin.conference.edit',$conference->id)}}" class="btn btn-info" title="View"><i class="fa fa-edit"></i></a>
                                                    <a href="{{route('admin.conference.delete',$conference->id)}}" class="btn btn-danger" title="View"><i class="fa fa-trash"></i></a>
                                                </nobr>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    @empty
                                        <tr>
                                            <td colspan="9">No Bookings yet.</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </section>
        <!-- /.content -->
    </div>
@stop

@section('scripts')
    @parent

@stop