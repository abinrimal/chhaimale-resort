@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Event
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Event</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  Add New Event
                        </div>
                        <div class="panel-body">
                            <form action="{{route('admin.event.update')}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <input type="hidden" value="{{$event->id}}" name="id">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('event_title') ? 'has-error' : '' }}">
                                            <label>Title</label>
                                            <input class="form-control" type="text" name="event_title"  value="{{ $event->title }}">
                                            @if ($errors->has('event_title'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('event_title') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('event_subtitle') ? 'has-error' : '' }}">
                                            <label>Subtitle</label>
                                            <input class="form-control" type="text" name="event_subtitle"  value="{{ $event->sub_title }}">
                                            @if ($errors->has('event_subtitle'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('event_subtitle') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group {{$errors->has('event_description') ? 'has-error' : '' }}">
                                            <label>Description</label>
                                            <textarea class="form-control" rows="5" type="file" name="event_description">{{ $event->description }}
                                            </textarea>
                                            @if ($errors->has('event_description'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('event_description') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4 form-group">
                                    <button type="submit" class="btn btn-success" value="save">Update</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop