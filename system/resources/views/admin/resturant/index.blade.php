@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Restaurant
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Restaurant</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  All Restaurant
                            {{--<a href="{{route('admin.testimonials.add')}}" class="pull-right"><i class="fa fa-plus"></i> Add New</a>--}}
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover"
                                       @if(count($allrestaurant) >0)
                                       id="dataTables-example"
                                        @endif
                                >
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Page Header</th>
                                        <th>Sub Header</th>
                                        <th>Chef name</th>
                                        <th>Chef photo</th>
                                        <th>Chef Message</th>
                                        <th>Featured Image</th>
                                        <th>Featured Title</th>
                                        <th>Featured Message</th>
                                        <th>Description</th>
                                        <th>Added By</th>
                                        <th>Edited By</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1; ?>
                                    @forelse($allrestaurant as $restaurant)
                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td>{!! str_limit($restaurant->page_header,10,'..') !!}</td>
                                            <td>{!! str_limit($restaurant->sub_header,10,'...') !!}</td>
                                            <td>{!! $restaurant->chef_name !!}</td>
                                            <td><img class="img-responsive" style="width: 50px; height: 50px;" src="{!! asset($img_path.$restaurant->chef_photo) !!}" alt=""></td>
                                            <td>{!! str_limit($restaurant->chef_message,20,'...') !!}</td>
                                            <td><img class="img-responsive" style="width: 50px; height: 50px;" src="{!! asset($img_path.$restaurant->featured_image) !!}" alt=""></td>
                                            <td>{!! $restaurant->featured_title !!}</td>
                                            <td>{!! str_limit($restaurant->featured_message,20,'...') !!}</td>
                                            <td>{!! str_limit($restaurant->description,20,'...') !!}</td>
                                            <td>{!! $restaurant->getUserName($restaurant->added_by)!!}</td>
                                            <td>{!! $restaurant->getUserName($restaurant->edited_by) !!}</td>
                                            <td>{!! date('d F Y',strtotime($restaurant->created_at)) !!}</td>
                                            <td>
                                                <nobr>
                                                    <a href="" class="btn btn-success" title="View"><i class="fa fa-search"></i></a>
                                                    <a href="{{route('admin.restaurant.edit',$restaurant->id)}}" class="btn btn-info" title="View"><i class="fa fa-edit"></i></a>
                                                    <a href="{{route('admin.restaurant.delete',$restaurant->id)}}" class="btn btn-danger" title="View"><i class="fa fa-trash"></i></a>
                                                </nobr>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    @empty
                                        <tr>
                                            <td colspan="9">No Activities yet.</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </section>
        <!-- /.content -->
    </div>
@stop

@section('scripts')
    @parent

@stop