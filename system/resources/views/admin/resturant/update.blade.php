@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Restaurant
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Restaurant</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  Update Restaurant
                        </div>
                        <div class="panel-body">
                            <form action="{{route('admin.restaurant.update')}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <input type="hidden" name="id" value="{!! $restaurant->id !!}">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('page_header') ? 'has-error' : '' }}">
                                            <label>Page Header</label>
                                            <input class="form-control" type="text" name="page_header"  value="{{$restaurant->page_header}}">
                                            @if ($errors->has('page_header'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('page_header') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('sub_header') ? 'has-error' : '' }}">
                                            <label>Sub header</label>
                                            <input class="form-control" type="text" name="sub_header"  value="{{$restaurant->sub_header}}">
                                            @if ($errors->has('sub_header'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('sub_header') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('chef_name') ? 'has-error' : '' }}">
                                            <label>Chef Name</label>
                                            <input class="form-control" type="text" name="chef_name"  value="{{$restaurant->chef_name}}">
                                            @if ($errors->has('chef_name'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('chef_name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('featured_title') ? 'has-error' : '' }}">
                                            <label>Featured title</label>
                                            <input class="form-control" type="text" name="featured_title"  value="{{$restaurant->featured_title}}">
                                            @if ($errors->has('featured_title'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('featured_title') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group {{$errors->has('chef_message') ? 'has-error' : '' }}">
                                            <label>Chef message</label>
                                            <textarea class="form-control" rows="5" type="text" name="chef_message"  value="">{{ $restaurant->chef_message}}
                                            </textarea>
                                            @if ($errors->has('chef_message'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('chef_message') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group {{$errors->has('featured_message') ? 'has-error' : '' }}">
                                            <label>Featured message</label>
                                            <textarea class="form-control" rows="5" type="text" name="featured_message"  value="">{{ $restaurant->featured_message}}
                                            </textarea>
                                            @if ($errors->has('featured_message'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('featured_message') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('chef_photo') ? 'has-error' : '' }}">
                                            <label>Chef photo</label>
                                            <input class="form-control" type="file" name="chef_photo"  value="{{$restaurant->chef_photo}}">
                                            @if ($errors->has('chef_photo'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('chef_photo') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('featured_image') ? 'has-error' : '' }}">
                                            <label>Featured image</label>
                                            <input class="form-control" type="file" name="featured_image"  value="{{$restaurant->featured_image}}">
                                            @if ($errors->has('featured_image'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('featured_image') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group {{$errors->has('description') ? 'has-error' : '' }}">
                                            <label>Description</label>
                                            <textarea class="form-control" rows="5" type="text" name="description"  value="">{{ $restaurant->description}}
                                            </textarea>
                                            @if ($errors->has('description'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4 form-group">
                                    <button type="submit" class="btn btn-success" value="save">Update</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop