@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Setting
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Setting</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  Update Setting
                        </div>
                        <div class="panel-body">
                            <form action="{{route('admin.setting.update')}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <input type="hidden" name="id" value="{!! $setting->id !!}">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('site_title') ? 'has-error' : '' }}">
                                            <label>Site Title</label>
                                            <input class="form-control" type="text" name="site_title"  value="{{$setting->site_title}}">
                                            @if ($errors->has('site_title'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('site_title') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('logo') ? 'has-error' : '' }}">
                                            <label>Logo</label>
                                            <input class="form-control" type="file" name="logo"  value="{{$setting->logo}}">
                                            @if ($errors->has('logo'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('logo') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group {{$errors->has('meta_description') ? 'has-error' : '' }}">
                                            <label>Meta Description</label>
                                            <textarea class="form-control" rows="5" type="file" name="meta_description"  value="">{{ $setting->meta_description}}
                                            </textarea>
                                            @if ($errors->has('meta_description'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('meta_description') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('meta_title') ? 'has-error' : '' }}">
                                            <label>Meta title</label>
                                            <input class="form-control" type="text" name="meta_title"  value="{{$setting->meta_title}}">
                                            @if ($errors->has('meta_title'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('meta_title') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('logo_collapsed') ? 'has-error' : '' }}">
                                            <label>Logo Collapsed</label>
                                            <input class="form-control" type="file" name="logo_collapsed"  value="{{$setting->logo_collapsed}}">
                                            @if ($errors->has('logo_collapsed'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('logo_collapsed') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('keywords') ? 'has-error' : '' }}">
                                            <label>Keywords</label>
                                            <input class="form-control" type="text" name="keywords"  value="{{$setting->keywords}}">
                                            @if ($errors->has('keywords'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('keywords') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('copyright_text') ? 'has-error' : '' }}">
                                            <label>Copyright text</label>
                                            <input class="form-control" type="text" name="copyright_text"  value="{{$setting->copyright_text}}">
                                            @if ($errors->has('copyright_text'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('copyright_text') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4 form-group">
                                    <button type="submit" class="btn btn-success" value="save">Update</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop