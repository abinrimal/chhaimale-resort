    @extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Setting
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Setting</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  All Testimonials
                            {{--<a href="{{route('admin.testimonials.add')}}" class="pull-right"><i class="fa fa-plus"></i> Add New</a>--}}
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover"
                                       @if(count($allsetting) >0)
                                       id="dataTables-example"
                                        @endif
                                >
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Site Title</th>
                                        <th>logo</th>
                                        <th>Logo Collapsed</th>
                                        <th>Meta title</th>
                                        <th>Meta description</th>
                                        <th>Keywords</th>
                                        <th>Copyright</th>
                                        <th>Added By</th>
                                        <th>Edited By</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1; ?>
                                    @forelse($allsetting as $setting)
                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td>{!! $setting->site_title !!}</td>
                                            <td><img class="img-responsive" style="width: 50px; height: 50px;" src="{!! asset($img_path.$setting->logo) !!}" alt=""></td>
                                            <td><img class="img-responsive" style="width: 50px; height: 50px;" src="{!! asset($img_path.$setting->logo_collapsed) !!}" alt=""></td>
                                            <td>{!! $setting->meta_title !!}</td>
                                            <td>{!! str_limit($setting->meta_description,20,'&raquo') !!}</td>
                                            <td>{!! $setting->keywords !!}</td>
                                            <td>{!! $setting->copyright_text !!}</td>
                                            <td>{!! $setting->getUserName($setting->added_by)!!}</td>
                                            <td>{!! $setting->getUserName($setting->edited_by) !!}</td>
                                            <td>{!! date('d F Y',strtotime($setting->created_at)) !!}</td>
                                            <td>
                                                <nobr>
                                                    <a href="" class="btn btn-success" title="View"><i class="fa fa-search"></i></a>
                                                    <a href="{{route('admin.setting.edit',$setting->id)}}" class="btn btn-info" title="View"><i class="fa fa-edit"></i></a>
                                                    {{--<a href="{{route('admin.setting.delete',$setting->id)}}" class="btn btn-danger" title="View"><i class="fa fa-trash"></i></a>--}}
                                                </nobr>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    @empty
                                        <tr>
                                            <td colspan="9">No Activities yet.</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </section>
        <!-- /.content -->
    </div>
@stop

@section('scripts')
    @parent

@stop