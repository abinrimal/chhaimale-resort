<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> Chhaimale Resort </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="shortcut icon" type="image/png" href="{{ url('favicon.ico')  }}" sizes="150x150">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('assets/css/admin-lte.min.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/iCheck/square/blue.css') }}">
    <style type="text/css">
        .icheckbox_square-blue.focus[aria-checked="false"] {
            background-position: -24px 0;
        }

        body {
            height: 100%;
            width: 100%;
            padding: 100px;
            background-size: 100px 100px, cover, cover !important;
            background: url('{{ url('assets/img/bg.jpg') }}'), repeat, no-repeat, no-repeat fixed, fixed, fixed top left, center center, bottom center !important;
        }

        .login-box {
            margin: 0 auto;
        }

        .form-control {
            background: rgba(255, 255, 255, 0.75);
        }

        .login-logo a:focus {
            text-decoration: underline;
        }
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="{{ url('/') }}"><b>Chhaimale Resort </b> <br> System Access </a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        @if($errors->has('auth'))
            <div class="alert alert-danger alert-dismissable fade in animated shake">
                <button class="close" data-dismiss="alert"><i class="fa fa-close"></i></button>
                <i class="fa fa-warning"></i>
                <span>{{ $errors->first('auth') }}</span>
            </div>
        @else
            <p class="login-box-msg ">Login in to start your session</p>
        @endif
        <form action="{{route('admin.post.login')}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" tabindex="0">
            <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                <input type=" text"
                       value="{{ old('email') }}" name="email" class="form-control"
                       placeholder="email" autofocus>
                <span class="fa fa-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                <input type="password" name="password" class="form-control" placeholder="Password">
                <span class="fa fa-key form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox" name="remember"> Remember Me
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
                <!-- /.col -->
            </div>
        </form>
    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
<div class="lockscreen-footer text-center">
    © 2018. Prabidhi Labs. <b>All rights reserved. </b><br>
    Developed by:
    <a href="http://www.prabidhilabs.com" target="_blank"><strong>Prabidhi Labs</strong></a> | <span>CMS Version: 1.0.0</span>
</div>
<!-- jQuery 2.2.0 -->
<script src="{{ asset('assets/js/plugins/jQuery-2.2.0.min.js') }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('assets/js/bootstrap.min.js')  }}"></script>
<!-- iCheck -->
<script src="{{ asset('assets/plugins/iCheck/icheck.min.js') }}"></script>
<script>0
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

        $('[data-toggle="popover"]').popover()

    });
</script>
</body>
