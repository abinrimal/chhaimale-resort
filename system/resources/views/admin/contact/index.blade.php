@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Mails
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Mails</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
             <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-comments fa-lg fa-fw"></i>  All Mails
                    <a href="" class="pull-right"><i class="fa fa-plus"></i> Send New Mail</a>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover"
                               @if(count($messages) >0)
                               id="dataTables-example"
                                @endif
                        >
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Mobile Number</th>
                                <th>Subject</th>
                                <th>Message</th>
                                <th>Sent at</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1; ?>
                            @forelse($messages as $message)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{!! $message->name !!}</td>
                                    <td>{!! $message->email !!}</td>
                                    <td>{!! $message->mobile!!}</td>
                                    <td>{!! $message->subject!!}</td>
                                    <td>{!! $message->message!!}</td>
                                    <td>{!! date('d F Y',strtotime($message->created_at)) !!}</td>
                                    <td>
                                        <nobr>
                                            <a href="" class="btn btn-success" title="View"><i class="fa fa-search"></i></a>
                                            <a href="{{route('admin.activities.edit',$message->id)}}" class="btn btn-info" title="View"><i class="fa fa-reply"></i></a>
                                        </nobr>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                            @empty
                                <tr>
                                    <td colspan="9">No messages yet.</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
        </section>
        <!-- /.content -->
    </div>
@stop

@section('scripts')
    @parent

@stop