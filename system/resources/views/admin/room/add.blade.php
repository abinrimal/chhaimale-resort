@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Rooms
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Rooms</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  Add New Rooms
                        </div>
                        <div class="panel-body">
                            <form action="{{route('admin.rooms.save')}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('room_type') ? 'has-error' : '' }}">
                                            <label>Type</label>
                                            <input class="form-control" type="text" name="room_type"  value="{{ old('room_type') }}">
                                            @if ($errors->has('room_type'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('room_type') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('room_facilities') ? 'has-error' : '' }}">
                                            <label>Facilities</label>
                                            <input class="form-control" type="text" name="room_facilities"  value="{{ old('room_facilities') }}">
                                            @if ($errors->has('room_facilities'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('room_facilities') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group {{$errors->has('room_description') ? 'has-error' : '' }}">
                                            <label>Description</label>
                                            <textarea class="form-control" rows="5" type="text" name="room_description"  value="{{ old('room_description') }}">
                                            </textarea>
                                            @if ($errors->has('room_description'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('room_description') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('room_last_price') ? 'has-error' : '' }}">
                                            <label>Last Price</label>
                                            <input class="form-control" type="text" name="room_last_price"  value="{{ old('room_last_price') }}">
                                            @if ($errors->has('room_last_price'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('room_last_price') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('room_starting_price') ? 'has-error' : '' }}">
                                            <label>Start Price</label>
                                            <input class="form-control" type="text" name="room_starting_price"  value="{{ old('room_starting_price') }}">
                                            @if ($errors->has('room_starting_price'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('room_starting_price') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('total_rooms') ? 'has-error' : '' }}">
                                            <label>Total Rooms</label>
                                            <input class="form-control" type="text" name="total_rooms"  value="{{ old('total_rooms') }}">
                                            @if ($errors->has('total_rooms'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('total_rooms') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('room_sales_price') ? 'has-error' : '' }}">
                                            <label>Sales Price</label>
                                            <input class="form-control" type="text" name="room_sales_price"  value="{{ old('room_sales_price') }}">
                                            @if ($errors->has('room_sales_price'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('room_sales_price') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('room_availability') ? 'has-error' : '' }}">
                                            <label>Availability</label>
                                            <input class="form-control" type="text" name="room_availability"  value="{{ old('room_availability') }}">
                                            @if ($errors->has('room_availability'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('room_availability') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('room_status') ? 'has-error' : '' }}">
                                            <label>Status</label>
                                            <input class="form-control" type="text" name="room_status"  value="{{ old('room_status') }}">
                                            @if ($errors->has('room_status'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('room_status') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('room_criteria') ? 'has-error' : '' }}">
                                            <label>Criteria</label>
                                            <input class="form-control" type="text" name="room_criteria"  value="{{ old('room_criteria') }}">
                                            @if ($errors->has('room_criteria'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('room_criteria') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4 form-group">
                                    <button type="submit" class="btn btn-success" value="save">Add New </button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop