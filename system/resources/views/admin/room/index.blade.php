@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Rooms
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Rooms</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  All Rooms
                            <a href="{{route('admin.rooms.add')}}" class="pull-right"><i class="fa fa-plus"></i> Add New</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover"
                                       @if(count($allrooms) >0)
                                       id="dataTables-example"
                                        @endif
                                >
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Type</th>
                                        <th>Starting price</th>
                                        <th>Last Price</th>
                                        <th>Criteria</th>
                                        <th>Facilities</th>
                                        <th>Description</th>
                                        <th>Total Rooms</th>
                                        <th>Availability</th>
                                        <th>Status</th>
                                        <th>Sales price</th>
                                        <th>Created at</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1; ?>
                                    @forelse($allrooms as $room)
                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td>{!! $room->type !!}</td>
                                            <td>{!! $room->starting_price !!}</td>
                                            <td>{!! $room->last_price !!}</td>
                                            <td>{!! $room->criteria !!}</td>
                                            <td>{!! str_limit($room->facilities,40,'..') !!}</td>
                                            <td>{!! str_limit($room->description,40,'..') !!}</td>
                                            <td>{!! $room->total_rooms!!}</td>
                                            <td>{!! $room->availability !!}</td>
                                            <td>{!! $room->status !!}</td>
                                            <td>{!! $room->sales_price !!}</td>
                                            <td>{!! date('d F Y',strtotime($room->created_at)) !!}</td>
                                            <td>
                                                <nobr>
                                                    <a href="" class="btn btn-success" title="View"><i class="fa fa-search"></i></a>
                                                    <a href="{{route('admin.rooms.edit',$room->id)}}" class="btn btn-info" title="View"><i class="fa fa-edit"></i></a>
                                                    <a href="{{route('admin.rooms.delete',$room->id)}}" class="btn btn-danger" title="View"><i class="fa fa-trash"></i></a>
                                                </nobr>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    @empty
                                        <tr>
                                            <td colspan="9">No Activities yet.</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </section>
        <!-- /.content -->
    </div>
@stop

@section('scripts')
    @parent

@stop