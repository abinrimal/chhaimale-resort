@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Activities
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Activities</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
             <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-comments fa-lg fa-fw"></i>  All Activities
                    <a href="{{route('admin.activities.add')}}" class="pull-right"><i class="fa fa-plus"></i> Add New</a>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover"
                               @if(count($allActivity) >0)
                               id="dataTables-example"
                                @endif
                        >
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Activity Name</th>
                                <th>Activity Image</th>
                                <th>Added By</th>
                                <th>Edited By</th>
                                <th>Created at</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1; ?>
                            @forelse($allActivity as $activity)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{!! $activity->name !!}</td>
                                    <td><img src="{!! asset($img_path.$activity->image) !!}" alt=""></td>
                                    <td>{{ $activity->getUserName($activity->added_by) }}</td>
                                    <td>{{ $activity->getUserName($activity->edited_by) }}</td>
                                    <td>{!! date('d F Y',strtotime($activity->created_at)) !!}</td>
                                    <td>
                                        <nobr>
                                            <a href="" class="btn btn-success" title="View"><i class="fa fa-search"></i></a>
                                            <a href="{{route('admin.activities.edit',$activity->id)}}" class="btn btn-info" title="View"><i class="fa fa-edit"></i></a>
                                            <a href="{{route('admin.activities.delete',$activity->id)}}" class="btn btn-danger" title="View"><i class="fa fa-trash"></i></a>
                                        </nobr>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                            @empty
                                <tr>
                                    <td colspan="9">No Activities yet.</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
        </section>
        <!-- /.content -->
    </div>
@stop

@section('scripts')
    @parent

@stop