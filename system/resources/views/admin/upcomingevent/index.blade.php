@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Upcoming Events
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Upcoming Events</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  All Upcoming Events
                            <a href="{{route('admin.upcomingevents.add')}}" class="pull-right"><i class="fa fa-plus"></i> Add New</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover"
                                       @if(count($allevents) >0)
                                       id="dataTables-example"
                                        @endif
                                >
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Amount</th>
                                        <th>Criteria</th>
                                        <th>Total Seats</th>
                                        <th>Image</th>
                                        <th>Added by</th>
                                        <th>Updated By</th>
                                        <th>Created At</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1; ?>
                                    @forelse($allevents as $events)
                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td>{!! $events->name !!}</td>
                                            <td>{!! $events->description !!}</td>
                                            <td>{!! $events->amount !!}</td>
                                            <td>{!! $events->criteria !!}</td>
                                            <td>{!! $events->seats !!}</td>
                                            <td><img class="img-responsive" src="{!! asset($img_path.$events->image) !!}" alt=""></td>
                                            <td>{!! $events->getUserName($events->added_by)!!}</td>
                                            <td>{!! $events->getUserName($events->updated_by) !!}</td>
                                            <td>{!! date('d F Y',strtotime($events->created_at)) !!}</td>
                                            <td>
                                                <nobr>
                                                    <a href="" class="btn btn-success" title="View"><i class="fa fa-search"></i></a>
                                                    <a href="{{route('admin.upcomingevents.edit',$events->id)}}" class="btn btn-info" title="View"><i class="fa fa-edit"></i></a>
                                                    <a href="{{route('admin.upcomingevents.delete',$events->id)}}" class="btn btn-danger" title="View"><i class="fa fa-trash"></i></a>
                                                </nobr>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    @empty
                                        <tr>
                                            <td colspan="9">No Activities yet.</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </section>
        <!-- /.content -->
    </div>
@stop

@section('scripts')
    @parent

@stop