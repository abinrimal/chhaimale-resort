<nav class="navbar navbar-expand-lg navbar-dark">
    <div class="container navigation">
        <a class="navbar-brand half-circle" href="{{route('index')}}"><img src="{{asset('assets\images\chhaimalelogo.png')}}"></a>
        <a class="navbar-brand-scroll" href="{{route('index')}}"><img src="{{asset('assets\images\chhaimale-logo.png')}}"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="block-menu ml-auto">
                <li><a href="{{route('index')}}">Home</a></li>
                <li><a href="{{route('room')}}">Rooms</a></li>
                <li><a href="{{route('event')}}">Event</a></li>
                <li><a href="{{route('resturant')}}">Restaurant</a></li>
                <li><a href="{{route('gallery')}}">Gallery</a></li>
                <li><a href="{{route('contact')}}">Contact Us</a></li>
            </ul>
        </div>
    </div>
</nav>