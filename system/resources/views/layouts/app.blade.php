<!DOCTYPE html>
<html lang="en">

<head>
    <title>Chhaimale Resort |The Pear Garden</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('assets\css\preloader.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets\css\main.css')}}">
    <link rel="stylesheet" href="{{asset('assets\css\animate.css')}}">
    <link href='https://fonts.googleapis.com/css?family=Bayon' rel='stylesheet'>
    <!-- Font Awesome  -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet">
    <!-- Font Awesome Ends -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets\css\nav.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets\css\tabnav.css')}}">
    <link rel="stylesheet" href="{{asset('assets\css\owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{'assets\css\slick.css'}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets\css\slick-theme.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets\css\hover-box.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets\css\3dhover.css')}}">
    <!-- Font Awesome  -->
    <!-- Font Awesome Ends -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" rel="stylesheet">

    <!-- Font Awesome  -->
    <!-- Font Awesome Ends -->

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>

    <!-- Font Awesome  -->
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
    <!-- Font Awesome Ends -->
    <link rel="stylesheet" href="{{asset('assets\css\jquery-steps.css')}}">
    <link rel="stylesheet" href="{{asset('assets\css\style.css')}}">
    <style type="text/css">
        body {
            counter-reset: my-sec-counter;
        }

        .step-steps a::before {
            counter-increment: my-sec-counter 1;
            content: counter(my-sec-counter) ". ";
        }
    </style>

    <!-- Font Awesome  -->
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
    <!-- Font Awesome Ends -->

    <!-- Font Awesome  -->
    <!-- Font Awesome Ends -->
    <link rel='stylesheet' href='{{asset('assets\css\unite-gallery.css')}}' type='text/css' />
    <!-- Font Awesome  -->
    <!-- Font Awesome Ends -->

    <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
    <!-- Font Awesome Ends -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css" />
    <!-- Font Awesome  -->
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
    <!-- Font Awesome Ends -->
    <!-- Font Awesome  -->
    <link href="https://fonts.googleapis.com/css?family=Didact+Gothic" rel="stylesheet">
    <!-- Font Awesome Ends -->
    <link rel="stylesheet" href="{{asset('assets\css\smartphoto.min.css')}}">
    <!-- Font Awesome  -->
    <link href="https://fonts.googleapis.com/css?family=Philosopher|Spirax" rel="stylesheet">
    <!-- Font Awesome Ends -->

    <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
    <style type="text/css">
        body {
            counter-reset: my-sec-counter;
        }

        .layout1 {

            background-image: url("{{asset('assets/images/conferencehall/u-shape.jpg')}}");
            border: 1px solid black;
        }

        .layout2 {
            background-image: url("{{asset('assets/images/conferencehall/conference.jpg')}}");
            border: 1px solid black;
        }

        .layout3 {
            background-image: url("{{asset('assets/images/conferencehall/hollow-square.jpg')}}");
            border: 1px solid black;
        }

        .layout4 {
            background-image: url("{{asset('assets/images/conferencehall/square.jpg')}}");
            border: 1px solid black;
        }

        .layout5 {
            background-image: url({{asset('assets/images/conferencehall/theater.jpg')}});
            border: 1px solid black;
        }

        .layout6 {
            background-image: url({{asset('assets/images/conferencehall/u-shape.jpg')}});
            border: 1px solid black;
        }

        .step-steps a::before {
            counter-increment: my-sec-counter 1;
            content: counter(my-sec-counter) ". ";
        }
    </style>
</head>
<body>
@include('layouts.header')

        @yield('content')

<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{asset('assets\js\main.js')}}"></script>
<script type="text/javascript" src="{{asset('assets\js\search.js')}}"></script>
<script type="text/javascript">
    function show1() {
        document.getElementById('card_detail').style.display = 'none';
    }

    function show2() {
        document.getElementById('card_detail').style.display = 'block';
    }
</script>

<script type='text/javascript' src='{{asset('assets\js\ug-theme-tiles.js')}}'></script>
<script type='text/javascript' src='{{asset('assets\js\unitegallery.min.js')}}'></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>


<script type="text/javascript">

    jQuery(document).ready(function(){

        jQuery("#gallery").unitegallery({
            tiles_type:"justified",
            tiles_justified_space_between:5,
            tiles_justified_row_height: 200,
            tile_enable_image_effect:true,
            tile_image_effect_reverse:true,
            tile_image_effect_type: "blur",
        });
    });

</script>

<script type="text/javascript">
    // Gallery Banner


    $(document).ready(function() {
        var onScroll = function() {
            var scrollTop = $(this).scrollTop();
            $('.gallery-banner').each(function(index, elem) {
                var $elem = $(elem);
                $elem.find('img').css('top', scrollTop - $elem.offset().top);
            });
        };
        onScroll.apply(window);
        $(window).on('scroll', onScroll);
    });

</script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>


<script type="text/javascript">

    jQuery(document).ready(function(){

        jQuery("#gallery").unitegallery({
            tiles_type:"justified",
            tiles_justified_space_between:5,
            tiles_justified_row_height: 200,
            tile_enable_image_effect:true,
            tile_image_effect_reverse:true,
            tile_image_effect_type: "blur",
        });
    });

</script>

<script type="text/javascript">
    // Gallery Banner


    $(document).ready(function() {
        var onScroll = function() {
            var scrollTop = $(this).scrollTop();
            $('.gallery-banner').each(function(index, elem) {
                var $elem = $(elem);
                $elem.find('img').css('top', scrollTop - $elem.offset().top);
            });
        };
        onScroll.apply(window);
        $(window).on('scroll', onScroll);
    });

</script>
<script src="{{asset('assets\js\owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets\js\slick.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets\js\jquery.bpopup.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets\js\jquery.hover3d.min.js')}}"></script>
<script src="{{asset('assets\js\wow.min.js')}}"></script>
<script>
    wow = new WOW({
        boxClass: 'wow', // default
        animateClass: 'animated', // default
        offset: 0, // default
        mobile: false, // default
        live: true // default
    })
    wow.init();
</script>
<script type="text/javascript">
    $(document).ready(function() {
        var onScroll = function() {
            var scrollTop = $(this).scrollTop();
            $('.banner-slider').each(function(index, elem) {
                var $elem = $(elem);
                $elem.find('img').css('top', scrollTop - $elem.offset().top);
            });
        };
        onScroll.apply(window);
        $(window).on('scroll', onScroll);
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {

        setTimeout(function() {
            $('body').addClass('loaded');
        }, 1000);

    });
</script>
<script type="text/javascript">
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        items: 4,
        loop: true,
        margin: 10,
        autoplay: true,
        autoplayTimeout: 2000,
        autoplayHoverPause: true
    });
    $('.play').on('click', function() {
        owl.trigger('play.owl.autoplay', [1000])
    })
    $('.stop').on('click', function() {
        owl.trigger('stop.owl.autoplay')
    })
</script>
<script type="text/javascript">
    $('.autoplay').slick({
        slidesToShow:4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
    });

</script>
<script>
    $(function() {
        setTimeout(function() {
            $('#popup_this').bPopup();
        }, 5000);
    });
</script>
<script>
    window.twttr = (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0],
            t = window.twttr || {};
        if (d.getElementById(id)) return t;
        js = d.createElement(s);
        js.id = id;
        js.src = "https://platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js, fjs);

        t._e = [];
        t.ready = function(f) {
            t._e.push(f);
        };

        return t;
    }(document, "script", "twitter-wjs"));
</script>
<script>
    $(document).ready(function() {
        $(".activite").hover3d({
            selector: ".activite_card"
        });

        $(".movie").hover3d({
            selector: ".movie__card",
            shine: true,
            sensitivity: 20,
        });
    });
</script>
<script type="text/javascript">
    $("#check_out").datepicker({
        minDate: new Date()
    });

    $("#check_in").datepicker({
        minDate: new Date(),
        onSelect: function(dateText, inst) {
            var selectedDate = $(this).datepicker("getDate");
            $("#check_out").datepicker("option", "minDate", selectedDate);
        }
    });

    $(function() {
        $("#check_in").datepicker({
            dateFormat: "yy-mm-dd"
        }).datepicker("setDate", "0");
    });

    $(function() {
        $("#check_out").datepicker({
            dateFormat: "yy-mm-dd"
        }).datepicker("setDate", "1");
    });
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script>
    new WOW().init();
</script>

<script type="text/javascript">
    $(".hover").mouseleave(
        function () {
            $(this).removeClass("hover");
        }
    );
</script>

<script type="text/javascript">
    $(document).ready(function() {
        var onScroll = function() {
            var scrollTop = $(this).scrollTop();
            $('.restuarant-banner').each(function(index, elem) {
                var $elem = $(elem);
                $elem.find('img').css('top', scrollTop - $elem.offset().top);
            });
        };
        onScroll.apply(window);
        $(window).on('scroll', onScroll);
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
<script src="{{asset('assets\js\jquery-steps.js')}}"></script>
<script>
    var meetingInfo = $('#meetingInfo');
    var meetingInfoValidator = meetingInfo.validate();

    var conferenceTime = $('#conferenceTime');
    var conferenceTimeValidator = conferenceTime.validate();

    var mealTime = $('#mealTime');
    var mealTimeValidator = mealTime.validate();


    var roomInfo = $('#roomInfo');
    var roomInfoValidator = roomInfo.validate();

    var usrInfo = $('#usrInfo');
    var usrInfoValidator = usrInfo.validate();

    $('#demo').steps({
        onChange: function(currentIndex, newIndex, stepDirection) {
            console.log('onChange', currentIndex, newIndex, stepDirection);
            // tab1
            if (currentIndex === 0) {
                if (stepDirection === 'forward') {
                    var valid = meetingInfo.valid();
                    return valid;
                }
                if (stepDirection === 'backward') {
                    meetingInfoValidator.resetForm();
                }
            }

            // tab2
            if (currentIndex === 1) {
                if (stepDirection === 'forward') {
                    var valid = conferenceTime.valid();
                    return valid;
                }
                if (stepDirection === 'backward') {
                    conferenceTimeValidator.resetForm();
                }
            }

            // tab3
            if (currentIndex === 2) {
                if (stepDirection === 'forward') {
                    var valid = mealTime.valid();
                    return valid;
                }
                if (stepDirection === 'backward') {
                    mealTimeValidator.resetForm();
                }
            }

            // tab4
            if (currentIndex === 3) {
                if (stepDirection === 'forward') {
                    var valid = roomInfo.valid();
                    return valid;
                }
                if (stepDirection === 'backward') {
                    roomInfoValidator.resetForm();
                }
            }

            if (currentIndex === 4) {
                if (stepDirection === 'forward') {
                    var valid = usrInfo.valid();
                    return valid;
                }
                if (stepDirection === 'backward') {
                    usrInfoValidator.resetForm();
                }
            }

            return true;

        },
        onFinish: function() {
            alert('Wizard Completed');
        }
    });





    // Date Picker for conference
    $("#conference_end").datepicker({
        minDate: new Date()
    });

    $("#conference_start").datepicker({
        minDate: new Date(),
        onSelect: function(dateText, inst) {
            var selectedDate = $(this).datepicker("getDate");
            $("#conference_end").datepicker("option", "minDate", selectedDate);
        }
    });

    // Display today date for conference start
    $(function() {
        $("#conference_start").datepicker({
            dateFormat: "yy-mm-dd"
        }).datepicker("setDate", "0");
    });


    // Display today date conference end
    $(function() {
        $("#conference_end").datepicker({
            dateFormat: "yy-mm-dd"
        }).datepicker("setDate", "0");
    });

    // Tabs 3 Check Box Appers
    $(function() {
        $("#breakfast").click(function() {
            if ($(this).is(":checked")) {
                $("#breakfasttime").show();
            } else {
                $("#breakfasttime").hide();
            }
        });
    });

    $(function() {
        $("#lunch").click(function() {
            if ($(this).is(":checked")) {
                $("#lunchtime").show();
            } else {
                $("#lunchtime").hide();
            }
        });
    });

    $(function() {
        $("#dinner").click(function() {
            if ($(this).is(":checked")) {
                $("#dinnertime").show();
            } else {
                $("#dinnertime").hide();
            }
        });
    });



    $(function() {
        $("#standard_room").click(function() {
            if ($(this).is(":checked")) {
                $("#standard_occupant").show();
                $("#standard_quantity").show();
            } else {
                $("#standard_occupant").hide();
                $("#standard_quantity").hide();
            }
        });
    });


    $(function() {
        $("#wooden_room").click(function() {
            if ($(this).is(":checked")) {
                $("#wooden_quantity").show();
                $("#wooden_occupant").show();
            } else {
                $("#wooden_quantity").hide();
                $("#wooden_occupant").hide();
            }
        });
    });


    $(function() {
        $("#tented_room").click(function() {
            if ($(this).is(":checked")) {
                $("#tented_quantity").show();
                $("#tented_occupant").show();
            } else {
                $("#tented_quantity").hide();
                $("#tented_occupant").hide();
            }
        });
    });


    $(function() {
        $("#group_room").click(function() {
            if ($(this).is(":checked")) {
                $("#group_quantity").show();
                $("#group_occupant").show();
            } else {
                $("#group_quantity").hide();
                $("#group_occupant").hide();
            }
        });
    });



    function show1() {
        document.getElementById('card_detail').style.display = 'none';
    }

    function show2() {
        document.getElementById('card_detail').style.display = 'block';
    }



    $(function() {
        $("#add_rooms").click(function() {
            if ($(this).is(":checked")) {
                $("#sleeping_rooms").show();
            } else {
                $("#sleeping_rooms").hide();
            }
        });
    });



    $(function() {
        $("#add_rooms").click(function() {
            if ($(this).is(":checked")) {
                $(".rooms_content").show();
            } else {
                $(".rooms_content").hide();
            }
        });
    });


</script>

<script type="text/javascript">
    $(document).ready(function() {
        var onScroll = function() {
            var scrollTop = $(this).scrollTop();
            $('.contactus-banner').each(function(index, elem) {
                var $elem = $(elem);
                $elem.find('img').css('top', scrollTop - $elem.offset().top);
            });
        };
        onScroll.apply(window);
        $(window).on('scroll', onScroll);
    });


    //Remove Parallax for mobile
</script>
<script type="text/javascript">

    $(document).ready(function() {
        var onScroll = function() {
            var scrollTop = $(this).scrollTop();
            $('.event-banner').each(function(index, elem) {
                var $elem = $(elem);
                $elem.find('img').css('top', scrollTop - $elem.offset().top);
            });
        };
        onScroll.apply(window);
        $(window).on('scroll', onScroll);
    });
</script>
<script>
    var eventInfo = $('#eventInfo');
    var eventInfoValidator = eventInfo.validate();

    var roomInfo = $('#roomInfo');
    var roomInfoValidator = roomInfo.validate()

    var package = $('#package');
    var packageValidator = package.validate();

    var mealTime = $('#mealTime');
    var mealTimeValidator = mealTime.validate();

    var usrInfo = $('#usrInfo');
    var usrInfoValidator = usrInfo.validate();

    $('#demo').steps({
        onChange: function(currentIndex, newIndex, stepDirection) {
            console.log('onChange', currentIndex, newIndex, stepDirection);
            // tab1
            if (currentIndex === 0) {
                if (stepDirection === 'forward') {
                    var valid = eventInfo.valid();
                    return valid;
                }
                if (stepDirection === 'backward') {
                    eventInfoValidator.resetForm();
                }
            }

            // tab2
            if (currentIndex === 1) {
                if (stepDirection === 'forward') {
                    var valid = roomInfo.valid();
                    return valid;
                }
                if (stepDirection === 'backward') {
                    roomInfoValidator.resetForm();
                }
            }

            // tab3
            if (currentIndex === 2) {
                if (stepDirection === 'forward') {
                    var valid = package.valid();
                    return valid;
                }
                if (stepDirection === 'backward') {
                    packageValidator.resetForm();
                }
            }


            if (currentIndex === 3) {
                if (stepDirection === 'forward') {
                    var valid = usrInfo.valid();
                    return valid;
                }
                if (stepDirection === 'backward') {
                    usrInfoValidator.resetForm();
                }
            }

            return true;

        },
        onFinish: function() {
            alert('Thank You for Booking');
            window.location.replace("index.html");
        }
    });





    // Date Picker for conference
    $("#event_end").datepicker({
        minDate: new Date()
    });

    $("#event_start").datepicker({
        minDate: new Date(),
        onSelect: function(dateText, inst) {
            var selectedDate = $(this).datepicker("getDate");
            $("#event_end").datepicker("option", "minDate", selectedDate);
        }
    });

    // Display today date for conference start
    $(function() {
        $("#event_start").datepicker({
            dateFormat: "yy-mm-dd"
        }).datepicker("setDate", "0");
    });


    // Display today date conference end
    $(function() {
        $("#event_end").datepicker({
            dateFormat: "yy-mm-dd"
        }).datepicker("setDate", "0");
    });

    // Tabs 3 Check Box Appers
    $(function() {
        $("#breakfast").click(function() {
            if ($(this).is(":checked")) {
                $("#breakfasttime").show();
            } else {
                $("#breakfasttime").hide();
            }
        });
    });

    $(function() {
        $("#lunch").click(function() {
            if ($(this).is(":checked")) {
                $("#lunchtime").show();
            } else {
                $("#lunchtime").hide();
            }
        });
    });

    $(function() {
        $("#dinner").click(function() {
            if ($(this).is(":checked")) {
                $("#dinnertime").show();
            } else {
                $("#dinnertime").hide();
            }
        });
    });

    $(function() {
        $("#standard_room").click(function() {
            if ($(this).is(":checked")) {
                $("#standard_occupant").show();
                $("#standard_quantity").show();
            } else {
                $("#standard_occupant").hide();
                $("#standard_quantity").hide();
            }
        });
    });


    $(function() {
        $("#wooden_room").click(function() {
            if ($(this).is(":checked")) {
                $("#wooden_quantity").show();
                $("#wooden_occupant").show();
            } else {
                $("#wooden_quantity").hide();
                $("#wooden_occupant").hide();
            }
        });
    });


    $(function() {
        $("#tented_room").click(function() {
            if ($(this).is(":checked")) {
                $("#tented_quantity").show();
                $("#tented_occupant").show();
            } else {
                $("#tented_quantity").hide();
                $("#tented_occupant").hide();
            }
        });
    });


    $(function() {
        $("#group_room").click(function() {
            if ($(this).is(":checked")) {
                $("#group_quantity").show();
                $("#group_occupant").show();
            } else {
                $("#group_quantity").hide();
                $("#group_occupant").hide();
            }
        });
    });

    function show1() {
        document.getElementById('card_detail').style.display = 'none';
    }

    function show2() {
        document.getElementById('card_detail').style.display = 'block';
    }



    $(function() {
        $("#add_rooms").click(function() {
            if ($(this).is(":checked")) {
                $("#sleeping_rooms").show();

            } else {
                $("#sleeping_rooms").hide();
            }
        });
    });



    $(function() {
        $("#add_rooms").click(function() {
            if ($(this).is(":checked")) {
                $(".rooms_content").show();
            } else {
                $(".rooms_content").hide();
            }
        });
    });



    $('#adult').on('change', function() {

        if (this.value == "other") {
            $('#adult_more').show();
        } else {

            $('#adult_more').hide();

        }
    });


    $('#child').on('change', function() {

        if (this.value == "other") {
            $('#child_more').show();
        } else {

            $('#child_more').hide();

        }
    });

    $('#children').on('change', function() {

        if (this.value == "other") {
            $('#children_more').show();
        } else {

            $('#children_more').hide();

        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        var onScroll = function() {
            var scrollTop = $(this).scrollTop();
            $('.event-banner').each(function(index, elem) {
                var $elem = $(elem);
                $elem.find('img').css('top', scrollTop - $elem.offset().top);
            });
        };
        onScroll.apply(window);
        $(window).on('scroll', onScroll);
    });
</script>
<script src="{{asset('assets\js\smartphoto.js?v=1')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var onScroll = function() {
            var scrollTop = $(this).scrollTop();
            $('.rooms-banner').each(function(index, elem) {
                var $elem = $(elem);
                $elem.find('img').css('top', scrollTop - $elem.offset().top);
            });
        };
        onScroll.apply(window);
        $(window).on('scroll', onScroll);
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

<script type="text/javascript">
    $( "#check-out" ).datepicker({
        minDate: new Date()
    });

    $( "#check-in" ).datepicker({
        minDate: new Date(),
        onSelect: function(dateText, inst) {
            var selectedDate = $( this ).datepicker( "getDate" );
            $( "#check-out" ).datepicker( "option", "minDate", selectedDate );
        }
    });

    $(function() {
        $("#check-in").datepicker({
            dateFormat: "yy-mm-dd"
        }).datepicker("setDate", "0");
    });

    $(function() {
        $("#check-out").datepicker({
            dateFormat: "yy-mm-dd"
        }).datepicker("setDate", "1");
    })
</script>
</body>
</html>