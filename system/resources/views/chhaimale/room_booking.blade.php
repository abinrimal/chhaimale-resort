@extends('layout.app')
@section('content')
    <!-- Navigation Ends -->
    <div class="content container" style="padding-top: 150px;">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-10">
                <div class="search_result">
                    <div class="heading">
                        <a href="{{route('search')}}"> Back </a>
                    </div>
                    <div class="header_content text-center">
                        <div class="content">
                            <div class="content__container">

                                <ul class="content__container__list">
                                    <li class="content__container__list__item">Free Internet Avaiable</li>
                                    <li class="content__container__list__item">Conference Hall </li>
                                    <li class="content__container__list__item">Special Chhaimale Food </li>
                                    <li class="content__container__list__item">Acoustic Friday Night</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="offer_room">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="image">
                                    <img src="{{asset('assets\images\standard-room.jpg')}}">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="room_description">
                                    <h4>Standard Room</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, recusandae, dolorum eveniet rerum ex quidem nam. Reprehenderit tenetur, soluta rerum nobis iusto aspernatur. Consectetur quis soluta cumque laboriosam tempora quasi.</p>
                                </div>


                            </div>
                            <div class="col-md-3">
                                <div class="price_details">
                                    <div class="card">
                                        <div class="card-header">
                                            Lowest Avaiable Rate
                                        </div>
                                        <div class="card-body text-center">
                                            <p class="card-text">Price Per Night
                                                <br> Rs 2000
                                            </p>
                                            <a href="{{route('roomdetail')}}" class="btn">Book Now</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Offer Rooms Ends -->

                    <!-- Rooms Search List -->
                    <div class="rooms_rate_list">
                        <div class="rooms_search">
                            <div class="date">
                                July 2000 - July 2010
                            </div>
                            <div class="card">

                                <!-- Card Heading One -->
                                <div class="card-header " role="tab" id="parentheadingOne">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionGroupClosed" href="#collapseCloseOne" aria-expanded="true" aria-controls="collapseCloseOne">
                                        Standard Rate
                                    </a>

                                </div>
                                <div id="collapseCloseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="parentheadingOne">
                                    <div class="card-body ">
                                        <ul class="list-group list-group-flush">

                                            <!-- Child Heading One -->
                                            <li class="list-group-item" role="tab" id="childheadingOne">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionGroupClosed" href="#collapseCloseChildOne" aria-expanded="true" aria-controls="collapseCloseOne">
                                                            Standard Room
                                                        </a>
                                                        <p class="policy_text"><i class="fa fa-check"></i> Free Cancellation</p>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="price_details">
                                                            <div class="rate inline">
                                                                <p>Rs 11,000</p>
                                                            </div>

                                                            <div class="booknow inline">
                                                                <a href="{{route('roomdetail')}}" class="btn"> Book NOw</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>




                                                <div id="collapseCloseChildOne" class="child-collapse collapse" role="tabpanel" aria-labelledby="childheadingOne">
                                                    <div class="title">
                                                        <h2>Standard Room</h2>
                                                    </div>

                                                    <div class="room_details">
                                                        <h4>Rooms Description</h4>
                                                        <p>Bright Morning Pillowtop Beds</p>
                                                        <p> Convenient Work Desk</p>
                                                        <p>Separate Area w/Sleeper Sofa</p>
                                                        <p>Microwave & Mini Refrigerator</p>
                                                        <p>Coffee Maker</p>
                                                        <p>Hairdryer</p>
                                                        <p>Iron w/Ironing Board</p>
                                                        <p>Free Local Calls</p>
                                                        <p>Alarm Clock</p>

                                                        <a href="{{route('roomdetail')}}" class="btn"> Book Now</a>
                                                    </div>

                                                </div>
                                            </li>

                                            <!-- Child Heading One Ends -->

                                            <!-- Child Heading Two Ends -->
                                            <li class="list-group-item" role="tab" id="childheadingTwo">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionGroupClosed" href="#collapseCloseChildTwo" aria-expanded="true" aria-controls="collapseCloseTwo">
                                                            Wooden Room
                                                        </a>
                                                        <p class="policy_text"><i class="fa fa-check"></i> Free Cancellation</p>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="price_details">
                                                            <div class="rate inline">
                                                                <p>Rs 15,000</p>
                                                            </div>

                                                            <div class="booknow inline">
                                                                <a href="#" class="btn"> Book NOw</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>




                                                <div id="collapseCloseChildTwo" class="child-collapse collapse" role="tabpanel" aria-labelledby="childheadingTwo">
                                                    <div class="title">
                                                        <h2>Wooden Room</h2>
                                                    </div>

                                                    <div class="room_details">
                                                        <h4>Rooms Description</h4>
                                                        <p>Bright Morning Pillowtop Beds</p>
                                                        <p> Convenient Work Desk</p>
                                                        <p>Separate Area w/Sleeper Sofa</p>
                                                        <p>Microwave & Mini Refrigerator</p>
                                                        <p>Coffee Maker</p>
                                                        <p>Hairdryer</p>
                                                        <p>Iron w/Ironing Board</p>
                                                        <p>Free Local Calls</p>
                                                        <p>Alarm Clock</p>

                                                        <a href="#" class="btn"> Book Now</a>
                                                    </div>

                                                </div>
                                            </li>

                                            <!-- Child Heading Two Ends -->


                                        </ul>

                                        <!-- Card Headingone Ends -->
                                    </div>

                                    <!-- Card Body Ends -->
                                </div>

                                <!-- Card One Ends -->

                                <!-- Card Two Begain -->

                                <div class="card-header " role="tab" id="parentheadingTwo">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionGroupClosed" href="#collapseCloseTwo" aria-expanded="true" aria-controls="collapseCloseTwo">
                                        Privileged Member Rate, Save 15% <span class="text"> Average Nightly Rate starting from 11,641NPR</span>
                                    </a>
                                </div>

                                <div id="collapseCloseTwo" class="card-collapse collapse" role="tabpanel" aria-labelledby="parentheadingTwo">
                                    <div class="card-body ">
                                        <ul class="list-group list-group-flush">

                                            <!-- Child  one -->
                                            <li class="list-group-item" role="tab" id="childheadingOne">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionGroupClosed" href="#collapseCloseprivilegedchildOne" aria-expanded="true" aria-controls="collapseCloseTwo">
                                                            Standard Room
                                                        </a>
                                                        <p class="policy_text"><i class="fa fa-check"></i> Free Cancellation</p>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="price_details">
                                                            <div class="strike-rate inline">
                                                                <strike> Rs 15000</strike>

                                                            </div>

                                                            <div class="rate inline">
                                                                <p>Rs 11,000</p>
                                                            </div>

                                                            <div class="booknow inline">
                                                                <a href="{{route('roomdetail')}}" class="btn"> Book NOw</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>




                                                <div id="collapseCloseprivilegedchildOne" class="child-collapse collapse" role="tabpanel" aria-labelledby="childheadingTwo">
                                                    <div class="title">
                                                        <h2>Wooden Room</h2>
                                                    </div>

                                                    <div class="room_details">
                                                        <h4>Rooms Description</h4>
                                                        <p>Bright Morning Pillowtop Beds</p>
                                                        <p> Convenient Work Desk</p>
                                                        <p>Separate Area w/Sleeper Sofa</p>
                                                        <p>Microwave & Mini Refrigerator</p>
                                                        <p>Coffee Maker</p>
                                                        <p>Hairdryer</p>
                                                        <p>Iron w/Ironing Board</p>
                                                        <p>Free Local Calls</p>
                                                        <p>Alarm Clock</p>

                                                        <a href="{{route('roomdetail')}}" class="btn"> Book Now</a>
                                                    </div>

                                                </div>
                                            </li>

                                            <!-- Child Heading One Ends -->

                                            <!-- Child Heading Two Ends -->
                                            <li class="list-group-item" role="tab" id="childheadingTwo">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionGroupClosed" href="#collapseCloseprivilegedchildTwo" aria-expanded="true" aria-controls="collapseCloseTwo">
                                                            Wooden Room
                                                        </a>
                                                        <p class="policy_text"><i class="fa fa-check"></i> Free Cancellation</p>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="price_details">
                                                            <div class="strike-rate inline">
                                                                <strike> Rs 15000</strike>
                                                            </div>
                                                            <div class="rate inline">
                                                                <p>Rs 15,000</p>
                                                            </div>

                                                            <div class="booknow inline">
                                                                <a href="{{route('roomdetail')}}" class="btn"> Book NOw</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>




                                                <div id="collapseCloseprivilegedchildTwo" class="child-collapse collapse" role="tabpanel" aria-labelledby="childheadingTwo">
                                                    <div class="title">
                                                        <h2>Wooden Room</h2>
                                                    </div>

                                                    <div class="room_details">
                                                        <h4>Rooms Description</h4>
                                                        <p>Bright Morning Pillowtop Beds</p>
                                                        <p> Convenient Work Desk</p>
                                                        <p>Separate Area w/Sleeper Sofa</p>
                                                        <p>Microwave & Mini Refrigerator</p>
                                                        <p>Coffee Maker</p>
                                                        <p>Hairdryer</p>
                                                        <p>Iron w/Ironing Board</p>
                                                        <p>Free Local Calls</p>
                                                        <p>Alarm Clock</p>

                                                        <a href="{{route('roomdetail')}}" class="btn"> Book Now</a>
                                                    </div>

                                                </div>
                                            </li>

                                            <!-- Child Heading Two Ends -->


                                        </ul>

                                        <!-- Card Headingone Ends -->
                                    </div>

                                    <!-- Card Body Ends -->
                                </div>

                                <!-- Card Two Ends -->


                                <!-- Card Three Begain -->

                                <div class="card-header " role="tab" id="parentheadingTwo">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionGroupClosed" href="#collapseCloseThree" aria-expanded="true" aria-controls="collapseCloseThree">
                                        Advance Purchase, Save 15% <span class="text"> Average Nightly Rate starting from 11,641NPR</span>
                                    </a>
                                </div>

                                <div id="collapseCloseThree" class="card-collapse collapse" role="tabpanel" aria-labelledby="parentheadingTwo">
                                    <div class="card-body ">
                                        <ul class="list-group list-group-flush">

                                            <!-- Child  one -->
                                            <li class="list-group-item" role="tab" id="childheadingOne">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionGroupClosed" href="#collapseCloseAdvancechildOne" aria-expanded="true" aria-controls="collapseCloseThree">
                                                            Standard Room
                                                        </a>

                                                        <p class="policy_text"><i class="fa fa-close"></i> Free Cancellation</p>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="price_details">
                                                            <div class="strike-rate inline">
                                                                <strike> Rs 15000</strike>

                                                            </div>

                                                            <div class="rate inline">
                                                                <p>Rs 11,000</p>
                                                            </div>

                                                            <div class="booknow inline">
                                                                <a href="{{route('roomdetail')}}" class="btn"> Book NOw</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>




                                                <div id="collapseCloseAdvancechildOne" class="child-collapse collapse" role="tabpanel" aria-labelledby="childheadingTwo">
                                                    <div class="title">
                                                        <h2>Standard Room</h2>
                                                    </div>

                                                    <div class="room_details">
                                                        <h4>Rooms Description</h4>
                                                        <p>Bright Morning Pillowtop Beds</p>
                                                        <p> Convenient Work Desk</p>
                                                        <p>Separate Area w/Sleeper Sofa</p>
                                                        <p>Microwave & Mini Refrigerator</p>
                                                        <p>Coffee Maker</p>
                                                        <p>Hairdryer</p>
                                                        <p>Iron w/Ironing Board</p>
                                                        <p>Free Local Calls</p>
                                                        <p>Alarm Clock</p>

                                                        <a href="{{route('roomdetail')}}" class="btn"> Book Now</a>
                                                    </div>

                                                </div>
                                            </li>

                                            <!-- Child Heading One Ends -->

                                            <!-- Child Heading Two Ends -->
                                            <li class="list-group-item" role="tab" id="childheadingTwo">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionGroupClosed" href="#collapseCloseAdvancechildTwo" aria-expanded="true" aria-controls="collapseCloseTwo">
                                                            Wooden Room
                                                        </a>

                                                        <p class="policy_text"><i class="fa fa-close"></i> Free Cancellation</p>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="price_details">
                                                            <div class="strike-rate inline">
                                                                <strike> Rs 15000</strike>
                                                            </div>
                                                            <div class="rate inline">
                                                                <p>Rs 15,000</p>
                                                            </div>

                                                            <div class="booknow inline">
                                                                <a href="{{route('roomdetail')}}" class="btn"> Book NOw</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div id="collapseCloseAdvancechildTwo" class="child-collapse collapse" role="tabpanel" aria-labelledby="childheadingTwo">
                                                    <div class="title">
                                                        <h2>Wooden Rooms</h2>
                                                    </div>

                                                    <div class="room_details">
                                                        <h4>Rooms Description</h4>
                                                        <p>Bright Morning Pillowtop Beds</p>
                                                        <p> Convenient Work Desk</p>
                                                        <p>Separate Area w/Sleeper Sofa</p>
                                                        <p>Microwave & Mini Refrigerator</p>
                                                        <p>Coffee Maker</p>
                                                        <p>Hairdryer</p>
                                                        <p>Iron w/Ironing Board</p>
                                                        <p>Free Local Calls</p>
                                                        <p>Alarm Clock</p>

                                                        <a href="{{route('roomdetail')}}" class="btn"> Book Now</a>
                                                    </div>

                                                </div>
                                            </li>

                                            <!-- Child Heading Two Ends -->


                                        </ul>

                                        <!-- Card Headingone Ends -->
                                    </div>

                                    <!-- Card Body Ends -->
                                </div>
                                <!-- Card Three Ends -->
                            </div>
                        </div>
                    </div>

                    <!-- Rooms Search ends -->
                </div>
                <!-- Search Result Ends -->

                <div class="terms">
                    <p>* Average Nightly Rate - Additional taxes and surcharges may apply. Total estimated cost is only available in hotel currency.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora totam nihil maxime dolore reiciendis. Amet, repellat, minima. Sint ipsam ratione libero facilis illum voluptatibus aut beatae laboriosam quae dignissimos, sed?</p>
                </div>

            </div>
            <!-- Col-md-10 Ends -->
        </div>
        <!-- Row Ends -->

        <!-- Content Ends -->
    </div>


    <div id="subscribe">
        <div class="container background">
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text">
                                <p>Subscribe to our  breif newsletter to get exclusive discounts and new events right in your inbox</p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <form>
                                <p><input class="form-control" name="email" required="" type="email" placeholder="Enter Your Email Address" style="outline:0;"></p>
                                <p><input class="btn btn-success" type="submit" value="Subscribe"></p>
                            </form>
                        </div>
                    </div>
                </div>


                <div class="col-md-4">
                    <div class="followus text-center">
                        <div class="title">
                            <h5>Follow Us</h5>
                        </div>
                        <a href="https://www.facebook.com/chhaimaleresort/" class="btn btn-fb"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="btn btn-tweet"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="btn btn-googleplus"><i class="fa fa-google-plus"></i></a>
                        <a href="#" class="btn btn-insta"><i class="fa fa-instagram"></i></a>

                    </div>

                </div>
            </div>
        </div>



        <footer class="footer-content container">
            <div class="container">
                <div class="row">

                    <div class="col-md-3">
                        <div class="footer-logo">
                            <img src="{{asset('assets\images\footer-logo.png')}}">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="text-wrapper">
                            <table>
                                <tbody>
                                <td> <i class="fa fa-map-marker"></i></td>
                                <td> <p> Ramche Bhanjyang-6,<br>Kathmandu</p></td>

                                </tbody>
                            </table>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="text-wrapper">
                            <table>
                                <tbody>
                                <td> <i class="fa fa-phone"></i></td>
                                <td><p> +977-016924909<br>+977-9851181409</p></td>

                                </tbody>
                            </table>
                        </div>


                    </div>

                    <div class="col-md-3">
                        <div class="text-wrapper">
                            <table>
                                <tbody>
                                <td> <i class="fa fa-envelope icon"></i></td>
                                <td> <p>info@chhaimaleresort.com.np<br>
                                        chhaimale.resort@gmail.com
                                    </p></td>
                                </tbody>
                            </table>
                        </div>
                    </div>





                    <div class="col-md-4">
                        <div class="copyright text-center">
                            <p>Copyright © <span class="name">Chhaimale Resort</span>  <script>document.write(new Date().getFullYear());</script> &nbsp;All rights reserved. </p>
                        </div>

                    </div>

                    <div class="col-md-4">
                        <div class="policy">
                            <ul>
                                <li><a href="#">Privacy</a></li>
                                <li><a href="#">Terms & Conditions</a></li>
                                <li><a href="#">Help & Support</a></li>
                            </ul>
                        </div>

                    </div>

                    <div class="col-md-4">

                        <div class="Developedby text-center">
                            <p>Developed with <img src="{{asset('assets\images\icon\love.gif')}}" width="20px" height="20px;"> <a href="https://prabidhilabs.com/">Prabidhi Labs</a></p>
                        </div>

                    </div>
                </div>





            </div>
            <!-- Copy Right Content -->

    </div>
    </footer>
@endsection