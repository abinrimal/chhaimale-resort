@extends('layout.app')
@section('content')
    <div class="content container" style="padding-bottom: 50px;">
        <section class="container">
            <div class="gallery-banner">
                <img src="{{asset('assets\images\banner3.jpg')}}">
                <div class="text-wrapper col-md-8 offset-md-2 text-center">
                    <h1 class="animated zoomIn"><strong> Chhaimale Gallery</strong></h1><br>
                </div>
            </div>
        </section>


        <div class="container">

            <div id="gallery" class="animated fadeInUp";">


            <a><img alt="" src="{{asset('assets\images\boating.jpg')}}" data-image="images/boating.jpg"></a>
            <a><img alt="" src="{{asset('assets\images\bar.jpg')}}" data-image="images/bar.jpg"></a>
            <a><img alt="" src="{{asset('assets\images\conference-hall.jpg')}}" data-image="images/conference-hall.jpg" ></a>
            <a><img alt="" src="{{asset('assets\images\drink.jpg')}}" data-image="images/drink.jpg"></a>
            <a><img alt="" src="{{asset('assets\images\deluxe-room.jpg')}}" data-image="images/deluxe-room.jpg" ></a>
            <a><img alt="" src="{{asset('assets\images\family-room.jpg')}}" data-image="images/family-room.jpg"></a>
            <a><img alt="" src="{{asset('assets\images\homestay.jpg')}}" data-image="images/homestay.jpg" ></a>
            <a><img alt="" src="{{asset('assets\images\banner1.jpg')}}" data-image="images/banner1.jpg"></a>
            <a><img alt="" src="{{asset('assets\images\wedding.jpg')}}" data-image="images/wedding.jpg"></a>
            <a><img alt="" src="{{asset('assets\images\deluxe-room.jpg')}}" data-image="images/deluxe-room.jpg" ></a>
            <a><img alt="" src="{{asset('assets\images\family-room.jpg')}}" data-image="images/family-room.jpg"></a>
            <a><img alt="" src="{{asset('assets\images\thakali.jpg')}}" data-image="images/thakali.jpg"></a>
            <a><img alt="" src="{{asset('assets\images\boating.jpg')}}" data-image="images/boating.jpg"></a>
            <a><img alt="" src="{{asset('assets\images\bar.jpg')}}" data-image="images/bar.jpg"></a>
            <a><img alt="" src="{{asset('assets\images\conference-hall.jpg')}}" data-image="images/conference-hall.jpg"></a>



        </div>

    </div>
    </div>

    <div id="subscribe">
        <div class="container background">
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text">
                                <p>Subscribe to our  breif newsletter to get exclusive discounts and new events right in your inbox</p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <form>
                                <p><input class="form-control" name="email" required="" type="email" placeholder="Enter Your Email Address" style="outline:0;"></p>
                                <p><input class="btn btn-success" type="submit" value="Subscribe"></p>
                            </form>
                        </div>
                    </div>
                </div>


                <div class="col-md-4">
                    <div class="followus text-center">
                        <div class="title">
                            <h5>Follow Us</h5>
                        </div>
                        <a href="https://www.facebook.com/chhaimaleresort/" class="btn btn-fb"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="btn btn-tweet"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="btn btn-googleplus"><i class="fa fa-google-plus"></i></a>
                        <a href="#" class="btn btn-insta"><i class="fa fa-instagram"></i></a>

                    </div>

                </div>
            </div>
        </div>



        <footer class="footer-content container">
            <div class="container">
                <div class="row">

                    <div class="col-md-3">
                        <div class="footer-logo">
                            <img src="{{asset('assets\images\footer-logo.png')}}">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="text-wrapper">
                            <table>
                                <tbody>
                                <td> <i class="fa fa-map-marker"></i></td>
                                <td> <p> Ramche Bhanjyang-6,<br>Kathmandu</p></td>

                                </tbody>
                            </table>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="text-wrapper">
                            <table>
                                <tbody>
                                <td> <i class="fa fa-phone"></i></td>
                                <td><p> +977-016924909<br>+977-9851181409</p></td>

                                </tbody>
                            </table>
                        </div>


                    </div>

                    <div class="col-md-3">
                        <div class="text-wrapper">
                            <table>
                                <tbody>
                                <td> <i class="fa fa-envelope icon"></i></td>
                                <td> <p>info@chhaimaleresort.com.np<br>
                                        chhaimale.resort@gmail.com
                                    </p></td>
                                </tbody>
                            </table>
                        </div>
                    </div>





                    <div class="col-md-4">
                        <div class="copyright text-center">
                            <p>Copyright © <span class="name">Chhaimale Resort</span>  <script>document.write(new Date().getFullYear());</script> &nbsp;All rights reserved. </p>
                        </div>

                    </div>

                    <div class="col-md-4">
                        <div class="policy">
                            <ul>
                                <li><a href="#">Privacy</a></li>
                                <li><a href="#">Terms & Conditions</a></li>
                                <li><a href="#">Help & Support</a></li>
                            </ul>
                        </div>

                    </div>

                    <div class="col-md-4">

                        <div class="Developedby text-center">
                            <p>Developed with <img src="{{asset('assets\images\icon\love.gif')}}" width="20px" height="20px;"> <a href="https://prabidhilabs.com/">Prabidhi Labs</a></p>
                        </div>

                    </div>
                </div>





            </div>
            <!-- Copy Right Content -->

    </div>
    </footer>

@endsection