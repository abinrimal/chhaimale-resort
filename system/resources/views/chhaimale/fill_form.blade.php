@extends('layout.app')
@section('content')
    <div class="container content" style="padding-top: 150px">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-10">
                <div class="rate_details">
                    <div class="heading">
                        <a href="room_booking_details.html">
                            < Back to Search Result</a>
                    </div>
                    <div class="header_content text-center">
                        <div class="content">
                            <div class="content__container">

                                <ul class="content__container__list">
                                    <li class="content__container__list__item">Free Internet Avaiable</li>
                                    <li class="content__container__list__item">Conference Hall </li>
                                    <li class="content__container__list__item">Special Chhaimale Food </li>
                                    <li class="content__container__list__item">Acoustic Friday Night</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="book_room">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="room_selected">
                                    <table>
                                        <tr>
                                            <td><img src="{{asset('assets\images\standard-room.jpg')}}"></td>
                                            <td>Standard Room
                                                <br> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime illo ipsum dolor ea quos architecto, neque, itaque doloremque delectus iure explicabo, cumque voluptatum iste repudiandae suscipit obcaecati non.
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Review Booking Ends -->
                </div>
                <!-- Card Details one  -->
                <div class="card">
                    <div class="card-header">
                        <span class="number">1.</span>Enter Your Personal Information
                    </div>
                    <div class="card-body">
                        <form name="usrInfo" id="usrInfo">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="firstname">Title<span class="asterisk">*</span></label>
                                        <select name="title" class="form-control" required="">
                                            <option value="mr">Mr</option>
                                            <option value="ms">Ms</option>
                                            <option value="mrs">Mrs</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="firstname">First Name <span class="asterisk">*</span></label>
                                        <input type="text" class="form-control" name="firstname" required="">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="lastname">L ast Name<span class="asterisk">*</span></label>
                                        <input type="text" class="form-control" name="lastname" required="">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="addressline1">Address Line 1 <span class="asterisk">*</span></label>
                                        <input type="text" class="form-control" name="addressline1" required="">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="addressline2">Address Line 2</label>
                                        <input type="text" class="form-control" name="addressline2">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="city">City <span class="asterisk">*</span></label>
                                        <input type="text" class="form-control" name="city">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="postalcode">Zip/Postal Code</label>
                                        <input type="text" class="form-control" name="postalcode">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="country">Country</label>
                                        <select class="form-control">
                                            <option value="NP">Nepal</option>
                                            <option value="AF">Afghanistan</option>
                                            <option value="AX">Åland Islands</option>
                                            <option value="AL">Albania</option>
                                            <option value="DZ">Algeria</option>
                                            <option value="AS">American Samoa</option>
                                            <option value="AD">Andorra</option>
                                            <option value="AO">Angola</option>
                                            <option value="AI">Anguilla</option>
                                            <option value="AQ">Antarctica</option>
                                            <option value="AG">Antigua and Barbuda</option>
                                            <option value="AR">Argentina</option>
                                            <option value="AM">Armenia</option>
                                            <option value="AW">Aruba</option>
                                            <option value="AU">Australia</option>
                                            <option value="AT">Austria</option>
                                            <option value="AZ">Azerbaijan</option>
                                            <option value="BS">Bahamas</option>
                                            <option value="BH">Bahrain</option>
                                            <option value="BD">Bangladesh</option>
                                            <option value="BB">Barbados</option>
                                            <option value="BY">Belarus</option>
                                            <option value="BE">Belgium</option>
                                            <option value="BZ">Belize</option>
                                            <option value="BJ">Benin</option>
                                            <option value="BM">Bermuda</option>
                                            <option value="BT">Bhutan</option>
                                            <option value="BO">Bolivia, Plurinational State of</option>
                                            <option value="BQ">Bonaire, Sint Eustatius and Saba</option>
                                            <option value="BA">Bosnia and Herzegovina</option>
                                            <option value="BW">Botswana</option>
                                            <option value="BV">Bouvet Island</option>
                                            <option value="BR">Brazil</option>
                                            <option value="IO">British Indian Ocean Territory</option>
                                            <option value="BN">Brunei Darussalam</option>
                                            <option value="BG">Bulgaria</option>
                                            <option value="BF">Burkina Faso</option>
                                            <option value="BI">Burundi</option>
                                            <option value="KH">Cambodia</option>
                                            <option value="CM">Cameroon</option>
                                            <option value="CA">Canada</option>
                                            <option value="CV">Cape Verde</option>
                                            <option value="KY">Cayman Islands</option>
                                            <option value="CF">Central African Republic</option>
                                            <option value="TD">Chad</option>
                                            <option value="CL">Chile</option>
                                            <option value="CN">China</option>
                                            <option value="CX">Christmas Island</option>
                                            <option value="CC">Cocos (Keeling) Islands</option>
                                            <option value="CO">Colombia</option>
                                            <option value="KM">Comoros</option>
                                            <option value="CG">Congo</option>
                                            <option value="CD">Congo, the Democratic Republic of the</option>
                                            <option value="CK">Cook Islands</option>
                                            <option value="CR">Costa Rica</option>
                                            <option value="CI">Côte d'Ivoire</option>
                                            <option value="HR">Croatia</option>
                                            <option value="CU">Cuba</option>
                                            <option value="CW">Curaçao</option>
                                            <option value="CY">Cyprus</option>
                                            <option value="CZ">Czech Republic</option>
                                            <option value="DK">Denmark</option>
                                            <option value="DJ">Djibouti</option>
                                            <option value="DM">Dominica</option>
                                            <option value="DO">Dominican Republic</option>
                                            <option value="EC">Ecuador</option>
                                            <option value="EG">Egypt</option>
                                            <option value="SV">El Salvador</option>
                                            <option value="GQ">Equatorial Guinea</option>
                                            <option value="ER">Eritrea</option>
                                            <option value="EE">Estonia</option>
                                            <option value="ET">Ethiopia</option>
                                            <option value="FK">Falkland Islands (Malvinas)</option>
                                            <option value="FO">Faroe Islands</option>
                                            <option value="FJ">Fiji</option>
                                            <option value="FI">Finland</option>
                                            <option value="FR">France</option>
                                            <option value="GF">French Guiana</option>
                                            <option value="PF">French Polynesia</option>
                                            <option value="TF">French Southern Territories</option>
                                            <option value="GA">Gabon</option>
                                            <option value="GM">Gambia</option>
                                            <option value="GE">Georgia</option>
                                            <option value="DE">Germany</option>
                                            <option value="GH">Ghana</option>
                                            <option value="GI">Gibraltar</option>
                                            <option value="GR">Greece</option>
                                            <option value="GL">Greenland</option>
                                            <option value="GD">Grenada</option>
                                            <option value="GP">Guadeloupe</option>
                                            <option value="GU">Guam</option>
                                            <option value="GT">Guatemala</option>
                                            <option value="GG">Guernsey</option>
                                            <option value="GN">Guinea</option>
                                            <option value="GW">Guinea-Bissau</option>
                                            <option value="GY">Guyana</option>
                                            <option value="HT">Haiti</option>
                                            <option value="HM">Heard Island and McDonald Islands</option>
                                            <option value="VA">Holy See (Vatican City State)</option>
                                            <option value="HN">Honduras</option>
                                            <option value="HK">Hong Kong</option>
                                            <option value="HU">Hungary</option>
                                            <option value="IS">Iceland</option>
                                            <option value="IN">India</option>
                                            <option value="ID">Indonesia</option>
                                            <option value="IR">Iran, Islamic Republic of</option>
                                            <option value="IQ">Iraq</option>
                                            <option value="IE">Ireland</option>
                                            <option value="IM">Isle of Man</option>
                                            <option value="IL">Israel</option>
                                            <option value="IT">Italy</option>
                                            <option value="JM">Jamaica</option>
                                            <option value="JP">Japan</option>
                                            <option value="JE">Jersey</option>
                                            <option value="JO">Jordan</option>
                                            <option value="KZ">Kazakhstan</option>
                                            <option value="KE">Kenya</option>
                                            <option value="KI">Kiribati</option>
                                            <option value="KP">Korea, Democratic People's Republic of</option>
                                            <option value="KR">Korea, Republic of</option>
                                            <option value="KW">Kuwait</option>
                                            <option value="KG">Kyrgyzstan</option>
                                            <option value="LA">Lao People's Democratic Republic</option>
                                            <option value="LV">Latvia</option>
                                            <option value="LB">Lebanon</option>
                                            <option value="LS">Lesotho</option>
                                            <option value="LR">Liberia</option>
                                            <option value="LY">Libya</option>
                                            <option value="LI">Liechtenstein</option>
                                            <option value="LT">Lithuania</option>
                                            <option value="LU">Luxembourg</option>
                                            <option value="MO">Macao</option>
                                            <option value="MK">Macedonia, the former Yugoslav Republic of</option>
                                            <option value="MG">Madagascar</option>
                                            <option value="MW">Malawi</option>
                                            <option value="MY">Malaysia</option>
                                            <option value="MV">Maldives</option>
                                            <option value="ML">Mali</option>
                                            <option value="MT">Malta</option>
                                            <option value="MH">Marshall Islands</option>
                                            <option value="MQ">Martinique</option>
                                            <option value="MR">Mauritania</option>
                                            <option value="MU">Mauritius</option>
                                            <option value="YT">Mayotte</option>
                                            <option value="MX">Mexico</option>
                                            <option value="FM">Micronesia, Federated States of</option>
                                            <option value="MD">Moldova, Republic of</option>
                                            <option value="MC">Monaco</option>
                                            <option value="MN">Mongolia</option>
                                            <option value="ME">Montenegro</option>
                                            <option value="MS">Montserrat</option>
                                            <option value="MA">Morocco</option>
                                            <option value="MZ">Mozambique</option>
                                            <option value="MM">Myanmar</option>
                                            <option value="NA">Namibia</option>
                                            <option value="NR">Nauru</option>
                                            <option value="NL">Netherlands</option>
                                            <option value="NC">New Caledonia</option>
                                            <option value="NZ">New Zealand</option>
                                            <option value="NI">Nicaragua</option>
                                            <option value="NE">Niger</option>
                                            <option value="NG">Nigeria</option>
                                            <option value="NU">Niue</option>
                                            <option value="NF">Norfolk Island</option>
                                            <option value="MP">Northern Mariana Islands</option>
                                            <option value="NO">Norway</option>
                                            <option value="OM">Oman</option>
                                            <option value="PK">Pakistan</option>
                                            <option value="PW">Palau</option>
                                            <option value="PS">Palestinian Territory, Occupied</option>
                                            <option value="PA">Panama</option>
                                            <option value="PG">Papua New Guinea</option>
                                            <option value="PY">Paraguay</option>
                                            <option value="PE">Peru</option>
                                            <option value="PH">Philippines</option>
                                            <option value="PN">Pitcairn</option>
                                            <option value="PL">Poland</option>
                                            <option value="PT">Portugal</option>
                                            <option value="PR">Puerto Rico</option>
                                            <option value="QA">Qatar</option>
                                            <option value="RE">Réunion</option>
                                            <option value="RO">Romania</option>
                                            <option value="RU">Russian Federation</option>
                                            <option value="RW">Rwanda</option>
                                            <option value="BL">Saint Barthélemy</option>
                                            <option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
                                            <option value="KN">Saint Kitts and Nevis</option>
                                            <option value="LC">Saint Lucia</option>
                                            <option value="MF">Saint Martin (French part)</option>
                                            <option value="PM">Saint Pierre and Miquelon</option>
                                            <option value="VC">Saint Vincent and the Grenadines</option>
                                            <option value="WS">Samoa</option>
                                            <option value="SM">San Marino</option>
                                            <option value="ST">Sao Tome and Principe</option>
                                            <option value="SA">Saudi Arabia</option>
                                            <option value="SN">Senegal</option>
                                            <option value="RS">Serbia</option>
                                            <option value="SC">Seychelles</option>
                                            <option value="SL">Sierra Leone</option>
                                            <option value="SG">Singapore</option>
                                            <option value="SX">Sint Maarten (Dutch part)</option>
                                            <option value="SK">Slovakia</option>
                                            <option value="SI">Slovenia</option>
                                            <option value="SB">Solomon Islands</option>
                                            <option value="SO">Somalia</option>
                                            <option value="ZA">South Africa</option>
                                            <option value="GS">South Georgia and the South Sandwich Islands</option>
                                            <option value="SS">South Sudan</option>
                                            <option value="ES">Spain</option>
                                            <option value="LK">Sri Lanka</option>
                                            <option value="SD">Sudan</option>
                                            <option value="SR">Suriname</option>
                                            <option value="SJ">Svalbard and Jan Mayen</option>
                                            <option value="SZ">Swaziland</option>
                                            <option value="SE">Sweden</option>
                                            <option value="CH">Switzerland</option>
                                            <option value="SY">Syrian Arab Republic</option>
                                            <option value="TW">Taiwan, Province of China</option>
                                            <option value="TJ">Tajikistan</option>
                                            <option value="TZ">Tanzania, United Republic of</option>
                                            <option value="TH">Thailand</option>
                                            <option value="TL">Timor-Leste</option>
                                            <option value="TG">Togo</option>
                                            <option value="TK">Tokelau</option>
                                            <option value="TO">Tonga</option>
                                            <option value="TT">Trinidad and Tobago</option>
                                            <option value="TN">Tunisia</option>
                                            <option value="TR">Turkey</option>
                                            <option value="TM">Turkmenistan</option>
                                            <option value="TC">Turks and Caicos Islands</option>
                                            <option value="TV">Tuvalu</option>
                                            <option value="UG">Uganda</option>
                                            <option value="UA">Ukraine</option>
                                            <option value="AE">United Arab Emirates</option>
                                            <option value="GB">United Kingdom</option>
                                            <option value="US">United States</option>
                                            <option value="UM">United States Minor Outlying Islands</option>
                                            <option value="UY">Uruguay</option>
                                            <option value="UZ">Uzbekistan</option>
                                            <option value="VU">Vanuatu</option>
                                            <option value="VE">Venezuela, Bolivarian Republic of</option>
                                            <option value="VN">Viet Nam</option>
                                            <option value="VG">Virgin Islands, British</option>
                                            <option value="VI">Virgin Islands, U.S.</option>
                                            <option value="WF">Wallis and Futuna</option>
                                            <option value="EH">Western Sahara</option>
                                            <option value="YE">Yemen</option>
                                            <option value="ZM">Zambia</option>
                                            <option value="ZW">Zimbabwe</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="phone">Phone Number <span class="asterisk">*</span></label>
                                        <input type="number" class="form-control" name="phone" required="">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="email">Email <span class="asterisk">*</span></label>
                                        <input type="email" class="form-control" name="email" required="">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="organizationname">Organization Name(if any)</label>
                                        <input type="text" class="form-control" name="organizationname">
                                    </div>
                                </div>
                            </div>
                            <!--Rows Ends -->
                        </form>
                    </div>
                </div>
                <!-- Card Details One Ends -->
                <!-- Card Detials Two  -->
                <div class="card">
                    <div class="card-header">
                        <span class="number">2.</span>Guarantee this Reservation
                    </div>
                    <div class="card-body">
                        <div class="col-md-6">
                            <input type="radio" name="paymentmethod" value="cashpayment" onclick="show1();" id="cash"> Cash Payment
                            <input type="radio" name="paymentmethod" value="credit-card" id="card" onclick="show2();" style="margin-left: 50px;"> Credit Card
                        </div>
                        <div id="card_detail" style="display: none;">
                            <table cellpadding="10" cellspacing="10">
                                <tbody>
                                <tr>
                                    <td>*</td>
                                    <td>Card Type</td>
                                    <td>
                                        <input type="number" name="card_type" class="form-control">
                                    </td>
                                </tr>
                                <tr>
                                    <td>*</td>
                                    <td>Card Number</td>
                                    <td>
                                        <input type="number" name="card_number" class="form-control">
                                    </td>
                                </tr>
                                <tr>
                                    <td>*</td>
                                    <td>Expire Date</td>
                                    <td>
                                        <select class="expire_date">
                                            <option value="">Month</option>
                                            <option value="Jan">Jan</option>
                                            <option value="Feb">Feb</option>
                                            <option value="Mar">Mar</option>
                                            <option value="Apr">Apr</option>
                                            <option value="May">May</option>
                                            <option value="Jun">Jun</option>
                                            <option value="July">July</option>
                                            <option value="Aug">Aug</option>
                                            <option value="Sep">Sep</option>
                                            <option value="Oct">Oct</option>
                                            <option value="Nov">Nov</option>
                                            <option value="Dec">Dec</option>
                                        </select>
                                        <select class="expire_date">
                                            <option value="">Year</option>
                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                            <option value="2022">2022</option>
                                            <option value="2023">2023</option>
                                            <option value="2024">2024</option>
                                            <option value="2025">2025</option>
                                            <option value="2026">2026</option>
                                            <option value="2027">2027</option>
                                            <option value="2028">2028</option>
                                        </select>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Card Details Two Ends -->
                <!-- Card Detials Three  -->
                <div class="card">
                    <div class="card-header">
                        <span class="number">3.</span>Review Reservation Details
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="room-title">
                                    <h2>Standard Room</h2>
                                </div>
                                <hr style="border: 1px solid black">
                            </div>
                            <div class="col-md-6">
                                <div class="select_room_detail">
                                    <table class="table">
                                        <tr>
                                            <td>Room 1</td>
                                            <td>1 Adult, 0 Children</td>
                                        </tr>
                                        <tr>
                                            <td>Room 2</td>
                                            <td>1 Adult, 0 Children</td>
                                        </tr>
                                    </table>
                                    <!-- Selected Rooms Details Ends -->
                                </div>
                                <!-- Col-md-6 Ends -->
                            </div>
                            <!-- COl-md-6 Begains -->
                            <div class="col-md-6">
                                <table class=" table">
                                    <thead><span class="head_title">Rooms Cost </span></thead>
                                    <tbody>
                                    <tr>
                                        <td>Room 1</td>
                                        <td>Rs 5,000</td>
                                    </tr>
                                    <tr>
                                        <td>Room 2</td>
                                        <td>Rs 5,000</td>
                                    </tr>
                                    <br>
                                    <tr>
                                        <td>Sub Total</td>
                                        <td> Rs 10,000</td>
                                    </tr>
                                    <tr>
                                        <td>13% Vat Added</td>
                                        <td>Rs 1,300</td>
                                    </tr>
                                    <tr>
                                        <td>Total Estimated Cost</td>
                                        <td>Rs 11,300</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <!-- Col-md-6 Ends -->
                            </div>
                            <!-- Row Ends -->
                        </div>
                        <!-- Card body Ends -->
                    </div>
                    <!-- Card Details Three Ends -->
                </div>

                <!-- Card Detials Four  -->
                <div class="card">
                    <div class="card-header">
                        <span class="number">4.</span>Terms and Conditions
                    </div>
                    <div class="card-body">
                        <form action="/action_page.php">
                            <div class="form-check">
                                <label class="form-check-label" for="check1">
                                    <input type="checkbox" class="form-check-input" id="check1" name="option1" value="something" checked>I have read and agree to the rate details and <a href="" data-toggle="modal" data-target="#exampleModalCenter">terms of sale.*</a>

                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label" for="check2">
                                    <input type="checkbox" class="form-check-input" id="check2" name="option2" value="something">I have read and agree that my personal data will be processed by the Chhaimale Resort accordance with the privacy policy.*
                                </label>
                            </div>

                            <div class="button">
                                <a href="#" class="btn btn-success">Back</a>
                                <a href="#" class="btn btn-success" >Submit</a>
                            </div>
                        </form>
                        <!-- Card body Ends -->
                    </div>
                    <!-- Card Details Four Ends -->
                </div>
            </div>
            <!-- Col-md-10 Ends -->
        </div>
        <!-- Row Ends -->
        <footer>
            <div id="subscribe">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="text">
                                        <p>Subscribe to our breif newsletter to get exclusive discounts and new events right in your inbox</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <form>
                                        <p>
                                            <input class="form-control" name="email" required="" type="email" placeholder="Enter Your Email Address" style="outline:0;">
                                        </p>
                                        <p>
                                            <input class="btn btn-success" type="submit" value="Subscribe">
                                        </p>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="followus text-center">
                                <div class="title">
                                    <h5>Follow Us</h5>
                                </div>
                                <a href="https://www.facebook.com/chhaimaleresort/" class="btn btn-fb"><i class="fa fa-facebook"></i></a>
                                <a href="#" class="btn btn-tweet"><i class="fa fa-twitter"></i></a>
                                <a href="#" class="btn btn-googleplus"><i class="fa fa-google-plus"></i></a>
                                <a href="#" class="btn btn-insta"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="footer-logo">
                                    <img src="{{asset('assets\images\footer-logo.png')}}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="text-wrapper">
                                    <table>
                                        <tbody>
                                        <td> <i class="fa fa-map-marker"></i></td>
                                        <td>
                                            <p> Ramche Bhanjyang-6
                                                <br>Kathmandu</p>
                                        </td>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="text-wrapper">
                                    <table>
                                        <tbody>
                                        <td> <i class="fa fa-phone"></i></td>
                                        <td>
                                            <p> +977-016924909
                                                <br>+977-9851181409</p>
                                        </td>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="text-wrapper">
                                    <table>
                                        <tbody>
                                        <td> <i class="fa fa-envelope icon"></i></td>
                                        <td>
                                            <p>info@chhaimaleresort.com.np
                                                <br> chhaimale.resort@gmail.com
                                            </p>
                                        </td>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="copyright text-center">
                                    <p>Copyright © <span class="name">Chhaimale Resort</span>
                                        <script>
                                            document.write(new Date().getFullYear());
                                        </script> &nbsp;All rights reserved. </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="policy">
                                    <ul>
                                        <li><a href="#">Privacy</a></li>
                                        <li><a href="#">Terms & Conditions</a></li>
                                        <li><a href="#">Help & Support</a></li>
                                    </ul>
                                </div>
                            </div>a
                            <div class="col-md-4">
                                <div class="Developedby text-center">
                                    <p>Developed with <img src="{{asset('assets\images\icon\love.gif')}}" width="20px" height="20px;"> <a href="https://prabidhilabs.com/">Prabidhi Labs</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Copy Right Content -->
                </div>
        </footer>
    </div>
    <!-- Content Ends -->

@endsection