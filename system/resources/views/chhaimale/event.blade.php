@extends('layout.app')
@section('content')
    <div class="content container">
        <!-- banner-section -->
        <section class="container">
            <div class="event-banner">
                <img src="{{asset('assets\images\banner3.jpg')}}" height="400px;" width="100%">
                <div class="text-wrapper col-md-8 offset-md-2 text-center">
                    <h1 class="animated zoomIn"><strong>Event</strong></h1><br>
                    <h5>Host meetings, conferences or a celebration that inspires your guests</h5>
                </div>
            </div>
        </section>

        <div id="wrapper">

            <div class="container">
                <div class="tab-wrapper">
                    <ul class="tabs">
                        <li class="tab-link active" data-tab="1"> Our Event</li>
                        <li class="tab-link" data-tab="2">Event</li>
                        <li class="tab-link" data-tab="3">Chhaimale Weather</li>
                    </ul>
                </div>

                <div class="content-wrapper">

                    <div id="tab-1" class="tab-content active">
                        <div class="container">

                            <div class="event">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="image">
                                            <img src="{{asset('assets\images\ourevent1.jpg')}}">
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="event-details">
                                            <div class="event-header">
                                                <div class="event-time">
                                                    <p class="day">10</p>
                                                    <p class="month">May</p>
                                                </div>

                                                <div class="event-info">
                                                    <h2>New Year Party at chhaimale</h2>


                                                </div>
                                            </div>

                                            <div class="event-detail text-justify">
                                                <p>It's the big countdown when the ball drops. I'm talking about New Year's Eve, of course! There is no other holiday I know of that involves a giant party with a "drink till you're silly" mentality followed by a day in which you make huge goals about your next year's productivity. If you don't usually remember your New Year's Eve. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="event">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="image">
                                            <img src="{{asset('assets\images\ourevent1.jpg')}}">
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="event-details">
                                            <div class="event-header">
                                                <div class="event-time">
                                                    <p class="day">10</p>
                                                    <p class="month">May</p>
                                                </div>

                                                <div class="event-info">
                                                    <h2>Christmas Party At Chhaimale</h2>


                                                </div>
                                            </div>

                                            <div class="event-detail text-justify">
                                                <p>This year be sure to celebrate the holiday with your family and friends. Throw a party filled with music, dance and fun. And if you’re not sure what to write in your Christmas party invitations, fret not. Here are some of the Christmas invitation wording samples that you may edit and use</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>


                    <div id="tab-2" class="tab-content">
                        <div  id="event-description">
                            <div class="container">
                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="event">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-12">
                                                    <div class="image">
                                                        <img src="{{asset('assets\images\conference-hall.jpg')}}">
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-sm-12">
                                                    <div class="title text-center">
                                                        <h5>Conference Hall</h5>
                                                        <p>State-of-the-art facilities</p>
                                                    </div>

                                                    <div class="details">
                                                        <ul>
                                                            <li>Projector Facalities</li>
                                                            <li>Free High-Speed Internet Access</li>
                                                            <li>Free Bright Side Breakfast</li>
                                                            <li>100 Guest Capacity</li>
                                                            <li>Projector Facalities</li>
                                                            <li>Free High-Speed Internet Access</li>
                                                            <li>Free Bright Side Breakfast</li>
                                                            <li>100 Guest Capacity</li>
                                                        </ul>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="col-md-6 col-xs-12">
                                        <div class="event">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="image">
                                                        <img src="{{asset('assets\images\wedding.jpg')}}">
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-sm-12">
                                                    <div class="title text-center">
                                                        <h5>Weddings and Honeymoons</h5>
                                                        <p>Plan every detail of your dream</p>
                                                    </div>

                                                    <div class="details">
                                                        <ul>
                                                            <li>Projector Facalities</li>
                                                            <li>Free High-Speed Internet Access</li>
                                                            <li>Free Bright Side Breakfast</li>
                                                            <li>100 Guest Capacity</li>
                                                            <li>Projector Facalities</li>
                                                            <li>Free Bright Side Breakfast</li>
                                                            <li>100 Guest Capacity</li>
                                                        </ul>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>


                                    <div class="col-md-6 col-sm-6">
                                        <div class="event">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="image">
                                                        <img src="{{asset('assets\images\conference-hall.jpg')}}">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="title text-center">
                                                        <h5>Conference Hall</h5>
                                                        <p>State-of-the-art facilities</p>
                                                    </div>

                                                    <div class="details">
                                                        <ul>
                                                            <li>Projector Facalities</li>
                                                            <li>Free High-Speed Internet Access</li>
                                                            <li>Free Bright Side Breakfast</li>
                                                            <li>100 Guest Capacity</li>
                                                            <li>Projector Facalities</li>
                                                            <li>Free High-Speed Internet Access</li>
                                                            <li>100 Guest Capacity</li>
                                                        </ul>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>


                                    <div class="col-md-6">
                                        <div class="event">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="image">
                                                        <img src="{{asset('assets\images\conference-hall.jpg')}}">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="title text-center">
                                                        <h5>Conference Hall</h5>
                                                        <p>State-of-the-art facilities</p>
                                                    </div>

                                                    <div class="details">
                                                        <ul>
                                                            <li>Projector Facalities</li>
                                                            <li>Free High-Speed Internet Access</li>
                                                            <li>Free Bright Side Breakfast</li>
                                                            <li>100 Guest Capacity</li>
                                                            <li>Projector Facalities</li>
                                                            <li>Free High-Speed Internet Access</li>
                                                            <li>Free Bright Side Breakfast</li>
                                                            <li>100 Guest Capacity</li>
                                                        </ul>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                    <!-- Event Details Ends -->








                                </div>
                            </div>

                        </div>

                    </div>

                    <div id="tab-3" class="tab-content">
                        <a class="weatherwidget-io" href="https://forecast7.com/en/27d5985d26/chhaimale/" data-label_1="CHHAIMALE" data-label_2="WEATHER" data-font="Roboto" data-days="5" data-theme="pure" >CHHAIMALE WEATHER</a>
                        <script>
                            !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
                        </script>
                    </div>


                </div>

            </div>

        </div>


        <!-- Content Ends -->
    </div>


    <div id="subscribe">
        <div class="container background">
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text">
                                <p>Subscribe to our  breif newsletter to get exclusive discounts and new events right in your inbox</p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <form>
                                <p><input class="form-control" name="email" required="" type="email" placeholder="Enter Your Email Address" style="outline:0;"></p>
                                <p><input class="btn btn-success" type="submit" value="Subscribe"></p>
                            </form>
                        </div>
                    </div>
                </div>


                <div class="col-md-4">
                    <div class="followus text-center">
                        <div class="title">
                            <h5>Follow Us</h5>
                        </div>
                        <a href="https://www.facebook.com/chhaimaleresort/" class="btn btn-fb"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="btn btn-tweet"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="btn btn-googleplus"><i class="fa fa-google-plus"></i></a>
                        <a href="#" class="btn btn-insta"><i class="fa fa-instagram"></i></a>

                    </div>

                </div>
            </div>
        </div>



        <footer class="footer-content container">
            <div class="container">
                <div class="row">

                    <div class="col-md-3">
                        <div class="footer-logo">
                            <img src="{{asset('assets\images\footer-logo.png')}}">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="text-wrapper">
                            <table>
                                <tbody>
                                <td> <i class="fa fa-map-marker"></i></td>
                                <td> <p> Ramche Bhanjyang-6,<br>Kathmandu</p></td>

                                </tbody>
                            </table>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="text-wrapper">
                            <table>
                                <tbody>
                                <td> <i class="fa fa-phone"></i></td>
                                <td><p> +977-016924909<br>+977-9851181409</p></td>

                                </tbody>
                            </table>
                        </div>


                    </div>

                    <div class="col-md-3">
                        <div class="text-wrapper">
                            <table>
                                <tbody>
                                <td> <i class="fa fa-envelope icon"></i></td>
                                <td> <p>info@chhaimaleresort.com.np<br>
                                        chhaimale.resort@gmail.com
                                    </p></td>
                                </tbody>
                            </table>
                        </div>
                    </div>





                    <div class="col-md-4">
                        <div class="copyright text-center">
                            <p>Copyright © <span class="name">Chhaimale Resort</span>  <script>document.write(new Date().getFullYear());</script> &nbsp;All rights reserved. </p>
                        </div>

                    </div>

                    <div class="col-md-4">
                        <div class="policy">
                            <ul>
                                <li><a href="#">Privacy</a></li>
                                <li><a href="#">Terms & Conditions</a></li>
                                <li><a href="#">Help & Support</a></li>
                            </ul>
                        </div>

                    </div>

                    <div class="col-md-4">

                        <div class="Developedby text-center">
                            <p>Developed with <img src="{{asset('assets\images\icon\love.gif')}}" width="20px" height="20px;"> <a href="https://prabidhilabs.com/">Prabidhi Labs</a></p>
                        </div>

                    </div>
                </div>





            </div>
            <!-- Copy Right Content -->

    </div>
    </footer>
@endsection

