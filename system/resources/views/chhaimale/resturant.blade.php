@extends('layout.app')
@section('content')

    <div class="content container">
        <!-- banner-section -->
        <section class="container">
            <div class="restuarant-banner">
                <img src="{{asset('assets\images\restaurant-banner.jpg')}}">
                <div class="text-wrapper col-md-8 offset-md-2 text-center">
                    <h1 class="animated zoomIn"><strong>Jana: Baha: Restro and Bar</strong></h1><br>
                    <h5>You don’t need a silver fork to eat good food</h5>
                </div>
            </div>
        </section>

        <!-- Banner- Section -->

        <section id="about-restuarent">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2 restauarant">
                        <div class="heading text-center">
                            <h3>TASTEFUL EXPERIENCES SINCE ’98</h3><br>
                        </div>

                        <div class="text-wrapper text-center">
                            <p>Liking to cook dinner for your friends and loved ones might be a good starting point, but it won't be enough if you want to make it as a line cook. Cooking the same dishes for strangers for a lengthy shift requires more than just a general "like" of the kitchen, you've got to love it.</p>
                            <p>Yours truly, Steven</p>
                        </div>

                        <div class="cook_image text-center">
                            <img src="{{asset('assets\images\chef.jpg')}}">
                            <p>
                                <strong>Ram Kumar Shrestha</strong><br>
                                Head Chef
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="bar" class="container">
            <div class="row">
                <div class="col-md-10">
                    <div class="wrapper">
                        <img src="{{asset('assets\images\bar-banner.jpg')}}" width="100%" height="600px;">

                        <div class="about-bar text-justify wow fadeInRight">
                            <h4>Drinks and Bar </h4><br>
                            <p>They’re seas gathering behold the years saying make and divide fill given whales fill female moved, blessed. Midst one from divide whales seasons cattle male own saying to night fruit own creeping second earth be lesser without deep beast female.. </p>
                        </div>

                    </div>
                </div>

            </div>
        </section>



        <section id="food-item">

            <div class="food-menu">
                <div class="container">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="heading text-center wow zoomIn">
                                <h1>Finest Food house</h1>
                            </div>
                        </div>


                        <div class="col-md-3 padding">
                            <figure class="snip1482 wow fadeIn" style="animation-delay: 0.2s">
                                <figcaption>
                                    <h2>Special Thakali Food <br>
                                        <small>Rs 5000</small></h2>


                                </figcaption>
                                <a href="#"></a> <img src="{{asset('assets\images\thakali.jpg')}}">
                            </figure>
                        </div>

                        <div class="col-md-3 padding">
                            <figure class="snip1482 wow fadeIn" style="animation-delay: 0.4s">
                                <figcaption>
                                    <h2> Special Chhaimale Food <br>
                                        <small>Rs 5000</small>
                                    </h2>


                                </figcaption>
                                <a href="#"></a> <img src="{{asset('assets\images\food-item1.jpg')}}">
                            </figure>
                        </div>


                        <div class="col-md-3 padding">
                            <figure class="snip1482 wow fadeIn" style="animation-delay: 0.6s">
                                <figcaption>
                                    <h2> Special Chhaimale Food <br>
                                        <small>Rs 5000</small>
                                    </h2>
                                </figcaption>
                                <a href="#"></a> <img src="{{asset('assets\images\nepali-food.jpg')}}">
                            </figure>
                        </div>



                        <div class="col-md-3 padding" >
                            <figure class="snip1482 wow fadeIn" style="animation-delay: 0.8s">
                                <figcaption>
                                    <h2> Special Chhaimale Food <br>
                                        <small>Rs 5000</small>
                                    </h2>

                                </figcaption>
                                <a href="#"></a> <img src="{{asset('assets\images\thakali.jpg')}}">
                            </figure>
                        </div>

                        <div class="col-md-3 padding">
                            <figure class="snip1482 wow fadeIn" style="animation-delay:1.2s">
                                <figcaption>
                                    <h2> Special Chhaimale Food <br>
                                        <small>Rs 5000</small>
                                    </h2>

                                </figcaption>
                                <a href="#"></a> <img src="{{asset('assets\images\thakali.jpg')}}">
                            </figure>
                        </div>

                        <div class="col-md-3 padding">
                            <figure class="snip1482 wow fadeIn" style="animation-delay: 1.4s">
                                <figcaption>
                                    <h2> Special Chhaimale Food <br>
                                        <small>Rs 5000</small>
                                    </h2>

                                </figcaption>
                                <a href="#"></a> <img src="{{asset('assets\images\thakali.jpg')}}">
                            </figure>
                        </div>

                        <div class="col-md-3 padding">
                            <figure class="snip1482 wow fadeIn " style="animation-delay: 1.6s">
                                <figcaption>
                                    <h2> Special Chhaimale Food <br>
                                        <small>Rs 5000</small>
                                    </h2>

                                </figcaption>
                                <a href="#"></a> <img src="{{asset('assets\images\thakali.jpg')}}">
                            </figure>
                        </div>

                        <div class="col-md-3 padding">
                            <figure class="snip1482 wow fadeIn" style="animation-delay: 1.8s">
                                <figcaption>
                                    <h2> Special Chhaimale Food <br>
                                        <small>Rs 5000</small>
                                    </h2>
                                </figcaption>
                                <a href="#"></a> <img src="{{asset('assets\images\thakali.jpg')}}">
                            </figure>
                        </div>
                    </div>

                </div>

            </div>

        </section>



        <!-- last Banner -->
        <section>
            <div class="last-banner">
                <img src="{{asset('assets\images\restaurant.jpg')}}" height="500px;" width="100%">
                <div class="text-wrapper col-md-8 offset-md-2 text-center">
                    <strong><h1>HAVE A ROOM? YOU HAVE A SEAT!</h1></strong>
                    <p>You can book your room online, and if your room supports it, automatically reserve a seat for dinner in our 5-star gourme restaurant. So do not wait, book your room now!</p>
                    <a href="{{route('search')}}" class="btn btn-outline">Book Now</a>
                </div>
            </div>
        </section>


    </div>


    <!-- Content Ends -->
    </div>


    <div id="subscribe">
        <div class="container background">
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text">
                                <p>Subscribe to our  breif newsletter to get exclusive discounts and new events right in your inbox</p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <form>
                                <p><input class="form-control" name="email" required="" type="email" placeholder="Enter Your Email Address" style="outline:0;"></p>
                                <p><input class="btn btn-success" type="submit" value="Subscribe"></p>
                            </form>
                        </div>
                    </div>
                </div>


                <div class="col-md-4">
                    <div class="followus text-center">
                        <div class="title">
                            <h5>Follow Us</h5>
                        </div>
                        <a href="https://www.facebook.com/chhaimaleresort/" class="btn btn-fb"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="btn btn-tweet"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="btn btn-googleplus"><i class="fa fa-google-plus"></i></a>
                        <a href="#" class="btn btn-insta"><i class="fa fa-instagram"></i></a>

                    </div>

                </div>
            </div>
        </div>



        <footer class="footer-content container">
            <div class="container">
                <div class="row">

                    <div class="col-md-3">
                        <div class="footer-logo">
                            <img src="{{asset('assets\images\footer-logo.png')}}">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="text-wrapper">
                            <table>
                                <tbody>
                                <td> <i class="fa fa-map-marker"></i></td>
                                <td> <p> Ramche Bhanjyang-6,<br>Kathmandu</p></td>

                                </tbody>
                            </table>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="text-wrapper">
                            <table>
                                <tbody>
                                <td> <i class="fa fa-phone"></i></td>
                                <td><p> +977-016924909<br>+977-9851181409</p></td>

                                </tbody>
                            </table>
                        </div>


                    </div>

                    <div class="col-md-3">
                        <div class="text-wrapper">
                            <table>
                                <tbody>
                                <td> <i class="fa fa-envelope icon"></i></td>
                                <td> <p>info@chhaimaleresort.com.np<br>
                                        chhaimale.resort@gmail.com
                                    </p></td>
                                </tbody>
                            </table>
                        </div>
                    </div>





                    <div class="col-md-4">
                        <div class="copyright text-center">
                            <p>Copyright © <span class="name">Chhaimale Resort</span>  <script>document.write(new Date().getFullYear());</script> &nbsp;All rights reserved. </p>
                        </div>

                    </div>

                    <div class="col-md-4">
                        <div class="policy">
                            <ul>
                                <li><a href="#">Privacy</a></li>
                                <li><a href="#">Terms & Conditions</a></li>
                                <li><a href="#">Help & Support</a></li>
                            </ul>
                        </div>

                    </div>

                    <div class="col-md-4">

                        <div class="Developedby text-center">
                            <p>Developed with <img src="{{asset('assets\images\icon\love.gif')}}" width="20px" height="20px;"> <a href="https://prabidhilabs.com/">Prabidhi Labs</a></p>
                        </div>

                    </div>
                </div>





            </div>
            <!-- Copy Right Content -->

    </div>
    </footer>
@endsection

