@extends('layouts.app')
@section('content')
    <!-- Pre Loader -->
    <div id="loader-wrapper">
        <div id="loader">
        </div>
        <div id="image1">
            <img src="{{asset('assets\images\logo.png')}}">
        </div>
        <div class="loader-section section-left">
        </div>
        <div class="loader-section section-right">
        </div>
    </div>
    <!-- Pre Loader Ends -->

    <!-- Navigation Begains -->
    <div id="popup_this" style="display: none;">
            <span class="button b-close">
        <span>X</span>
            </span>
        <img src="{{asset('assets\images\ad2.jpg')}}" height="500px" alt="Chhaimale Event">
    </div>
    <!-- Navigation Ends -->
    <!-- Banner Slider -->

    <div class="content container">
        <section class="banner-slider">
            <div id="demo" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="{{asset('assets\images\banner4.jpg')}}" alt="Chhaimale Resort">
                    </div>
                    <div class="carousel-item">
                        <img src="{{asset('assets\images\banner2.jpg')}}" alt="Chhaimale Resort">
                    </div>

                    <div class="carousel-item">
                        <img src="{{asset('assets\images\banner1.jpg')}}" alt=" Standard Room">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#demo" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#demo" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>
            </div>
            <!-- Book Now in Banner -->
            <div class="booknow">
                <form>
                    <div class="col-md-10 offset-md-1">
                        <div class="Wrapper">
                            <div class="row">


                                <!-- date  -->
                                <div class="col-md-4 padding">
                                    <div class="form-group" style="padding-left:30px;">
                                        <label>Check  In</label>
                                        <input type="text" id="check_in" name="check_in" class="form">
                                    </div>
                                </div>
                                <div class="col-md-4 padding">
                                    <div class="form-group">
                                        <label>Check Out</label>
                                        <input type="text" id="check_out" name="check_out" class="form">

                                    </div>
                                </div>

                                <!-- Date Ends -->

                                <!-- check availablity button -->
                                <div class="col-md-4">
                                    <a href="{{route('room')}}" class="btn">Check Availability</a>
                                </div>
                                <!-- Check Availablity Button Ends -->
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </section>


        <!-- About Us  -->
        <section id="aboutus-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading text-uppercase text-center wow zoomIn">
                            <h4>welcome to</h4>
                            <h2>Chhaimale Resort</h2>
                            <div class="outline text-center">
                                <img src="{{asset('assets\images\border.png')}}">
                            </div>
                        </div>
                        <div class="info">
                            <div class="col-md-10 offset-md-1 text-center">
                                <p>Chhaimaile Resort is located in Chhaimale, Dakshinkaali, just 22 kilometers away from Kathmandu. Located in a peaceful environmental, we deliver a natural environment with quality services, comfortable rooms, excellent and hygienic foods at relatively cheap price.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="amenities">
                <div class="row">
                    <div class="col-md-3">
                        <div class="info">
                            <h2>Amenities</h2>
                            <p> Chhaimale Resort privately owns residence with distinct features, and no two properties are exactly alike. </p>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="owl-carousel">
                            <div class="amenities-cat-item">
                                <img src="{{asset('assets\images\icon\cocktail.png')}}">
                                <h4>Drink and Bar</h4>
                            </div>

                            <div class="amenities-cat-item">
                                <img src="{{asset('assets\images\icon\music.png')}}">
                                <h4>Live Music</h4>

                            </div>

                            <div class="amenities-cat-item">
                                <img src="{{asset('assets\images\icon\bath.png')}}">
                                <h4>Bathroom</h4>

                            </div>

                            <div class="amenities-cat-item">
                                <img src="{{asset('assets\images\icon\wifi.png')}}">
                                <h4>Free Wifi</h4>

                            </div>

                            <div class="amenities-cat-item">
                                <img src="{{asset('assets\images\icon\tv.png')}}">
                                <h4>Television</h4>
                            </div>

                            <div class="amenities-cat-item">
                                <img src="{{asset('assets\images\icon\res.png')}}">
                                <h4>Restuarant</h4>
                            </div>

                        </div>


                    </div>
                </div>
        </section>
        <!-- About Us Ends -->
        <!-- Rooms Description-->

        <section id="rooms-section" class="container">
            <div class="container">
                <div class="col-lg-12">
                    <div class="heading text-center">
                        <h2 class="wow zoomIn">Rooms and Suite</h2>
                        <p>With a privileged location next to the Daksinkali Temple, the recently refurbished Chhaimale Resort is the ideal resort for your leisure or work trip. Get to know our exclusive The Level service, an experience that will make your stay unforgettable.</p>
                    </div>
                </div>
                <div class="autoplay">
                    <div class="room-description padding">
                        <div class="image">
                            <img src="{{asset('assets\images\wooden-house.jpg')}}">
                        </div>
                        <div class="info">
                            <h4>Wooden Room</h4>
                            <div class="price">
                                <img src="{{asset('assets\images\ribbon2.png')}}">

                                <div class="price_tag">
                                    <p> Rs 3500 per Night</p>
                                </div>

                            </div>
                            <span class="icon">
                              <i class="fa fa-wifi" data-toggle="tooltip" title="Free WIFI!"></i>
                              <i class="fa fa-bath" data-toggle="tooltip" title="Attached Bathroom!"></i>
                              <i class="fa fa-tv" data-toggle="tooltip" title="TV "></i>
                              <i class="fa fa-tty" data-toggle="tooltip" title="Phone"></i>


                          </span>
                            <br>
                            <br>
                            <a href="{{route('search')}}" class="btn btn-outline"> Book Now</a>
                        </div>
                    </div>


                    <!-- Room 2 -->

                    <div class="room-description padding">
                        <div class="image">
                            <img src="{{asset('assets\images\family-room.jpg')}}">
                        </div>
                        <div class="info">
                            <h4>Family Room</h4>
                            <div class="price">
                                <img src="{{asset('assets\images\ribbon2.png')}}">

                                <div class="price_tag">
                                    <p> Rs 2500 per Night</p>
                                </div>

                            </div>
                            <span class="icon">
                              <i class="fa fa-wifi" data-toggle="tooltip" title="Free WIFI!"></i>
                              <i class="fa fa-bath" data-toggle="tooltip" title="Attached Bathroom!"></i>
                              <i class="fa fa-tv" data-toggle="tooltip" title="TV "></i>
                              <i class="fa fa-tty" data-toggle="tooltip" title="Phone"></i>


                          </span>
                            <br>
                            <br>
                            <a href="{{route('search')}}" class="btn btn-outline"> Book Now</a>
                        </div>
                    </div>

                    <!-- Room 3 -->

                    <div class="room-description padding">
                        <div class="image">
                            <img src="{{asset('assets\images\standard-room.jpg')}}">
                        </div>
                        <div class="info">
                            <h4>Standard Room</h4>
                            <div class="price">
                                <img src="{{asset('assets\images\ribbon2.png')}}">

                                <div class="price_tag">
                                    <p> Rs 4000 per Night</p>
                                </div>

                            </div>
                            <span class="icon">
                              <i class="fa fa-wifi" data-toggle="tooltip" title="Free WIFI!"></i>
                              <i class="fa fa-bath" data-toggle="tooltip" title="Attached Bathroom!"></i>
                              <i class="fa fa-tv" data-toggle="tooltip" title="TV "></i>
                              <i class="fa fa-tty" data-toggle="tooltip" title="Phone"></i>


                          </span>
                            <br>
                            <br>
                            <a href="{{route('search')}}" class="btn btn-outline"> Book Now</a>
                        </div>
                    </div>

                    <!-- Room 4 -->

                    <div class="room-description padding">
                        <div class="image">
                            <img src="{{asset('assets\images\tent.jpg')}}">
                        </div>
                        <div class="info">
                            <h4>Tent Camp</h4>
                            <div class="price">
                                <img src="{{asset('assets\images\ribbon2.png')}}">

                                <div class="price_tag">
                                    <p> Rs 2500 per Night</p>
                                </div>

                            </div>
                            <span class="icon">
                              <i class="fa fa-wifi" data-toggle="tooltip" title="Free WIFI!"></i>
                              <i class="fa fa-bath" data-toggle="tooltip" title="Attached Bathroom!"></i>
                              <i class="fa fa-tv" data-toggle="tooltip" title="TV "></i>
                              <i class="fa fa-tty" data-toggle="tooltip" title="Phone"></i>


                          </span>
                            <br>
                            <br>
                            <a href="{{route('search')}}" class="btn btn-outline"> Book Now</a>
                        </div>
                    </div>

                    <!-- Room 5 -->

                    <div class="room-description padding">
                        <div class="image">
                            <img src="{{asset('assets\images\family-room.jpg')}}">
                        </div>
                        <div class="info">
                            <h4>Family Room</h4>
                            <div class="price">
                                <img src="{{asset('assets\images\ribbon2.png')}}">

                                <div class="price_tag">
                                    <p> Rs 2500 per Night</p>
                                </div>

                            </div>
                            <span class="icon">
                              <i class="fa fa-wifi" data-toggle="tooltip" title="Free WIFI!"></i>
                              <i class="fa fa-bath" data-toggle="tooltip" title="Attached Bathroom!"></i>
                              <i class="fa fa-tv" data-toggle="tooltip" title="TV "></i>
                              <i class="fa fa-tty" data-toggle="tooltip" title="Phone"></i>


                          </span>
                            <br>
                            <br>
                            <a href="{{route('search')}}" class="btn btn-outline"> Book Now</a>
                        </div>
                    </div>

                </div>
            </div>
            <!-- Container Ends -->
        </section>

        <section id="activites" class="container">
            <div class="heading text-center wow zoomIn">
                <h2>Activities</h2>
                <div class="outline text-center">
                    <img src="{{asset('assets\images\border.png')}}">
                </div>
            </div>
            <div class="site-content">
                <div class="container">
                    <div class="activite-list">
                        <div class="row">
                            <div class="col-md-4 col-xs-3   ">
                                <div class="activite">
                                    <div class="activite_card wow fadeIn" style="animation-delay: 0.1s">
                                        <a href="" class="activite_image"><img src="{{asset('assets\images\bbq.jpg')}}" alt=""></a>
                                        <div class="activite_detail">
                                            <h2 class="activite_title">BBQ</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 ">
                                <div class="activite">
                                    <div class="activite_card  wow fadeIn" style="animation-delay: 0.2s">
                                        <a href="" class="activite_image" ><img src="{{asset('assets\images\boating.jpg')}}"  alt=""></a>
                                        <div class="activite_detail">
                                            <h2 class="activite_title">Boating</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 ">
                                <div class="activite">
                                    <div class="activite_card wow fadeIn" style="animation-delay: 0.3s">
                                        <a href="" class="activite_image"><img src="{{asset('assets\images\bar.jpg')}}"  alt="" data-toggle="modal" data-target="#exampleModal"></a>

                                        <div class="activite_detail">
                                            <h2 class="activite_title">Bar</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 ">
                                <div class="activite">
                                    <div class="activite_card wow fadeInUp" style="animation-delay: 0.4s" >
                                        <a href="" class="activite_image " ><img src="{{asset('assets\images\friday.jpg')}}"  alt=""></a>
                                        <div class="activite_detail">
                                            <h2 class="activite_title">Friday Night </h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 ">
                                <div class="activite">
                                    <div class="activite_card wow fadeInUp" style="animation-delay:0.4s">
                                        <a href="" class="activite_image" ><img src="{{asset('assets\images\treking.jpg')}}"  alt=""></a>
                                        <div class="activite_detail">
                                            <h2 class="activite_title">trekking</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="activite">
                                    <div class="activite_card wow fadeInUp" style="animation-delay:0.4s">
                                        <div class="activite_image" ><img src="{{asset('assets\images\acoustic.jpg')}}"  alt="">
                                            <div class="activite_detail">
                                                <h2 class="activite_title">Acoustic Friday Night</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


        </section>
        <!-- Client Review -->
        <section id="testimonial" class="container">
            <div class="heading text-center wow zoomIn">
                <h4>Testimonial</h4>
                <h2>Customer Satisfaction</h2>
                <hr align="center" width="5%">
            </div>
            <!-- Slider Starts -->
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Carousel indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>
                <!-- Wrapper for carousel items -->
                <div class="carousel-inner">
                    <div class="item carousel-item active">
                        <div class="img-box"><img src="{{asset('assets\images\customer1.jpg')}}" alt=""></div>
                        <p class="testimonial">Have been here three times before and loved coming here each year...Each time we stay...the staff is great and are always wonderful to their guests!"</p>
                        <p class="overview"><b>Sunil Prajapati</b>Wordpress Developer at <a href="#">Prabidhi Labs</a></p>
                        <div class="star-rating">
                            <ul class="list-inline">
                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                            </ul>
                        </div>
                    </div>
                    <div class="item carousel-item">
                        <div class="img-box"><img src="{{asset('assets\images\customer.jpg')}}" alt=""></div>
                        <p class="testimonial">Have been here three times before and loved coming here each year...Each time we stay...the staff is great and are always wonderful to their guests!"</p>
                        <p class="overview"><b>Paula Wilson</b>Media Analyst at <a href="#">SkyNet Inc.</a></p>
                        <div class="star-rating">
                            <ul class="list-inline">
                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                            </ul>
                        </div>
                    </div>
                    <div class="item carousel-item">
                        <div class="img-box"><img src="{{asset('assets\images\customer.jpg')}}" alt=""></div>
                        <p class="testimonial">Have been here three times before and loved coming here each year...Each time we stay...the staff is great and are always wonderful to their guests! "</p>
                        <p class="overview"><b>Antonio Moreno</b>Web Developer at <a href="#">Circle Ltd.</a></p>
                        <div class="star-rating">
                            <ul class="list-inline">
                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                <li class="list-inline-item"><i class="fa fa-star-half-o"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Carousel controls -->
                <a class="carousel-control left carousel-control-prev" href="#myCarousel" data-slide="prev">
                    <i class="fa fa-angle-left"></i>
                </a>
                <a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next">
                    <i class="fa fa-angle-right"></i>
                </a>
            </div>
        </section>
        <!-- Client Reviews Ends -->
        <!-- Book Now Bottom Fixed -->
        <div id="book_botton">
            <a href="{{route('search')}}" class="btn btn-success animated rotateInDownRight">
                <span class="text"> B </span>
                <span class="text"> O </span>
                <span class="text"> O </span>
                <span class="text"> K </span>
                <span class="text"> N</span>
                <span class="text"> N</span>
                <span class="text"> N</span>
                <span class="text"> N</span>
                <span class="text"> N</span>
                <span class="text"> N</span>
                <span class="text"> O </span>
                <span class="text">W </span>
                </h1></a>
        </div>
        <!-- Book Now Bottom Ends -->



        <!-- Content Ends -->
    </div>


    <div id="subscribe">
        <div class="container background">
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text">
                                <p>Subscribe to our  breif newsletter to get exclusive discounts and new events right in your inbox</p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <form>
                                <p><input class="form-control" name="email" required="" type="email" placeholder="Enter Your Email Address" style="outline:0;"></p>
                                <p><input class="btn btn-success" type="submit" value="Subscribe"></p>
                            </form>
                        </div>
                    </div>
                </div>


                <div class="col-md-4">
                    <div class="followus text-center">
                        <div class="title">
                            <h5>Follow Us</h5>
                        </div>
                        <a href="https://www.facebook.com/chhaimaleresort/" class="btn btn-fb"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="btn btn-tweet"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="btn btn-googleplus"><i class="fa fa-google-plus"></i></a>
                        <a href="#" class="btn btn-insta"><i class="fa fa-instagram"></i></a>

                    </div>

                </div>
            </div>
        </div>



        <footer class="footer-content container">
            <div class="container">
                <div class="row">

                    <div class="col-md-3">
                        <div class="footer-logo">
                            <img src="{{asset('assets\images\footer-logo.png')}}">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="text-wrapper">
                            <table>
                                <tbody>
                                <td> <i class="fa fa-map-marker"></i></td>
                                <td> <p> Ramche Bhanjyang-6,<br>Kathmandu</p></td>

                                </tbody>
                            </table>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="text-wrapper">
                            <table>
                                <tbody>
                                <td> <i class="fa fa-phone"></i></td>
                                <td><p> +977-016924909<br>+977-9851181409</p></td>

                                </tbody>
                            </table>
                        </div>


                    </div>

                    <div class="col-md-3">
                        <div class="text-wrapper">
                            <table>
                                <tbody>
                                <td> <i class="fa fa-envelope icon"></i></td>
                                <td> <p>info@chhaimaleresort.com.np<br>
                                        chhaimale.resort@gmail.com
                                    </p></td>
                                </tbody>
                            </table>
                        </div>
                    </div>





                    <div class="col-md-4">
                        <div class="copyright text-center">
                            <p>Copyright © <span class="name">Chhaimale Resort</span>  <script>document.write(new Date().getFullYear());</script> &nbsp;All rights reserved. </p>
                        </div>

                    </div>

                    <div class="col-md-4">
                        <div class="policy">
                            <ul>
                                <li><a href="#">Privacy</a></li>
                                <li><a href="#">Terms & Conditions</a></li>
                                <li><a href="#">Help & Support</a></li>
                            </ul>
                        </div>

                    </div>

                    <div class="col-md-4">

                        <div class="Developedby text-center">
                            <p>Developed with <img src="{{asset('assets\images\icon\love.gif')}}" width="20px" height="20px;"> <a href="https://prabidhilabs.com/">Prabidhi Labs</a></p>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Copy Right Content -->
    </div>
    </footer>
@endsection