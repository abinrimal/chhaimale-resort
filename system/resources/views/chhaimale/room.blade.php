@extends('layout.app')
@section('content')
    <div class="content container">
        <div class="rooms-banner">
            <div class="banner-image">
                <img src="{{asset('assets\images\deluxe-room.jpg')}}" alt="Standard Room ">
            </div>
            <div class="text-wrapper">
                <div class="container">
                    <div class="row">
                        <div class=" col-xs-6 col-md-8">
                            <div class="heading">
                                <h3>Chhaimale Resort The Pear Garden</h3>
                                <p><i class="fa fa-map-marker"></i>Ramche Bhanjyang-6,Kathmandu</p>
                            </div>
                        </div>
                        <div class="col-xs-6 col-md-4">
                            <div class="rate-info">
                                <div class="rate-label">
                                    Rate From
                                    <br>Rs 2000/NIGHT
                                    <a href="#wrapper" class="btn btn-success btn-lg">See Rooms</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Room Banner -->
        <div id="wrapper">
            <div class="container">
                <div class="tab-wrapper">
                    <ul class="tabs">
                        <li class="tab-link active" data-tab="1">Rooms</li>
                        <li class="tab-link" data-tab="2">Weather Forecast</li>
                    </ul>
                </div>
                <div class="content-wrapper">
                    <div id="tab-1" class="tab-content active">
                        <div id="room-description">
                            <!-- //////////////////////////////////////////////////////
                                  /////////////Standard Room -->

                            <div class="row">
                                <div class=" col-sm-12 col-md-4">
                                    <div class="room-image">
                                        <a href="{{asset('assets\images\deluxe-room.jpg')}}" class="js-img-viwer" data-caption="deluxe-room" data-id="deluxe-room">
                                            <img src="{{asset('assets\images\deluxe-room.jpg')}}" width="360"/ >
                                        </a>
                                        <a href="{{asset('assets\images\family-room.jpg')}}" class="js-img-viwer" data-caption="family-room" data-id="family-room">
                                            <img src="{{asset('assets\images\family-room.jpg')}}" width="360"/ style="display: none">
                                        </a>
                                        <a href="{{asset('assets\images\homestay.jpg')}}" class="js-img-viwer" data-caption="Home Stay" data-id="Home Stay">
                                            <img src="{{asset('assets\images\homestay.jpg')}}" width="360"/ style="display: none">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="room-detail">
                                        <div class="heading">
                                            <h3>Standard Room</h3>
                                        </div>
                                        <div class="room-feature">
                                            <ul>
                                                <li><span>32-inch Flat Screen TV w/Premium Cable Channels</span></li>
                                                <li>Free High-Speed Internet Access</li>
                                                <li>Free Bright Side Breakfast</li>
                                                <div class="standard-hidden-feature">
                                                    <li>Bright Morning Pillowtop Beds</li>
                                                    <li> Convenient Work Desk</li>
                                                    <li>Separate Area w/Sleeper Sofa</li>
                                                    <li>Microwave & Mini Refrigerator</li>
                                                    <li>Coffee Maker</li>
                                                    <li>Hairdryer</li>
                                                    <li>Iron w/Ironing Board</li>
                                                    <li>Free Local Calls</li>
                                                    <li>Alarm Clock</li>
                                                </div>
                                            </ul>
                                            <hr>
                                            <p id="standard-room">MORE ROOMS DETAILS </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="room-price">
                                        <div class="price">
                                            <strike>Rs 2500/ Per Night</strike>
                                            <h2>Rs 2000 / Per Night</h2>
                                        </div>
                                        <a href="{{route('search')}}" class="button">Book Now</a>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->

                            <!-- /.container -->
                            <hr>
                            <!-- //////////////////////////////////////////////////////
                                  /////////////Family Room -->

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="room-image">
                                        <a href="{{asset('assets\images\family-room.jpg')}}" class="img-viwer" data-caption="family-room" data-id="family-room">
                                            <img src="{{asset('assets\images\family-room.jpg')}}" width="360"/ alt="family room">
                                        </a>
                                        <a href="{{asset('assets\images\deluxe-room.jpg')}}" class="img-viwer" data-caption="deluxe-room" data-id="deluxe-room">
                                            <img src="{{asset('assets\images\deluxe-room.jpg')}}" width="360"/ style="display: none" alt="standard room">
                                        </a>
                                        <a href="{{asset('assets\images\homestay.jpg')}}" class="img-viwer" data-caption="home-stay" data-id="home-stay">
                                            <img src="{{asset('assets\images\homestay.jpg')}}" width="360"/ style="display: none" alt="home stay">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="room-detail">
                                        <div class="heading">
                                            <h3>Wooden Room</h3>
                                        </div>
                                        <div class="room-feature">
                                            <ul>
                                                <li>32-inch Flat Screen TV w/Premium Cable Channels</li>
                                                <li>Free High-Speed Internet Access</li>
                                                <li>Free Bright Side Breakfast</li>
                                                <div class="wooden-hidden-feature">
                                                    <li>Bright Morning Pillowtop Beds</li>
                                                    <li> Convenient Work Desk</li>
                                                    <li>Separate Area w/Sleeper Sofa</li>
                                                    <li>Microwave & Mini Refrigerator</li>
                                                    <li>Coffee Maker</li>
                                                    <li>Hairdryer</li>
                                                    <li>Iron w/Ironing Board</li>
                                                    <li>Free Local Calls</li>
                                                    <li>Alarm Clock</li>
                                                </div>
                                            </ul>
                                            <hr>
                                            <p id="wooden-room">MORE ROOMS DETAILS </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="room-price">
                                        <div class="price">
                                            <strike>Rs 2500 / Per Night</strike>
                                            <h2>Rs 2000/ Per Night</h2>
                                        </div>
                                        <a href="{{route('search')}}" class="button">Book Now</a>
                                    </div>
                                </div>
                            </div>

                            <hr>
                            <!-- //////////////////////////////////////////////////////
                                  /////////////Home Stay -->

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="room-image">
                                        <a href="{{asset('assets\images\homestay.jpg')}}" class="viwer" data-caption="home-stay" data-id="home-stay">
                                            <img src="{{asset('assets\images\homestay.jpg')}}" width="360"/ alt="home stay">
                                        </a>
                                        <a href="{{asset('assets\images\family-room.jpg')}}" class="viwer" data-caption="family-room" data-id="family-room">
                                            <img src="{{asset('assets\images\family-room.jpg')}}" width="360"/ style="display: none;" alt="family room">
                                        </a>
                                        <a href="{{asset('assets\images\deluxe-room.jpg')}}" class="viwer" data-caption="deluxe-room" data-id="deluxe-room">
                                            <img src="{{asset('assets\images\deluxe-room.jpg')}}" width="360"/ style="display: none;" alt="deluxe room">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="room-detail">
                                        <div class="heading">
                                            <h3>Group Room</h3>
                                        </div>
                                        <div class="room-feature">
                                            <ul>
                                                <li>32-inch Flat Screen TV w/Premium Cable Channels</li>
                                                <li>Free High-Speed Internet Access</li>
                                                <li>Free Bright Side Breakfast</li>
                                                <div class="homestay-hidden-feature">
                                                    <li>Bright Morning Pillowtop Beds</li>
                                                    <li> Convenient Work Desk</li>
                                                    <li>Separate Area w/Sleeper Sofa</li>
                                                    <li>Microwave & Mini Refrigerator</li>
                                                    <li>Coffee Maker</li>
                                                    <li>Hairdryer</li>
                                                    <li>Iron w/Ironing Board</li>
                                                    <li>Free Local Calls</li>
                                                    <li>Alarm Clock</li>
                                                </div>
                                            </ul>
                                            <hr>
                                            <p id="homestay">MORE ROOMS DETAILS </p>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="room-price">
                                        <div class="price">
                                            <strike>Rs 2500 / Per Night</strike>
                                            <h2>Rs 2000 / Per Night</h2>
                                        </div>
                                        <a href="{{route('search')}}" class="button">Book Now</a>
                                    </div>
                                </div>
                            </div>


                            <!-- Tented Rooms -->



                            <div class="row">
                                <div class="col-md-4">
                                    <div class="room-image">
                                        <a href="{{asset('assets\images\homestay.jpg')}}" class="viwer" data-caption="home-stay" data-id="home-stay">
                                            <img src="{{asset('assets\images\homestay.jpg')}}" width="360"/ alt="home stay">
                                        </a>
                                        <a href="{{asset('assets\images\family-room.jpg')}}" class="viwer" data-caption="family-room" data-id="family-room">
                                            <img src="{{asset('assets\images\family-room.jpg')}}" width="360"/ style="display: none;" alt="family room">
                                        </a>
                                        <a href="{{asset('assets\images\deluxe-room.jpg')}}" class="viwer" data-caption="deluxe-room" data-id="deluxe-room">
                                            <img src="{{asset('assets\images\deluxe-room.jpg')}}" width="360"/ style="display: none;" alt="deluxe room">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="room-detail">
                                        <div class="heading">
                                            <h3>Tented Room</h3>
                                        </div>
                                        <div class="room-feature">
                                            <ul>
                                                <li>32-inch Flat Screen TV w/Premium Cable Channels</li>
                                                <li>Free High-Speed Internet Access</li>
                                                <li>Free Bright Side Breakfast</li>
                                                <div class="tentstay-hidden-feature">
                                                    <li>Bright Morning Pillowtop Beds</li>
                                                    <li> Convenient Work Desk</li>
                                                    <li>Separate Area w/Sleeper Sofa</li>
                                                    <li>Microwave & Mini Refrigerator</li>
                                                    <li>Coffee Maker</li>
                                                    <li>Hairdryer</li>
                                                    <li>Iron w/Ironing Board</li>
                                                    <li>Free Local Calls</li>
                                                    <li>Alarm Clock</li>
                                                </div>
                                            </ul>
                                            <hr>
                                            <p id="tentstay">MORE ROOMS DETAILS </p>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="room-price">
                                        <div class="price">
                                            <strike>Rs 2500 / Per Night</strike>
                                            <h2>Rs 2000 / Per Night</h2>
                                        </div>
                                        <a href="{{route('search')}}" class="button">Book Now</a>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div id="tab-2" class="tab-content">
                        <a class="weatherwidget-io" href="https://forecast7.com/en/27d5985d26/chhaimale/" data-label_1="CHHAIMALE" data-label_2="WEATHER" data-font="Roboto" data-days="5" data-theme="pure">CHHAIMALE WEATHER</a>
                        <script>
                            ! function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (!d.getElementById(id)) { js = d.createElement(s);
                                js.id = id;
                                js.src = 'https://weatherwidget.io/js/widget.min.js';
                                fjs.parentNode.insertBefore(js, fjs); } }(document, 'script', 'weatherwidget-io-js');
                        </script>
                    </div>
                </div>
            </div>
        </div>
        <!-- Content Ends -->
    </div>


    <div id="subscribe">
        <div class="container background">
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text">
                                <p>Subscribe to our  breif newsletter to get exclusive discounts and new events right in your inbox</p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <form>
                                <p><input class="form-control" name="email" required="" type="email" placeholder="Enter Your Email Address" style="outline:0;"></p>
                                <p><input class="btn btn-success" type="submit" value="Subscribe"></p>
                            </form>
                        </div>
                    </div>
                </div>


                <div class="col-md-4">
                    <div class="followus text-center">
                        <div class="title">
                            <h5>Follow Us</h5>
                        </div>
                        <a href="https://www.facebook.com/chhaimaleresort/" class="btn btn-fb"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="btn btn-tweet"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="btn btn-googleplus"><i class="fa fa-google-plus"></i></a>
                        <a href="#" class="btn btn-insta"><i class="fa fa-instagram"></i></a>

                    </div>

                </div>
            </div>
        </div>



        <footer class="footer-content container">
            <div class="container">
                <div class="row">

                    <div class="col-md-3">
                        <div class="footer-logo">
                            <img src="{{asset('assets\images\footer-logo.png')}}">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="text-wrapper">
                            <table>
                                <tbody>
                                <td> <i class="fa fa-map-marker"></i></td>
                                <td> <p> Ramche Bhanjyang-6,<br>Kathmandu</p></td>

                                </tbody>
                            </table>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="text-wrapper">
                            <table>
                                <tbody>
                                <td> <i class="fa fa-phone"></i></td>
                                <td><p> +977-016924909<br>+977-9851181409</p></td>

                                </tbody>
                            </table>
                        </div>


                    </div>

                    <div class="col-md-3">
                        <div class="text-wrapper">
                            <table>
                                <tbody>
                                <td> <i class="fa fa-envelope icon"></i></td>
                                <td> <p>info@chhaimaleresort.com.np<br>
                                        chhaimale.resort@gmail.com
                                    </p></td>
                                </tbody>
                            </table>
                        </div>
                    </div>





                    <div class="col-md-4">
                        <div class="copyright text-center">
                            <p>Copyright © <span class="name">Chhaimale Resort</span>  <script>document.write(new Date().getFullYear());</script> &nbsp;All rights reserved. </p>
                        </div>

                    </div>

                    <div class="col-md-4">
                        <div class="policy">
                            <ul>
                                <li><a href="#">Privacy</a></li>
                                <li><a href="#">Terms & Conditions</a></li>
                                <li><a href="#">Help & Support</a></li>
                            </ul>
                        </div>

                    </div>

                    <div class="col-md-4">

                        <div class="Developedby text-center">
                            <p>Developed with <img src="{{asset('assets\images\icon\love.gif')}}" width="20px" height="20px;"> <a href="https://prabidhilabs.com/">Prabidhi Labs</a></p>
                        </div>

                    </div>
                </div>





            </div>
            <!-- Copy Right Content -->

    </div>
    </footer>
</div>
@endsection