@extends('layout.app')
@section('content')
    <div class="content container">
        <!-- Navigation Ends -->
        <section class="container">
            <div class="contactus-banner">
                <img src="{{asset('assets\images\banner3.jpg')}}">
                <div class="text-wrapper col-md-8 offset-md-2 text-center">
                    <h1 class="animated zoomIn" style=" animation-delay: 1s;"><strong>Contact Now</strong></h1>
                    <br>
                    <h5>We are at your Service</h5>
                </div>
            </div>
        </section>
        <!-- Contact Us -->
        <section id="contactus">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-area">
                            <div class="heading text-uppercase">
                                <h3><span class="color">Get in </span>touch</h3>
                                <hr align="left" width="10%">
                            </div>
                            <form role="form">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject">
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" type="textarea" id="message" placeholder="Message" maxlength="140" rows="7"></textarea>
                                </div>
                                <button type="button" id="submit" name="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="contact-info">
                            <div class="text-center text-uppercase">
                                <h4>Contact info</h4>
                                <hr align="center" width="10%">
                            </div>
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td><b>Address</b></td>
                                    <td>
                                        Ramche Bhanjyang-6,Kathmandu
                                        <br> City Office: Newa:Nuga:Complex,
                                        <br> Om Bahal,Kathmandu,Nepal
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Resort Contact</b></td>
                                    <td> 01-6924909/9851181409 </td>
                                </tr>
                                <tr>
                                <tr>
                                    <td><b>Office Contact</b></td>
                                    <td>+977-4268121/Fax +977 4265205 </td>
                                </tr>
                                <td><b>Email</b></td>
                                <td>booking@chhaimaleresort.com.np
                                    <br> chhaimaleresort@gmail.com
                                </td>
                                </tr>
                                <tr>
                                    <td><b>Online</b></td>
                                    <td><a href="search_rooms.html" style="color:#8B6B43">Booking now</a></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Contact Us ends -->
        <div class="container border">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d8294.982963030816!2d85.25012550007948!3d27.587183216266165!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb3d94c8ba1e27%3A0xddffa2d612cebd37!2sChhaimale+Resort!5e1!3m2!1sen!2snp!4v1523293085986" width="100%" height="450" frameborder="0" style="" allowfullscreen></iframe>
        </div>
        <!-- Content Ends -->
    </div>


    <div id="subscribe">
        <div class="container background">
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text">
                                <p>Subscribe to our  breif newsletter to get exclusive discounts and new events right in your inbox</p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <form>
                                <p><input class="form-control" name="email" required="" type="email" placeholder="Enter Your Email Address" style="outline:0;"></p>
                                <p><input class="btn btn-success" type="submit" value="Subscribe"></p>
                            </form>
                        </div>
                    </div>
                </div>


                <div class="col-md-4">
                    <div class="followus text-center">
                        <div class="title">
                            <h5>Follow Us</h5>
                        </div>
                        <a href="https://www.facebook.com/chhaimaleresort/" class="btn btn-fb"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="btn btn-tweet"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="btn btn-googleplus"><i class="fa fa-google-plus"></i></a>
                        <a href="#" class="btn btn-insta"><i class="fa fa-instagram"></i></a>

                    </div>

                </div>
            </div>
        </div>



        <footer class="footer-content container">
            <div class="container">
                <div class="row">

                    <div class="col-md-3">
                        <div class="footer-logo">
                            <img src="{{asset('assets\images\footer-logo.png')}}">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="text-wrapper">
                            <table>
                                <tbody>
                                <td> <i class="fa fa-map-marker"></i></td>
                                <td> <p> Ramche Bhanjyang-6<br>Kathmandu</p></td>

                                </tbody>
                            </table>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="text-wrapper">
                            <table>
                                <tbody>
                                <td> <i class="fa fa-phone"></i></td>
                                <td><p> +977-016924909<br>+977-9851181409</p></td>

                                </tbody>
                            </table>
                        </div>


                    </div>

                    <div class="col-md-3">
                        <div class="text-wrapper">
                            <table>
                                <tbody>
                                <td> <i class="fa fa-envelope icon"></i></td>
                                <td> <p>info@chhaimaleresort.com.np<br>
                                        chhaimale.resort@gmail.com
                                    </p></td>
                                </tbody>
                            </table>
                        </div>
                    </div>





                    <div class="col-md-4">
                        <div class="copyright text-center">
                            <p>Copyright © <span class="name">Chhaimale Resort</span>  <script>document.write(new Date().getFullYear());</script> &nbsp;All rights reserved. </p>
                        </div>

                    </div>

                    <div class="col-md-4">
                        <div class="policy">
                            <ul>
                                <li><a href="#">Privacy</a></li>
                                <li><a href="#">Terms & Conditions</a></li>
                                <li><a href="#">Help & Support</a></li>
                            </ul>
                        </div>

                    </div>

                    <div class="col-md-4">

                        <div class="Developedby text-center">
                            <p>Developed with <img src="{{asset('assets\images\icon\love.gif')}}" width="20px" height="20px;"> <a href="https://prabidhilabs.com/">Prabidhi Labs</a></p>
                        </div>

                    </div>
                </div>





            </div>
            <!-- Copy Right Content -->

    </div>
    </footer>

@endsection