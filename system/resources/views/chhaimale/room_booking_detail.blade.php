@extends('layout.app')
@section('content')
    <div class=" content container" style="padding-top: 125px;">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-10">
                <div class="rate_details">
                    <div class="heading">
                        <a href="{{route('roombook')}}">< Back to Search Result</a>
                    </div>

                    <div class="header_content text-center">
                        <div class="content">
                            <div class="content__container">

                                <ul class="content__container__list">
                                    <li class="content__container__list__item">Free Internet Avaiable</li>
                                    <li class="content__container__list__item">Conference Hall </li>
                                    <li class="content__container__list__item">Special Chhaimale Food </li>
                                    <li class="content__container__list__item">Acoustic Friday Night</li>
                                </ul>
                            </div>
                        </div>
                    </div>


                    <div class="review_booking">

                        <div class="book_room">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="room_selected">
                                        <table>
                                            <tr>
                                                <td><img src="{{asset('assets\images\standard-room.jpg')}}"></td>
                                                <td>Standard Room<br>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime illo ipsum dolor</p>

                                            </tr>
                                        </table>


                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="date_details" style="float: right;">
                                        <table>
                                            <tr>
                                                <td>Check-in: Jul 04, 2018</td>

                                            </tr>
                                            <tr>
                                                <td>Check-out: Jul 05, 2018</td>

                                            </tr>
                                            <tr>
                                                <td>1 Room, Adult 1, Child 0 </td>

                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Review Booking Ends -->


                        <div class="review_content">
                            <div class="yoursearch">
                                <span class="yourSearch">Your Search :</span>
                                <span class="yourSearchdata">
                                                       Standard Room|  Jul 04, 2018 –  Jul 05, 2018 |  1 Room | 1 Adult, 0 Children
                                                    </span>
                            </div>


                            <div class="row">
                                <div class="col-md-7">
                                    <div class="title">
                                        <h4>Chhaimale Resort Alert</h4>
                                    </div>

                                    <div class="notice">
                                        <p>CREDIT CARD ON THE RESERVATION WILL BE PREAUTHORIZED 24 HOURS PRIOR TO ARRIVAL DAY - UNLESS POLICY STATES EARLIER. IF CREDIT CARD IS DECLINED, RESERVATION WILL BE CANCELLED</p>
                                    </div>
                                </div>

                                <div class="col-md-5">
                                    <table class=" table table-dark">
                                        <thead><span class="head_title">Rooms Cost </span></thead>
                                        <tbody>

                                        <tr>
                                            <td>Room 1</td>
                                            <td>Rs 5,000</td>
                                        </tr>

                                        <tr>
                                            <td>Room 2</td>
                                            <td>Rs 5,000</td>
                                        </tr>
                                        <br>
                                        <tr>
                                            <td>Sub Total</td>
                                            <td> Rs 10,000</td>
                                        </tr>

                                        <tr>
                                            <td>13% Vat Added</td>
                                            <td>Rs 1,300</td>
                                        </tr>

                                        <tr>
                                            <td>Total Estimated Cost</td>
                                            <td>Rs 11,300</td>
                                        </tr>

                                        </tbody>
                                    </table>

                                    <a href="{{route('form')}}" class="btn btn-success" style="float: right;"> Continue</a>

                                </div>
                            </div>
                        </div>



                    </div>


                    <!-- Rate End Details -->
                </div>
                <!-- Col-md-10 Ends -->
            </div>
            <!-- Row Ends -->
        </div>
        <!-- Container Ends -->
        <!-- Content Ends -->
    </div>


    <div id="subscribe">
        <div class="container background">
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text">
                                <p>Subscribe to our  breif newsletter to get exclusive discounts and new events right in your inbox</p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <form>
                                <p><input class="form-control" name="email" required="" type="email" placeholder="Enter Your Email Address" style="outline:0;"></p>
                                <p><input class="btn btn-success" type="submit" value="Subscribe"></p>
                            </form>
                        </div>
                    </div>
                </div>


                <div class="col-md-4">
                    <div class="followus text-center">
                        <div class="title">
                            <h5>Follow Us</h5>
                        </div>
                        <a href="https://www.facebook.com/chhaimaleresort/" class="btn btn-fb"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="btn btn-tweet"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="btn btn-googleplus"><i class="fa fa-google-plus"></i></a>
                        <a href="#" class="btn btn-insta"><i class="fa fa-instagram"></i></a>

                    </div>

                </div>
            </div>
        </div>



        <footer class="footer-content container">
            <div class="container">
                <div class="row">

                    <div class="col-md-3">
                        <div class="footer-logo">
                            <img src="{{asset('assets\images\footer-logo.png')}}">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="text-wrapper">
                            <table>
                                <tbody>
                                <td> <i class="fa fa-map-marker"></i></td>
                                <td> <p> Ramche Bhanjyang-6,<br>Kathmandu</p></td>

                                </tbody>
                            </table>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="text-wrapper">
                            <table>
                                <tbody>
                                <td> <i class="fa fa-phone"></i></td>
                                <td><p> +977-016924909<br>+977-9851181409</p></td>

                                </tbody>
                            </table>
                        </div>


                    </div>

                    <div class="col-md-3">
                        <div class="text-wrapper">
                            <table>
                                <tbody>
                                <td> <i class="fa fa-envelope icon"></i></td>
                                <td> <p>info@chhaimaleresort.com.np<br>
                                        chhaimale.resort@gmail.com
                                    </p></td>
                                </tbody>
                            </table>
                        </div>
                    </div>





                    <div class="col-md-4">
                        <div class="copyright text-center">
                            <p>Copyright © <span class="name">Chhaimale Resort</span>  <script>document.write(new Date().getFullYear());</script> &nbsp;All rights reserved. </p>
                        </div>

                    </div>

                    <div class="col-md-4">
                        <div class="policy">
                            <ul>
                                <li><a href="#">Privacy</a></li>
                                <li><a href="#">Terms & Conditions</a></li>
                                <li><a href="#">Help & Support</a></li>
                            </ul>
                        </div>

                    </div>

                    <div class="col-md-4">

                        <div class="Developedby text-center">
                            <p>Developed with <img src="{{asset('assets\images\icon\love.gif')}}" width="20px" height="20px;"> <a href="https://prabidhilabs.com/">Prabidhi Labs</a></p>
                        </div>

                    </div>
                </div>





            </div>
            <!-- Copy Right Content -->

    </div>
    </footer>
@endsection