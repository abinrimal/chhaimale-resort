<?php

namespace App\Http\Controllers;

use App\libraries\storage\SettingRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingController extends Controller
{
    public  $settingRepo,  $img_path;
    public $title = 'Setting';

    public function __construct(SettingRepository $settingRepo)
    {
        $this->settingRepo = $settingRepo;
        $this->img_path = 'assets/uploads/setting/';
    }

    public function index()
    {
        $img_path = $this->img_path;
        $title = $this->title;

        $allsetting = $this->settingRepo->all();

        return view('admin.setting.index', compact('allsetting', 'title','img_path'));

    }

    public function add()
    {

    }

    public function save()
    {

    }

    public function edit($id)
    {
        if (!$setting = $this->settingRepo->find($id)) {
            return view('errors.503');
        }
        $img_path = $this->img_path;

        return view('admin.setting.update')->with(compact('setting','img_path'));
    }

    public function update(Request $request)
    {

        $this->validate($request,[
            'site_title' => 'required|min:5',
            'logo' => 'required',
            'logo_collapsed' => 'required',
            'meta_title' => 'required',
            'meta_description' => 'required',
            'keywords'=>'required',
            'copyright_text'=>'required',

        ]);
        $id = $request->input('id');
        if (!$setting = $this->settingRepo->find($id)) {
            return view('errors.503');
        }
        $data=[
            'site_title'=>$request->input('site_title'),
            'meta_title'=>$request->input('meta_title'),
            'meta_description'=>$request->input('meta_description'),
            'keywords'=>$request->input('keywords'),
            'copyright_text'=>$request->input('copyright_text'),
            'added_by'=>Auth::user()->id,
            'edited_by'=>Auth::user()->id
        ];

        if($request->hasFile('logo'))
        {
            $extension = $request->file('logo')->getClientOriginalExtension();
            $filename  = str_random(8).'.'.$extension;
            $request->file('logo')->move($this->img_path,$filename);
            $data['logo']=$filename;
        }
        if($request->hasFile('logo_collapsed'))
        {
            $extension = $request->file('logo_collapsed')->getClientOriginalExtension();
            $filename  = str_random(8).'.'.$extension;
            $request->file('logo_collapsed')->move($this->img_path,$filename);
            $data['logo_collapsed']=$filename;
        }
        $this->settingRepo->update($id,$data);
        return redirect()->route('admin.setting');
    }


    public function delete($id)
    {

    }
}
