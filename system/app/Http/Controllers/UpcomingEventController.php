<?php

namespace App\Http\Controllers;

use App\libraries\storage\UpcomingEventRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UpcomingEventController extends Controller
{
     public  $upcomingeventRepo,  $img_path;
    public $title = 'Upcoming Event';

    public function __construct(UpcomingEventRepository $upcomingeventRepo)
    {
         $this->upcomingeventRepo = $upcomingeventRepo;
        $this->img_path = 'assets/uploads/upcomingevent/';
    }

    public function index()
    {
        $img_path = $this->img_path;
        $title = $this->title;

        $allevents = $this->upcomingeventRepo->all();

        return view('admin.upcomingevent.index', compact('allevents', 'title','img_path'));
    }

    public function add()
    {
        $title = $this->title;

        return view('admin.upcomingevent.add', compact('title'));
    }

    public function save(Request $request)
    {

        $this->validate($request,[
            'event_title' => 'required',
            'event_amount' => 'required',
            'event_date' => 'required',
            'event_description' => 'required',
            'event_criteria' => 'required',
            'event_total_seats'=>'required',
            'event_image'=>'required',
            'event_meta_title'=>'required',
            'event_keywords'=>'required',
            'event_meta_description'=>'required',

        ]);
        $data=[
            'title'=>$request->input('event_title'),
            'description'=>$request->input('event_description'),
            'date'=>$request->input('event_date'),
            'amount'=>$request->input('event_amount'),
            'criteria'=>$request->input('event_criteria'),
            'total_seats'=>$request->input('event_total_seats'),
            'meta_title'=>$request->input('event_meta_title'),
            'meta_description'=>$request->input('event_meta_description'),
            'keywords'=>$request->input('event_keywords'),
            'added_by'=>Auth::user()->id,
            'updated_by'=>Auth::user()->id
        ];

        if($request->hasFile('event_image'))
        {
            $extension = $request->file('event_image')->getClientOriginalExtension();
            $filename  = str_random(8).'.'.$extension;
            $request->file('event_image')->move($this->img_path,$filename);
            $data['image']=$filename;
        }
        $this->upcomingeventRepo->create($data);
        return redirect()->route('admin.upcomingevents');
    }

    public function edit($id)
    {
            if (!$event = $this->upcomingeventRepo->find($id)) {
            return view('errors.503');
        }
        $img_path = $this->img_path;

        return view('admin.upcomingevent.update')->with(compact('event','img_path'));
    }

    public function update(Request $request)
    {
         $this->validate($request,[
            'event_title' => 'required',
            'event_amount' => 'required',
            'event_date' => 'required',
            'event_description' => 'required',
            'event_criteria' => 'required',
            'event_total_seats'=>'required',
            'event_image'=>'required',
            'event_meta_title'=>'required',
            'event_keywords'=>'required',
            'event_meta_description'=>'required',

        ]);
               $id = $request->input('id');
                $data=[
            'title'=>$request->input('event_title'),
            'description'=>$request->input('event_description'),
            'date'=>$request->input('event_date'),
            'amount'=>$request->input('event_amount'),
            'criteria'=>$request->input('event_criteria'),
            'total_seats'=>$request->input('event_total_seats'),
            'meta_title'=>$request->input('event_meta_title'),
            'meta_description'=>$request->input('event_meta_description'),
            'keywords'=>$request->input('event_keywords'),
            'added_by'=>Auth::user()->id,
            'updated_by'=>Auth::user()->id
        ];

        if($request->hasFile('event_image'))
        {
            $extension = $request->file('event_image')->getClientOriginalExtension();
            $filename  = str_random(8).'.'.$extension;
            $request->file('event_image')->move($this->img_path,$filename);
            $data['image']=$filename;
        }
        $this->upcomingeventRepo->update($id,$data);
        return redirect()->route('admin.upcomingevents');
    }

    public function delete($id)
    {
             if (!$event = $this->upcomingeventRepo->find($id)) {
            return view('errors.503');
        }else{
            $this->upcomingeventRepo->delete($id);
        }
        return redirect()->route('admin.upcomingevents');
    }
}
