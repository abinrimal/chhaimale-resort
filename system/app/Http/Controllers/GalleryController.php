<?php

namespace App\Http\Controllers;

use App\libraries\storage\GalleryRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GalleryController extends Controller
{
    public  $galleryRepo,  $img_path;
    public $title = 'Gallery';

    public function __construct(GalleryRepository $galleryRepo)
    {
        $this->galleryRepo = $galleryRepo;
        $this->img_path = 'assets/uploads/gallery/';
    }

    public function index()
    {
        $img_path = $this->img_path;
        $title = $this->title;

        $allgallery = $this->galleryRepo->all();

        return view('admin.gallery.index', compact('allgallery', 'title','img_path'));
    }

    public function add()
    {
        $title = $this->title;

        return view('admin.gallery.add', compact('title'));
    }

    public function save(Request $request)
    {
        $this->validate($request,[
            'gallery_title' => 'required',
            'gallery_image' => 'required',
        ]);
        $data=[
            'title'=>$request->input('gallery_title'),
            'added_by'=>Auth::user()->id,
            'edited_by'=>Auth::user()->id
        ];

        if($request->hasFile('gallery_image'))
        {
            $extension = $request->file('gallery_image')->getClientOriginalExtension();
            $filename  = str_random(8).'.'.$extension;
            $request->file('gallery_image')->move($this->img_path,$filename);
            $data['image']=$filename;
        }
        $this->galleryRepo->create($data);
        return redirect()->route('admin.gallery');
    }

    public function edit($id)
    {
        if (!$gallery = $this->galleryRepo->find($id)) {
            return view('errors.503');
        }
        $img_path = $this->img_path;

        return view('admin.gallery.update')->with(compact('gallery','img_path'));
    }

    public function update(Request $request)
    {
        $id = $request->input('id');
        $this->validate($request,[
            'gallery_title' => 'required',
            'gallery_image' => 'required',
        ]);

        if (!$testimonial = $this->galleryRepo->find($id)) {
            return view('errors.503');
        }
        $data=[
            'title'=>$request->input('gallery_title'),
            'edited_by'=>Auth::user()->id
        ];


        if($request->hasFile('gallery_image'))
        {
            $extension = $request->file('gallery_image')->getClientOriginalExtension();
            $filename  = str_random(8).'.'.$extension;
            $request->file('gallery_image')->move($this->img_path,$filename);
            $data['image']=$filename;
        }

        $this->galleryRepo->update($id,$data);
        return redirect()->route('admin.gallery');
    }

    public function delete($id)
    {
        if (!$gallery = $this->galleryRepo->find($id)) {
            return view('errors.503');
        }else{
            $this->galleryRepo->delete($id);
        }
        return redirect()->route('admin.gallery');
    }
}
