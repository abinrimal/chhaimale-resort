<?php

namespace App\Http\Controllers;

use App\libraries\storage\EventRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EventController extends Controller
{
    public  $eventRepo,  $img_path;
    public $title = 'Event';

    public function __construct(EventRepository $eventRepo)
    {
        $this->eventRepo = $eventRepo;
    }

    public function index()
    {
        $title = $this->title;

        $allevent = $this->eventRepo->all();

        return view('admin.event.index', compact('allevent', 'title'));

    }

    public function add()
    {
        $title = $this->title;

        return view('admin.event.add', compact('title'));
    }

    public function save(Request $request)
    {
        $this->validate($request,[
            'event_title' => 'required',
            'event_subtitle' => 'required',
            'event_description' => 'required',

        ]);
        $data=[
            'title'=>$request->input('event_title'),
            'sub_title'=>$request->input('event_subtitle'),
            'description'=>$request->input('event_description'),
            'added_by'=>Auth::user()->id,
            'edited_by'=>Auth::user()->id
        ];
        $this->eventRepo->create($data);
        return redirect()->route('admin.event');
    }

    public function edit($id)
    {
        if (!$event = $this->eventRepo->find($id)) {
            return view('errors.503');
        }

        return view('admin.event.update')->with(compact('event'));
    }

    public function update(Request $request)
    {
        $id = $request->input('id');
        $this->validate($request,[
            'event_title' => 'required',
            'event_subtitle' => 'required',
            'event_description' => 'required',

        ]);
      
        
        if (!$testimonial = $this->eventRepo->find($id)) {
            return view('errors.503');
        }
        $data=[
            'title'=>$request->input('event_title'),
            'sub_title'=>$request->input('event_subtitle'),
            'description'=>$request->input('event_description'),
            'added_by'=>Auth::user()->id,
            'edited_by'=>Auth::user()->id
        ];
       

        $this->eventRepo->update($id,$data);
        return redirect()->route('admin.event');
    }

    public function delete($id)
    {
        if (!$event = $this->eventRepo->find($id)) {
            return view('errors.503');
        }else{
            $this->eventRepo->delete($id);
        }
        return redirect()->route('admin.event');
    }
}
