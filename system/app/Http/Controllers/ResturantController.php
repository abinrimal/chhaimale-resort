<?php

namespace App\Http\Controllers;

use App\libraries\storage\ResturantRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ResturantController extends Controller
{
    public  $restaurantRepo,  $img_path;
    public $title = 'Restaurant';

    public function __construct(ResturantRepository $restaurantRepo)
    {
        $this->restaurantRepo = $restaurantRepo;
        $this->img_path = 'assets/uploads/restaurant/';
    }

    public function index()
    {
        $img_path = $this->img_path;
        $title = $this->title;

        $allrestaurant = $this->restaurantRepo->all();

        return view('admin.resturant.index', compact('allrestaurant', 'title','img_path'));
    }

    public function add()
    {

    }

    public function save()
    {

    }

    public function edit($id)
    {
        if (!$restaurant = $this->restaurantRepo->find($id)) {
            return view('errors.503');
        }
        $img_path = $this->img_path;

        return view('admin.resturant.update')->with(compact('restaurant','img_path'));

    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'page_header' => 'required|min:5',
            'sub_header' => 'required',
            'chef_name' => 'required',
            'featured_title' => 'required',
            'chef_message' => 'required',
            'featured_message'=>'required',
            'description'=>'required',
            'featured_image'=>'required',
            'chef_photo'=>'required',
        ]);
        $id = $request->input('id');
        if (!$restaurant = $this->restaurantRepo->find($id)) {
            return view('errors.503');
        }
        $data=[
            'page_header'=>$request->input('page_header'),
            'sub_header'=>$request->input('sub_header'),
            'chef_name'=>$request->input('chef_name'),
            'featured_title'=>$request->input('featured_title'),
            'chef_message'=>$request->input('chef_message'),
            'featured_message'=>$request->input('featured_message'),
            'description'=>$request->input('description'),
            'added_by'=>Auth::user()->id,
            'edited_by'=>Auth::user()->id
        ];
        if($request->hasFile('chef_photo'))
        {
            $extension = $request->file('chef_photo')->getClientOriginalExtension();
            $filename  = str_random(8).'.'.$extension;
            $request->file('chef_photo')->move($this->img_path,$filename);
            $data['chef_photo']=$filename;
        }
        if($request->hasFile('featured_image'))
        {
            $extension = $request->file('featured_image')->getClientOriginalExtension();
            $filename  = str_random(8).'.'.$extension;
            $request->file('featured_image')->move($this->img_path,$filename);
            $data['featured_image']=$filename;
        }
        $this->restaurantRepo->update($id,$data);
        return redirect()->route('admin.restaurant');
    }

    public function delete($id)
    {

    }
}
