<?php

namespace App\Http\Controllers;

use App\libraries\storage\TestimonialRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TestimonialController extends Controller
{
    public  $testimonialRepo,  $img_path;
    public $title = 'Testimonial';

    public function __construct(TestimonialRepository $testimonialRepo)
    {
        $this->testimonialRepo = $testimonialRepo;
        $this->img_path = 'assets/uploads/testimonial/';
    }

    public function index()
    {
        $img_path = $this->img_path;
        $title = $this->title;

        $alltestimonial = $this->testimonialRepo->all();

        return view('admin.testimonials.index', compact('alltestimonial', 'title','img_path'));
    }

    public function add()
    {
        $title = $this->title;

        return view('admin.testimonials.add', compact('title'));
    }

    public function save(Request $request)
    {
        $this->validate($request,[
            'testimonial_name' => 'required|min:5',
            'testimonial_position' => 'required',
            'testimonial_company' => 'required',
            'testimonial_rating' => 'required',
            'testimonial_number' => 'required',
            'testimonial_comment'=>'required'

        ]);
        $data=[
            'name'=>$request->input('testimonial_name'),
            'position'=>$request->input('testimonial_position'),
            'company'=>$request->input('testimonial_company'),
            'rating'=>$request->input('testimonial_rating'),
            'phone_number'=>$request->input('testimonial_number'),
            'comment'=>$request->input('testimonial_comment'),
            'added_by'=>Auth::user()->id,
            'edited_by'=>Auth::user()->id
        ];

        if($request->hasFile('testimonial_image'))
        {
            $extension = $request->file('testimonial_image')->getClientOriginalExtension();
            $filename  = str_random(8).'.'.$extension;
            $request->file('testimonial_image')->move($this->img_path,$filename);
            $data['picture']=$filename;
        }
        $this->testimonialRepo->create($data);
        return redirect()->route('admin.testimonials');
    }

    public function edit($id)
    {
        if (!$testimonial = $this->testimonialRepo->find($id)) {
            return view('errors.503');
        }
        $img_path = $this->img_path;

        return view('admin.testimonials.update')->with(compact('testimonial','img_path'));
    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'testimonial_name' => 'required|min:5',
            'testimonial_position' => 'required',
            'testimonial_company' => 'required',
            'testimonial_rating' => 'required',
            'testimonial_number' => 'required',
            'testimonial_comment'=>'required'

        ]);
        $id = $request->input('id');
        if (!$testimonial = $this->testimonialRepo->find($id)) {
            return view('errors.503');
        }
        $data=[
            'name'=>$request->input('testimonial_name'),
            'position'=>$request->input('testimonial_position'),
            'company'=>$request->input('testimonial_company'),
            'rating'=>$request->input('testimonial_rating'),
            'phone_number'=>$request->input('testimonial_number'),
            'comment'=>$request->input('testimonial_comment'),
            'added_by'=>Auth::user()->id,
            'edited_by'=>Auth::user()->id
        ];

        if($request->hasFile('testimonial_image'))
        {
            $extension = $request->file('testimonial_image')->getClientOriginalExtension();
            $filename  = str_random(8).'.'.$extension;
            $request->file('testimonial_image')->move($this->img_path,$filename);
            $data['picture']=$filename;
        }

        $this->testimonialRepo->update($id,$data);
        return redirect()->route('admin.testimonials');
    }

    public function delete($id)
    {
        if (!$testimonial = $this->testimonialRepo->find($id)) {
            return view('errors.503');
        }else{
            $this->testimonialRepo->delete($id);
        }
        return redirect()->route('admin.testimonials');
    }
}
