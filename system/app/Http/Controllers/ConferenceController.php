<?php

namespace App\Http\Controllers;

use App\libraries\storage\ConferenceRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ConferenceController extends Controller
{
    public  $conference;
    public $title = 'Conference';

    public function __construct(ConferenceRepository $conference)
    {
        $this->conference = $conference;
    }

    public function index()
    {
        $title = $this->title;

        $conference_bookings = $this->conference->all();

        return view('admin.conference.index', compact('conference_bookings', 'title'));
    }
    public function View($id)
    {
        $title = $this->title;

        if (!$conference = $this->conference->find($id)) {
            return view('errors.503');
        }
        
        return view('admin.conference.single',compact('conference', 'title'));
    }

    public function add()
    {
        $title = $this->title;

        return view('admin.conference.add', compact('title'));
    }

    public function save(Request $request)
    {
        $data=[
            'title'=>$request->input('title'),
            'first_name'=>$request->input('first_name'),
            'last_name'=>$request->input('last_name'),
            'address1'=>$request->input('address1'),
            'address2'=>$request->input('address2'),
            'city'=>$request->input('city'),
            'zip'=>$request->input('zip'),
            'country'=>$request->input('country'),
            'phone'=>$request->input('phone'),
            'email'=>$request->input('email'),
            'org'=>$request->input('org'),
            'cfname'=>$request->input('cfname'),
            'attendance'=>$request->input('attendance'),
            'startdate'=>$request->input('startdate'),
            'starttime'=>$request->input('starttime'),
            'endtime'=>$request->input('endtime'),
            'enddate'=>$request->input('enddate'),
            'style'=>$request->input('style'),
            'setup_suggestion'=>$request->input('setup_suggestion'),
            'meal'=>json_encode($request->input('meal')),
            'meal_suggestion'=>$request->input('meal_suggestion'),
            'room_detail'=>json_encode($request->input('room_detail')),
            'room_suggestion'=>$request->input('room_suggestion'),
            'credit_card'=>$request->input('credit_card'),
            'added_by'=>Auth::user()->id,
            'edited_by'=>Auth::user()->id
        ];

        $this->conference->create($data);
        return redirect()->route('admin.conference');
    }

    public function edit($id)
    {
        if (!$conference = $this->conference->find($id)) {
            return view('errors.503');
        }
        $img_path = $this->img_path;

        return view('admin.conference.update')->with(compact('conference','img_path'));
    }

    public function update(Request $request)
    {
        $id = $request->input('id');
        if (!$conference = $this->conference->find($id)) {
            return view('errors.503');
        }
        $data=[
            'title'=>$request->input('title'),
            'first_name'=>$request->input('first_name'),
            'last_name'=>$request->input('last_name'),
            'address1'=>$request->input('address1'),
            'address2'=>$request->input('address2'),
            'city'=>$request->input('city'),
            'zip'=>$request->input('zip'),
            'country'=>$request->input('country'),
            'phone'=>$request->input('phone'),
            'email'=>$request->input('email'),
            'org'=>$request->input('org'),
            'cfname'=>$request->input('cfname'),
            'attendance'=>$request->input('attendance'),
            'startdate'=>$request->input('startdate'),
            'starttime'=>$request->input('starttime'),
            'endtime'=>$request->input('endtime'),
            'enddate'=>$request->input('enddate'),
            'style'=>$request->input('style'),
            'setup_suggestion'=>$request->input('setup_suggestion'),
            'meal'=>json_encode($request->input('meal')),
            'meal_suggestion'=>$request->input('meal_suggestion'),
            'room_detail'=>json_encode($request->input('room_detail')),
            'room_suggestion'=>$request->input('room_suggestion'),
            'credit_card'=>$request->input('credit_card'),
            'edited_by'=>Auth::user()->id
        ];

        $this->conference->update($id,$data);
        return redirect()->route('admin.conference');

    }

    public function delete($id)
    {
        if (!$conference = $this->conference->find($id)) {
            return view('errors.503');
        }
        $this->conference->delete($id);
        return redirect()->route('admin.conference');
    }
}
