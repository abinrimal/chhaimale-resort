<?php

namespace App\Http\Controllers;

use App\libraries\storage\CompanyRepository;
use App\model\company_detail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompanyController extends Controller
{
    private $company;
    public $title = 'Company Details';

    public function __construct(CompanyRepository $company)
    {
        $this->company = $company;
    }

    public function index()
    {
        $title = $this->title;

        $company_details = $this->company->all();

        return view('admin.company.index', compact('company_details', 'title'));
    }

    public function edit($id)
    {
        if (!$company = $this->company->find($id)) {
            return view('errors.503');
        }

        return view('admin.company.update')->with(compact('company'));
    }

    public function update(Request $request)
    {
        $id = $request->input('id');
        if (!$activity = $this->company->find($id)) {
            return view('errors.503');
        }
        $data=[
            'name'=>$request->input('company_name'),
            'address'=>$request->input('company_address'),
            'resort_contact'=>$request->input('resort_contact'),
            'office_contact'=>$request->input('office_contact'),
            'email'=>$request->input('email'),
            'facebook_link'=>$request->input('facebook_link'),
            'twitter_link'=>$request->input('twitter_link'),
            'google_link'=>$request->input('google_link'),
            'instagram_link'=>$request->input('instagram_link'),
            'map_link'=>$request->input('map_link'),
            'edited_by'=>Auth::user()->id
        ];

        $this->company->update($id,$data);
        return redirect()->route('admin.company');

    }
}
