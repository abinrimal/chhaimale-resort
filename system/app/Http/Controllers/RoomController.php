<?php

namespace App\Http\Controllers;

use App\libraries\storage\RoomRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoomController extends Controller
{
    public  $roomRepo,  $img_path;
    public $title = 'Rooms';

    public function __construct(RoomRepository $roomRepo)
    {
        $this->roomRepo = $roomRepo;
    }

    public function index()
    {
        $title = $this->title;

        $allrooms = $this->roomRepo->all();

        return view('admin.room.index', compact('allrooms', 'title'));

    }

    public function add()
    {

        $title = $this->title;

        return view('admin.room.add', compact('title'));

    }

    public function save(Request $request)
    {
        $this->validate($request,[
            'room_type' => 'required|min:5',
            'room_facilities' => 'required',
            'room_description' => 'required',
            'room_last_price' => 'required',
            'room_starting_price' => 'required',
            'total_rooms'=>'required',
            'room_sales_price'=>'required',
            'room_availability'=>'required',
            'room_status'=>'required',
            'room_criteria'=>'required',

        ]);
        $data=[
            'type'=>$request->input('room_type'),
            'facilities'=>$request->input('room_facilities'),
            'description'=>$request->input('room_description'),
            'last_price'=>$request->input('room_last_price'),
            'starting_price'=>$request->input('room_starting_price'),
            'total_rooms'=>$request->input('total_rooms'),
            'sales_price'=>$request->input('room_sales_price'),
            'availability'=>$request->input('room_availability'),
            'status'=>$request->input('room_status'),
            'criteria'=>$request->input('room_criteria'),
            'added_by'=>Auth::user()->id,
            'edited_by'=>Auth::user()->id
        ];
        $this->roomRepo->create($data);
        return redirect()->route('admin.rooms');
    }

    public function edit($id)
    {
        if (!$room = $this->roomRepo->find($id)) {
            return view('errors.503');
        }
        return view('admin.room.update')->with(compact('room'));
    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'room_type' => 'required|min:5',
            'room_facilities' => 'required',
            'room_description' => 'required',
            'room_last_price' => 'required',
            'room_starting_price' => 'required',
            'total_rooms'=>'required',
            'room_sales_price'=>'required',
            'room_availability'=>'required',
            'room_status'=>'required',
            'room_criteria'=>'required',

        ]);
        $id = $request->input('id');
        $data=[
            'type'=>$request->input('room_type'),
            'facilities'=>$request->input('room_facilities'),
            'description'=>$request->input('room_description'),
            'last_price'=>$request->input('room_last_price'),
            'starting_price'=>$request->input('room_starting_price'),
            'total_rooms'=>$request->input('total_rooms'),
            'sales_price'=>$request->input('room_sales_price'),
            'availability'=>$request->input('room_availability'),
            'status'=>$request->input('room_status'),
            'criteria'=>$request->input('room_criteria'),
            'added_by'=>Auth::user()->id,
            'edited_by'=>Auth::user()->id
        ];

        $this->roomRepo->update($id,$data);
        return redirect()->route('admin.rooms');

    }

    public function delete($id)
    {
        if (!$room = $this->roomRepo->find($id)) {
            return view('errors.503');
        }else{
            $this->roomRepo->delete($id);
        }
        return redirect()->route('admin.rooms');
    }
}
