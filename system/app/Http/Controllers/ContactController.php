<?php

namespace App\Http\Controllers;

use App\libraries\storage\ContactRepository;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public  $contacts,  $img_path;
    public $title = 'Messages';

    public function __construct(ContactRepository $contacts)
    {
        $this->contacts = $contacts;
       
    }

    public function index()
    {
    
        $title = $this->title;

        $messages = $this->contacts->all();

        return view('admin.contact.index', compact('messages', 'title'));
    }

    public function add()
    {
        $title = $this->title;

        return view('admin.contact.add', compact('title'));
    }

    public function save(Request $request)
    {
        $data=[
            'name'=>$request->input('name'),
            'added_by'=>Auth::user()->id,
            'edited_by'=>Auth::user()->id
        ];

        
        $this->contacts->create($data);
        return redirect()->route('admin.contacts');
    }

    public function delete($id)
    {
        if (!$message = $this->contacts->find($id)) {
            return view('errors.503');
        }
        $this->contacts->delete($id);
        return redirect();
    }
}
