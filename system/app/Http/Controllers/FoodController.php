<?php

namespace App\Http\Controllers;

use App\libraries\storage\FoodRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FoodController extends Controller
{

    public  $foodRepo,  $img_path;
    public $title = 'Foods';
    public function __construct(FoodRepository $foodRepo)
    {
        $this->foodRepo = $foodRepo;
        $this->img_path = 'assets/uploads/foods/';
    }

    public function index()
    {
        $img_path = $this->img_path;
        $title = $this->title;

        $allfoods = $this->foodRepo->all();

        return view('admin.food.index', compact('allfoods', 'title','img_path'));
    }

    public function add()
    {
        $title = $this->title;

        return view('admin.food.add', compact('title'));
    }

    public function save(Request $request)
    {
        $this->validate($request,[
            'food_name' => 'required',
            'food_price' => 'required',
            'food_image' => 'required',
        ]);
        $data=[
            'name'=>$request->input('food_name'),
            'price'=>$request->input('food_price'),
            'added_by'=>Auth::user()->id,
            'edited_by'=>Auth::user()->id
        ];

        if($request->hasFile('food_image'))
        {
            $extension = $request->file('food_image')->getClientOriginalExtension();
            $filename  = str_random(8).'.'.$extension;
            $request->file('food_image')->move($this->img_path,$filename);
            $data['image']=$filename;
        }
        $this->foodRepo->create($data);
        return redirect()->route('admin.foods');
    }

    public function edit($id)
    {
        if (!$food = $this->foodRepo->find($id)) {
            return view('errors.503');
        }
        $img_path = $this->img_path;

        return view('admin.food.update')->with(compact('food','img_path'));
    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'food_name' => 'required',
            'food_price' => 'required',
            'food_image' => 'required',
        ]);
        $id = $request->input('id');
        $data=[
            'name'=>$request->input('food_name'),
            'price'=>$request->input('food_price'),
            'edited_by'=>Auth::user()->id
        ];

        if($request->hasFile('food_image'))
        {
            $extension = $request->file('food_image')->getClientOriginalExtension();
            $filename  = str_random(8).'.'.$extension;
            $request->file('food_image')->move($this->img_path,$filename);
            $data['image']=$filename;
        }
        $this->foodRepo->update($id,$data);
        return redirect()->route('admin.foods');
    }


    public function delete($id)
    {
        if (!$food = $this->foodRepo->find($id)) {
            return view('errors.503');
        }else{
            $this->foodRepo->delete($id);
        }
        return redirect()->route('admin.foods');
    }
}
