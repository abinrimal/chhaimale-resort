<?php
/**
 * Created by PhpStorm.
 * User: Kribin
 * Date: 4/17/2018
 * Time: 12:46 PM
 */

namespace App\Libraries\Storage;


class Repository implements RepositoryInterface
{
    protected $model;

    public function all() {
        return $this->model->all();
    }

    public function first() {
        return $this->model->first();
    }

    public function find($id) {
        return $this->model->find($id);
    }

    public function getById($id)
    {
        return $this->model->find($id);
    }

    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }

    public function update($id, array $attributes)
    {
        $data = $this->model->findOrFail($id);
        $data->update($attributes);
        return $data;
    }

    public function delete($id)
    {
        $this->model->find($id)->delete();

        return true;
    }
    public function destroy($id) {
        return $this->model->destroy($id);
    }

    public function orderBy($prop, $type = null) {
        return $this->model->orderBy($prop, $type);
    }

    public function where($column, $operator, $value) {
        return $this->model->where($column, $operator, $value);
    }

    public function orWhere($column, $operator, $value) {
        return $this->model->orWhere($column, $operator, $value);
    }

    public function orWhereBetween($column, $range) {
        return $this->model->orWhereBetween($column, $range);
    }

    public function whereBetween($column, $range) {
        return $this->model->whereBetween($column, $range);
    }

    public function where_array($array) {
        return $this->model->where($array);
    }
    public function whereIn($column,$array) {
        return $this->model->whereIn($column,$array);
    }
    public function whereNull($column) {
        return $this->model->whereNull($column);
    }
    public function firstOrNew($id) {
        return $this->model->firstOrNew(array('id' => $id));
    }
}