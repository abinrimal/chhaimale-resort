<?php
/**
 * Created by PhpStorm.
 * User: Kundan
 * Date: 24/07/2018
 * Time: 13:49
 */

namespace App\libraries\storage;


use App\model\settings;

class SettingRepository extends Repository
{
        public function __construct(settings $setting)
        {
            $this->model = $setting;
        }
}