<?php
/**
 * @project Chhaimale Resort
 * @author Abin Rimal
 * Prabidhi Labs
 */

namespace App\libraries\storage;

interface RepositoryInterface
{
    public function all();
    public function first();
    public function find($id);
    public function create(array $attributes);
    public function update($id, array $attributes);
    public function delete($id);
    public function destroy($id);
    public function orderBy($prop, $type = null);
    public function where($column, $operator,$value);
    public function orWhere($column, $operator,$value);
    public function orWhereBetween($column, $range);
    public function whereBetween($column, $range);
}