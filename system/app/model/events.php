<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class events extends Model
{
	use Helper;
    protected $fillable = [
      'title','sub_title','description','added_by','edited_by'
    ];
}
