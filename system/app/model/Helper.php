<?php

namespace App\model;

use App\User;

trait Helper 
{

    public function getUserName($key) {
 		$data =  json_decode(User::where('id', $key)->select('name')->get());
 		return $data[0]->name;
	}
	
}
