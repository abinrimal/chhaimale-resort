<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class conference extends Model
{
	use Helper;
    protected $fillable = [
      'title','first_name','last_name','address1','address2','city','zip','country','phone','email','org','cfname','attendance','startdate','starttime','endtime',
        'enddate','style','setup_suggestion','meal','meal_suggestion','room_detail','room_suggestion','credit_card','added_by','edited_by'
    ];
}
