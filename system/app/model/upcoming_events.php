<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class upcoming_events extends Model
{
	use Helper;
    protected $fillable = [
      'title','description','amount','criteria','total_seats','image','meta_title','meta_description','keywords',
        'added_by','updated_by','date'
    ];
}
