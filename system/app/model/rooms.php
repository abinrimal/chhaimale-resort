<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class rooms extends Model
{
	use Helper;
    protected $fillable = [
      'type','starting_price','last_price','criteria','facilities','description','total_rooms','availability','status',
        'sales_price','added_by','edited_by'
    ];
}
