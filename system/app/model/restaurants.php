<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class restaurants extends Model
{
	use Helper;
    protected $fillable = [
      'page_header','sub_header','chef_name','chef_photo','chef_message','featured_image','featured_title'
        ,'description','added_by','edited_by','featured_message'
    ];
}
