(function($){
    $(function(){
        //global variables
        var type;

        function init(){
            $('.listing-type').on("change",function(e){
                type = $(e.target).data("stat");
                if(type === 1){
                    $('#bank_valuation').fadeOut();
                }else{
                    $('#bank_valuation').fadeIn();
                }
            });
            $('.property_type').on("change",function(e){
                type = $(e.target).data("stat");
                if(type === 2){
                    $('#landed_sub').fadeIn(600);
                    $('#hdb_sub,#condo_sub,#banksale_sub').css('display','none');
                }
                if(type === 0){
                    $('#hdb_sub').fadeIn(600);
                    $('#landed_sub,#condo_sub,#banksale_sub').css('display','none');
                }
                if(type === 1){
                    $('#condo_sub').fadeIn(600);
                    $('#hdb_sub,#landed_sub,#banksale_sub').css('display','none');
                }
                if(type === 3){
                    $('#banksale_sub').fadeIn(600);
                    $('#hdb_sub,#landed_sub,#condo_sub').css('display','none');
                }
            });
            $('.landed-type').on("change",function(e){
                type = $(e.target).data("stat");
                if(type === 0){
                    $('#land_area').fadeIn();
                }else{
                    $('#land_area').fadeOut();
                }
                if(type === 4){
                    $('#bank_valuation').fadeOut();
                }else{
                    $('#bank_valuation').fadeIn();
                }
            });
        }
        $(document).ready(function(){
            init();
        });
    });
})(this.jQuery);


$(document).ready(function(){
    imageUploader();
});

function imageUploader() {
    var imgData = [],
        uploadLabel = $('#upload')
    preview = $('.preview');
    var filesSum = [];

    $("#post-image").on("change", function()
    {
        var files = !!this.files ? this.files : [];
        if(filesSum.length) files.length = filesSum.length;

        if (!files.length || !window.FileReader) return;

        if(files.length){
            for(var i=0;i<files.length;i++){


                createImage(files[i]);
            }

        }

    });
    function createImage(file){
        if (/^image/.test( file.type)){
            var reader = new FileReader();
            reader.readAsDataURL(file);

            reader.onloadend = function(){
                filesSum.push(this.result);
                //filesSum.push(file.name);
                preview.find('ul').prepend('<li style="background-image:url('+this.result+');background-size:cover;background-position:center center;" class="in"><i class="fa fa-close"></i></li>');
                uploadLabel.html(filesSum.length+"file(s)<span>(click to upload more)</span>");
            }
        }
    }

    //removing thumbnail
    $('ul.post-image').on('click','li i',function(e){
        var target = $(e.target),
            index = Math.abs(target.parent().index() - filesSum.length)-1;
        target.parent().addClass('out');
        setTimeout(function(){
            target.parent().remove();
        },800);

        filesSum.splice(index,1);
        uploadLabel.html(filesSum.length+"file(s)<span>(click to upload more)</span>");
        console.log(filesSum);
    });

}

$(window).load(function(){
    if($('#test4').attr('checked') === 'checked'){
        $('#hdb_sub').fadeIn(600);
        $('#landed_sub,#condo_sub,#banksale_sub').css('display','none');
    }

    if($('#test5').attr('checked') === 'checked'){
        $('#condo_sub').fadeIn(600);
        $('#hdb_sub,#landed_sub,#banksale_sub').css('display','none');
    }

    if($('#test6').attr('checked') === 'checked'){
        $('#landed_sub').fadeIn(600);
        $('#hdb_sub,#condo_sub,#banksale_sub').css('display','none');
    }

    if($('#test7').attr('checked') === 'checked'){
        $('#banksale_sub').fadeIn(600);
        $('#hdb_sub,#condo_sub,#landed_sub').css('display','none');
    }

});