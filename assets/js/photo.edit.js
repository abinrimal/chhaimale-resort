/* Photo Module */
admin.photo = {};
admin.photo.images = [];
admin.photo.cropper = [];
admin.photo.saveAction = "crop";

admin.photo.cropperConfig = {
    aspectRatio: null,
    viewMode: 0,
    autoCrop: false,
    responsive: true,
    crop: function (e) {
        var imageId = e.target.id;
        var photoPane = $('[data-id="' + imageId + '"]');

        var cropWidth = photoPane.find('[data-crop="width"]');
        var cropHeight = photoPane.find('[data-crop="height"]');

        cropWidth.val(parseInt(e.detail.width));
        cropHeight.val(parseInt(e.detail.height));
        console.log(e);
    }
};

$('[data-toggle="cropper"]').click(function () {
    admin.photo.show(this);
});

$('[data-toggle="tab"]').on('show.bs.tab', function (e) {
    location.hash = $(this).data('tab');
});

admin.photo.show = function (trigger) {
    var imageId = $(trigger).data('target-id');
    var photoPane = $('[data-id="' + imageId + '"]');
    var image = document.getElementById(imageId);
    var imageType = $(image).data('photo-type');

    var imageWidth = parseInt(photoPane.find('[data-image-width]').val());
    var imageHeight = parseInt(photoPane.find('[data-image-height]').val());

    admin.photo.images[imageType] = {width: imageWidth, height: imageHeight};

    admin.photo.cropper[imageType] = new Cropper(image, admin.photo.cropperConfig);

    admin.photo.setHandlers(photoPane, imageType);

    photoPane.find('.photo-info').fadeOut(100, function () {
        photoPane.find('.cropper-tools').fadeIn(100);
    });
};

admin.photo.hide = function (trigger) {
    var imageId = $(trigger).data('target-id');
    var photoPane = $('[data-id="' + imageId + '"]');
    var image = document.getElementById(imageId);
    var imageType = $(image).data('photo-type');

    admin.photo.cropper[imageType].destroy();

    photoPane.find('.cropper-tools').fadeOut(100, function () {
        photoPane.find('.photo-info').fadeIn(100);
    });
};

admin.photo.scale = function (photoType) {
    var cropper = admin.photo.cropper[photoType];
    var image = cropper.getImageData();

    var photoPane = $('[data-id="image_' + photoType + '"]');

    var originalWidth = image.naturalWidth;
    var originalHeight = image.naturalHeight;

    var adjustedWidth = parseInt(photoPane.find('[data-scale="width"]').val());
    var adjustedHeight = parseInt(photoPane.find('[data-scale="height"]').val());

    var scaleX = adjustedWidth / originalWidth;
    var scaleY = adjustedHeight / originalHeight;

    cropper.scale(scaleX, scaleY);
    admin.photo.saveAction = "scale";
};

admin.photo.setHandlers = function (pane, type) {

    var scale = pane.find('[data-scale="scale"]');
    var scaleWidth = pane.find('[data-scale="width"]');
    var scaleHeight = pane.find('[data-scale="height"]');

    var cropWidth = pane.find('[data-crop="width"]');
    var cropHeight = pane.find('[data-crop="height"]');
    var cropAspect = pane.find('[data-crop-aspect]');

    var save = pane.find('[data-action="save"]');
    var cancel = pane.find('[data-action="cancel"]');

    scale.bind('click', function () {
        var photoType = $(this).data('photo-type');
        admin.photo.scale(photoType);
    });

    scaleWidth.bind('keyup change', function (e) {
        var photoType = $(this).data('photo-type');
        var cropper = admin.photo.cropper[photoType];
        var image = cropper.getImageData();

        var originalWidth = image.naturalWidth;
        var originalHeight = image.naturalHeight;

        var adjustedWidth = parseInt($(this).val());
        var adjustedHeight = null;

        adjustedHeight = adjustedWidth / (originalWidth / originalHeight);
        scaleHeight.val(parseInt(adjustedHeight));

        admin.photo.scale(photoType);
    });

    scaleHeight.bind('keyup change', function (e) {
        var photoType = $(this).data('photo-type');
        var cropper = admin.photo.cropper[photoType];
        var image = cropper.getImageData();

        var originalWidth = image.naturalWidth;
        var originalHeight = image.naturalHeight;

        var adjustedWidth = null;
        var adjustedHeight = parseInt($(this).val());

        adjustedWidth = (originalWidth / originalHeight) * adjustedHeight;
        scaleWidth.val(parseInt(adjustedWidth));

        admin.photo.scale(photoType);

    });

    cropWidth.on('change', function () {
        var photoType = $(this).data('photo-type');
        var cropper = admin.photo.cropper[photoType];
        cropper.setData({"width": parseInt($(this).val())});
    });

    cropHeight.on('change', function () {
        var photoType = $(this).data('photo-type');
        var cropper = admin.photo.cropper[photoType];
        cropper.setData({"height": parseInt($(this).val())});
    });

    cropAspect.on('click', function () {
        var photoType = $(this).data('photo-type');
        var cropper = admin.photo.cropper[photoType];

        var ratio = $(this).data('crop-aspect');
        if (ratio == "free") {
            cropper.setAspectRatio(null)
        } else {
            cropper.setAspectRatio(eval(ratio));
        }
    });

    save.on('click', function () {

        var btnSave = $(this);
        btnSave.button('loading');

        var photoType = $(this).data('photo-type');
        var photoPane = $('[data-id="image_' + photoType + '"');

        var cropper = admin.photo.cropper[photoType];
        var scaleWidth = photoPane.find('[data-scale="width"]').val();
        var scaleHeight = photoPane.find('[data-scale="height"]').val();

        var cropUrl = pane.data('action-url');
        var data = {
            "_token": admin.csrfToken,
            "action": admin.photo.saveAction,
            "type": photoType,
            "scale_data": {
                "w": scaleWidth,
                "h": scaleHeight
            },
            "crop_data": cropper.getData(true)
        };

        console.log(data);

        $.ajax({
            url: cropUrl,
            type: "POST",
            dataType: "json",
            data: data,
            success: function (data, status, xhr) {
                toastr.success(data.msg, 'Success');
                location.reload()
            },
            error: function (xhr, status, error) {
                toastr.error("Failed to save image");
                btnSave.button('reset')
            },
            complete: function (xhr, status) {

            }
        });

    });

    cancel.on('click', function () {
        var btnCancel = $(this);
        admin.photo.hide(this);
    });

};