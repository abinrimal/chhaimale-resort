//3D Nav Bar

  $('.block-menu').find('a').each(function(){

  var el = $(this),
       elText = el.text();
  
  el.addClass("three-d");
  el.append('<span aria-hidden="true" class="three-d-box"><span class="front">'+elText+'</span><span class="back">'+elText+'</span></span>');


});





//Scrolling Js

       $(window).scroll(function() {
                  /* affix after scrolling 100px */
                  if ($(document).scrollTop() > 100) {
                    $('.navbar').addClass('affix');
                  } else {
                    $('.navbar').removeClass('affix');
                  }
                })




// Scrolling  apper Botton


       $(window).scroll(function() {
                  /* affix after scrolling 200px */
                  if ($(document).scrollTop() > 200) {
                    $('#book_botton').addClass('show');
                  } else {
                    $('#book_botton').removeClass('show');
                  }
        })
       
        
//Reservation Page Checkbox select
   $('.package-list').on('change', function() {
       $('.package-list').not(this).prop('checked', false);  
    });


// tabnav.js rooms page

$('.tab-link').click( function() {
  
  var tabID = $(this).attr('data-tab');
  
  $(this).addClass('active').siblings().removeClass('active');
  
  $('#tab-'+tabID).addClass('active').siblings().removeClass('active');
});

//Pop Over over icon in rooms slider

    $('.popoverData').popover();


//Footer Parallax Scrolling 


//rooms details toggle


$(document).ready(function(){
    $("#standard-room").click(function(){
        $(".standard-hidden-feature").toggle();
    });
});


$(document).ready(function(){
    $("#wooden-room").click(function(){
        $(".wooden-hidden-feature").toggle();
    });
});

$(document).ready(function(){
    $("#homestay").click(function(){
        $(".homestay-hidden-feature").toggle();
    });
});

$(document).ready(function(){
    $("#tentstay").click(function(){
        $(".tentstay-hidden-feature").toggle();
    });
});





//Reservation Page
        

//Rooms Booking  date picker

$( "#check-out" ).datepicker({
    minDate: new Date()
});

$( "#check-in" ).datepicker({
    minDate: new Date(),
    onSelect: function(dateText, inst) {
        var selectedDate = $( this ).datepicker( "getDate" );
        $( "#check-out" ).datepicker( "option", "minDate", selectedDate );
    }
});

$(function() {
    $("#check-in").datepicker({
        dateFormat: "yy-mm-dd"
    }).datepicker("setDate", "0");
});

$(function() {
    $("#check-out").datepicker({
        dateFormat: "yy-mm-dd"
    }).datepicker("setDate", "1");
});  


//conference hall booking date picker


       $( "#checkout" ).datepicker({
    minDate: new Date()
});

$( "#checkin" ).datepicker({
    minDate: new Date(),
    onSelect: function(dateText, inst) {
        var selectedDate = $( this ).datepicker( "getDate" );
        $( "#checkout" ).datepicker( "option", "minDate", selectedDate );
    }
});

$(function() {
    $("#checkin").datepicker({
        dateFormat: "yy-mm-dd"
    }).datepicker("setDate", "0");
});

$(function() {
    $("#checkout").datepicker({
        dateFormat: "yy-mm-dd"
    }).datepicker("setDate", "1");
});         




// Deluxe Room Image Viewer Js

 document.addEventListener('DOMContentLoaded',function(){
    var photo = new SmartPhoto(".js-img-viwer");
        photo.on('change',function(){
            console.log('change');
        });
        photo.on('close',function(){
            console.log('close');
        });
        photo.on('swipestart',function(){
            console.log('swipestart');
        });
        photo.on('swipeend',function(){
            console.log('swipeend');
        });
        photo.on('loadall',function(){
            console.log('loadall');
        });
        photo.on('zoomin',function(){
            console.log('zoomin');
        });
        photo.on('zoomout',function(){
            console.log('zoomout');
        });
        photo.on('open',function(){
            console.log('open');
        });
  });
// Family Room Image Viewer Js

 document.addEventListener('DOMContentLoaded',function(){
    var photo = new SmartPhoto(".img-viwer");
        photo.on('change',function(){
            console.log('change');
        });
        photo.on('close',function(){
            console.log('close');
        });
        photo.on('swipestart',function(){
            console.log('swipestart');
        });
        photo.on('swipeend',function(){
            console.log('swipeend');
        });
        photo.on('loadall',function(){
            console.log('loadall');
        });
        photo.on('zoomin',function(){
            console.log('zoomin');
        });
        photo.on('zoomout',function(){
            console.log('zoomout');
        });
        photo.on('open',function(){
            console.log('open');
        });
  });




//Home Stay image viewer js

 document.addEventListener('DOMContentLoaded',function(){
    var photo = new SmartPhoto(".viwer");
        photo.on('change',function(){
            console.log('change');
        });
        photo.on('close',function(){
            console.log('close');
        });
        photo.on('swipestart',function(){
            console.log('swipestart');
        });
        photo.on('swipeend',function(){
            console.log('swipeend');
        });
        photo.on('loadall',function(){
            console.log('loadall');
        });
        photo.on('zoomin',function(){
            console.log('zoomin');
        });
        photo.on('zoomout',function(){
            console.log('zoomout');
        });
        photo.on('open',function(){
            console.log('open');
        });
  });

 $(".rate-label a").on("click",function(e){
    e.preventDefault();
    var target=$(this).attr("href");
    $('html,body').animate({
        scrollTop:$('#wrapper').offset().top  - 150
    },500)
 });

      






           